<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/jquery/jquery.js'></script>
<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/jquery/jquery-migrate.min.js'></script>
<script type='text/javascript' src='<?= base_url() ?>theme/theme/plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js'></script>
<script type='text/javascript' src='<?= base_url() ?>theme/theme/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js'></script>

<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/jquery/ui/effect.min.js'></script>

<script type='text/javascript' src='<?= base_url() ?>theme/theme/js/script.js'></script>
<script type='text/javascript' src='<?= base_url() ?>theme/theme/plugins/goodlayers-core/plugins/combine/script.js'></script>

<script type='text/javascript' src='<?= base_url() ?>theme/theme/plugins/goodlayers-core/include/js/page-builder.js'></script>


<script type="text/javascript" src="<?= base_url() ?>theme/theme/plugins/revslider/public/assets/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/plugins/revslider/public/assets/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/plugins/revslider/public/assets/js/extensions/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/plugins/revslider/public/assets/js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/plugins/revslider/public/assets/js/extensions/revolution.extension.parallax.min.js"></script>  
<script type="text/javascript" src="<?= base_url() ?>theme/theme/plugins/revslider/public/assets/js/extensions/revolution.extension.actions.min.js"></script> 
<script type="text/javascript">
    /*<![CDATA[*/
    function setREVStartSize(e) {
        try {
            var i = jQuery(window).width(),
                t = 9999,
                r = 0,
                n = 0,
                l = 0,
                f = 0,
                s = 0,
                h = 0;
            if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function(e, f) {
                    f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e)
                }), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) {
                var u = (e.c.width(), jQuery(window).height());
                if (void 0 != e.fullScreenOffsetContainer) {
                    var c = e.fullScreenOffsetContainer.split(",");
                    if (c) jQuery.each(c, function(e, i) {
                        u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u
                    }), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ? u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0))
                }
                f = u
            } else void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
            e.c.closest(".rev_slider_wrapper").css({
                height: f
            })
        } catch (d) {
            console.log("Failure at Presize of Slider:" + d)
        }
    }; /*]]>*/
</script>
<script type="text/javascript">
    /*<![CDATA[*/
    function revslider_showDoubleJqueryError(sliderID) {
        var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
        errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
        errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
        errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
        errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
        jQuery(sliderID).show().html(errorMessage);
    } /*]]>*/
</script>

<script>
    /*<![CDATA[*/
    var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
    var htmlDivCss = "";
    if (htmlDiv) {
        htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
    } else {
        var htmlDiv = document.createElement("div");
        htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
        document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
    } /*]]>*/
</script>
<script type="text/javascript">
    /*<![CDATA[*/
    setREVStartSize({
        c: jQuery('#rev_slider_2_1'),
        gridwidth: [1180],
        gridheight: [710],
        sliderLayout: 'auto'
    });
    var revapi2, tpj = jQuery;
    tpj(document).ready(function() {
        if (tpj("#rev_slider_2_1").revolution == undefined) {
            revslider_showDoubleJqueryError("#rev_slider_2_1");
        } else {
            revapi2 = tpj("#rev_slider_2_1").show().revolution({
                sliderType: "standard",
                jsFileLocation: "//demo.goodlayers.com/realfactory/wp-content/plugins/revslider/public/assets/js/",
                sliderLayout: "auto",
                dottedOverlay: "none",
                delay: 9000,
                navigation: {
                    keyboardNavigation: "off",
                    keyboard_direction: "horizontal",
                    mouseScrollNavigation: "off",
                    mouseScrollReverse: "default",
                    onHoverStop: "off",
                    arrows: {
                        style: "uranus",
                        enable: true,
                        hide_onmobile: false,
                        hide_onleave: false,
                        tmp: '',
                        left: {
                            h_align: "left",
                            v_align: "center",
                            h_offset: 20,
                            v_offset: 0
                        },
                        right: {
                            h_align: "right",
                            v_align: "center",
                            h_offset: 20,
                            v_offset: 0
                        }
                    },
                    bullets: {
                        enable: true,
                        hide_onmobile: false,
                        style: "uranus",
                        hide_onleave: false,
                        direction: "horizontal",
                        h_align: "center",
                        v_align: "bottom",
                        h_offset: 0,
                        v_offset: 35,
                        space: 7,
                        tmp: '<span class="tp-bullet-inner"></span>'
                    }
                },
                visibilityLevels: [1240, 1024, 778, 480],
                gridwidth: 1180,
                gridheight: 710,
                lazyType: "none",
                shadow: 0,
                spinner: "off",
                stopLoop: "off",
                stopAfterLoops: -1,
                stopAtSlide: -1,
                shuffle: "off",
                autoHeight: "off",
                disableProgressBar: "on",
                hideThumbsOnMobile: "off",
                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                debugMode: false,
                fallbacks: {
                    simplifyAll: "off",
                    nextSlideOnWindowFocus: "off",
                    disableFocusListener: false,
                }
            });
        }
    }); /*]]>*/
</script>
<script>
    /*<![CDATA[*/
    var htmlDivCss = unescape("%23rev_slider_2_1%20.uranus.tparrows%20%7B%0A%20%20width%3A50px%3B%0A%20%20height%3A50px%3B%0A%20%20background%3Argba%28255%2C255%2C255%2C0%29%3B%0A%20%7D%0A%20%23rev_slider_2_1%20.uranus.tparrows%3Abefore%20%7B%0A%20width%3A50px%3B%0A%20height%3A50px%3B%0A%20line-height%3A50px%3B%0A%20font-size%3A40px%3B%0A%20transition%3Aall%200.3s%3B%0A-webkit-transition%3Aall%200.3s%3B%0A%20%7D%0A%20%0A%20%20%23rev_slider_2_1%20.uranus.tparrows%3Ahover%3Abefore%20%7B%0A%20%20%20%20opacity%3A0.75%3B%0A%20%20%7D%0A%23rev_slider_2_1%20.uranus%20.tp-bullet%7B%0A%20%20border-radius%3A%2050%25%3B%0A%20%20box-shadow%3A%200%200%200%202px%20rgba%28255%2C%20255%2C%20255%2C%200%29%3B%0A%20%20-webkit-transition%3A%20box-shadow%200.3s%20ease%3B%0A%20%20transition%3A%20box-shadow%200.3s%20ease%3B%0A%20%20background%3Atransparent%3B%0A%20%20width%3A15px%3B%0A%20%20height%3A15px%3B%0A%7D%0A%23rev_slider_2_1%20.uranus%20.tp-bullet.selected%2C%0A%23rev_slider_2_1%20.uranus%20.tp-bullet%3Ahover%20%7B%0A%20%20box-shadow%3A%200%200%200%202px%20rgba%28255%2C%20255%2C%20255%2C1%29%3B%0A%20%20border%3Anone%3B%0A%20%20border-radius%3A%2050%25%3B%0A%20%20background%3Atransparent%3B%0A%7D%0A%0A%23rev_slider_2_1%20.uranus%20.tp-bullet-inner%20%7B%0A%20%20-webkit-transition%3A%20background-color%200.3s%20ease%2C%20-webkit-transform%200.3s%20ease%3B%0A%20%20transition%3A%20background-color%200.3s%20ease%2C%20transform%200.3s%20ease%3B%0A%20%20top%3A%200%3B%0A%20%20left%3A%200%3B%0A%20%20width%3A%20100%25%3B%0A%20%20height%3A%20100%25%3B%0A%20%20outline%3A%20none%3B%0A%20%20border-radius%3A%2050%25%3B%0A%20%20background-color%3A%20rgb%28255%2C%20255%2C%20255%29%3B%0A%20%20background-color%3A%20rgba%28255%2C%20255%2C%20255%2C%200.3%29%3B%0A%20%20text-indent%3A%20-999em%3B%0A%20%20cursor%3A%20pointer%3B%0A%20%20position%3A%20absolute%3B%0A%7D%0A%0A%23rev_slider_2_1%20.uranus%20.tp-bullet.selected%20.tp-bullet-inner%2C%0A%23rev_slider_2_1%20.uranus%20.tp-bullet%3Ahover%20.tp-bullet-inner%7B%0A%20transform%3A%20scale%280.4%29%3B%0A%20-webkit-transform%3A%20scale%280.4%29%3B%0A%20background-color%3Argb%28255%2C%20255%2C%20255%29%3B%0A%7D%0A");
    var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
    if (htmlDiv) {
        htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
    } else {
        var htmlDiv = document.createElement('div');
        htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
        document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
    } /*]]>*/
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBiTQp5QUWTeD0XQpNnv3kevaKrtokkpnA"></script>
<script type='text/javascript'>
    var wpgmp_local = {
        "all_location": "All",
        "show_locations": "Show Locations",
        "sort_by": "Sort by",
        "wpgmp_not_working": "not working...",
        "place_icon_url": "https:////demo.goodlayers.com//kleanity//wp-content//plugins//wp-google-map-plugin//assets//images//icons//"
    };
</script>
<script type='text/javascript' src='<?= base_url() ?>theme/theme/plugins/google-map-plugin/assets/js/maps.js?'></script>
<script type='text/javascript' src='<?= base_url() ?>theme/theme/plugins/google-map-plugin/assets/js/frontend.js'></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/plugins/twentytwenty/jquery.twentytwenty.js"></script>
<script>
    jQuery(document).ready(function($) {
        var map1 = $("#map1").maps({
            "map_options": {
                "center_lat": "41.3949429",
                "center_lng": "2.1911371",
                "zoom": 16,
                "map_type_id": "ROADMAP",
                "draggable": true,
                "scroll_wheel": false,
                "display_45_imagery": "",
                "marker_default_icon": null,
                "infowindow_setting": "",
                "infowindow_bounce_animation": null,
                "infowindow_drop_animation": false,
                "close_infowindow_on_map_click": false,
                "default_infowindow_open": false,
                "infowindow_open_event": "click",
                "full_screen_control": true,
                "search_control": true,
                "zoom_control": true,
                "map_type_control": true,
                "street_view_control": true,
                "full_screen_control_position": null,
                "search_control_position": null,
                "zoom_control_position": "TOP_LEFT",
                "map_type_control_position": "TOP_RIGHT",
                "map_type_control_style": "HORIZONTAL_BAR",
                "street_view_control_position": "TOP_LEFT",
                "map_control": true,
                "map_control_settings": null,
                "width": "",
                "height": "550"
            },
            "places": [{
                "id": "1",
                "title": "Mold Arc SL",
                "address": "Carrer de Pere IV, 29-35, 08018 Barcelona",
                "source": "manual",
                "content": "Barcelona",
                "location": {
                    "icon": '<?= base_url('img/map-marker.png') ?>',
                    "lat": "41.3949429",
                    "lng": "2.1911371",
                    "city": "Barcelona",
                    "state": "Barcelona",
                    "country": "España",
                    "onclick_action": "marker",
                    "redirect_custom_link": "",
                    "marker_image": null,
                    "open_new_tab": "yes",
                    "postal_code": "",
                    "draggable": false,
                    "infowindow_default_open": false,
                    "animation": "BOUNCE",
                    "infowindow_disable": true,
                    "zoom": 5,
                    "extra_fields": null
                },
                "categories": [],
                "custom_filters": null
            }],
            "street_view": false,
            "map_property": {
                "map_id": "1"
            }
        }).data("wpgmp_maps");


        var $before_after_slider = $('.twentytwenty-container');
        if( $before_after_slider.length > 0 ) {
            $before_after_slider.each(function() {
                var $this = $(this);
                var data_offset_pct = ( $this.data("offset-percent") === undefined ) ? 0.5: $this.data("offset-percent");
                var data_orientation = ( $this.data("orientation") === undefined ) ? 'horizontal': $this.data("orientation");
                var data_before_label = ( $this.data("before-label") === undefined ) ? 'Before': $this.data("before-label");
                var data_after_label = ( $this.data("after-label") === undefined ) ? 'After': $this.data("after-label");
                var data_no_overlay = ( $this.data("no-overlay") === undefined ) ? true: $this.data("no-overlay");
                $this.twentytwenty({
                    default_offset_pct: data_offset_pct, // How much of the before image is visible when the page loads
                    orientation: data_orientation, // Orientation of the before and after images ('horizontal' or 'vertical')
                    before_label: data_before_label, // Set a custom before label
                    after_label: data_after_label, // Set a custom after label
                    no_overlay: data_no_overlay //Do not show the overlay with before and after
                });
            });
        }


    });
</script>


<script src='https://www.google.com/recaptcha/api.js?render=6Ld0NnwUAAAAADYC-xXMRmpYMEc4wZvl0XVrlyEC'></script>
<script>
grecaptcha.ready(function() {
grecaptcha.execute('6Ld0NnwUAAAAADYC-xXMRmpYMEc4wZvl0XVrlyEC', {action: 'contacto'})
.then(function(token) {
    jQuery("#recaptcha").val(token);
});
});
</script>

<script type="text/javascript" src="<?= base_url() ?>js/frame.js"></script>
