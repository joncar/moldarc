<!doctype html>
<html lang="en">

	<!-- Google Web Fonts
	================================================== -->

	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i%7CFrank+Ruhl+Libre:300,400,500,700,900" rel="stylesheet">

	<!-- Basic Page Needs
	================================================== -->

	<title><?= empty($title) ? 'Monalco' : $title ?></title>
  	<meta name="keywords" content="<?= empty($keywords) ?'': $keywords ?>" />
	<meta name="description" content="<?= empty($keywords) ?'': strip_tags($description) ?>" /> 	
	<link rel="icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>	
	<link rel="shortcut icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>theme/theme/css/editor.css">
	<link href="<?= base_url() ?>js/stocookie/stoCookie.css" rel="stylesheet">

	<script>var URL = '<?= base_url() ?>';</script>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    

    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Hind%3A300%2Cregular%2C500%2C600%2C700&#038;subset=latin%2Clatin-ext%2Cdevanagari&#038;ver=4.9.7' type='text/css' media='all'>
    <link rel='stylesheet' href='<?= base_url() ?>theme/theme/plugins/revslider/public/assets/css/settings.css' type='text/css' media='all'>
    <link rel='stylesheet' href='<?= base_url() ?>theme/theme/css/style-core.css' type='text/css' media='all'>
    <link rel='stylesheet' href='<?= base_url() ?>theme/theme/css/rftr-style-custom.css' type='text/css' media='all'>
    <link rel='stylesheet' href='<?= base_url() ?>theme/theme/plugins/goodlayers-core/plugins/combine/style.css' type='text/css' media='all'>
    <link rel='stylesheet' href='<?= base_url() ?>theme/theme/plugins/goodlayers-core/include/css/page-builder.css' type='text/css' media='all'>
    <link rel='stylesheet' href='<?= base_url() ?>theme/theme/plugins/twentytwenty/jquery.twentytwenty.css' type='text/css' media='all'>
 
</head>
<body data-rsssl="1" class="home page-template-default page page-id-2039 gdlr-core-body woocommerce-no-js realfactory-body realfactory-body-front realfactory-full  realfactory-with-sticky-navigation gdlr-core-link-to-lightbox">
<?php 
	if(empty($editor)){
		$this->load->view($view); 
	}else{
		echo $view;
	}
?>	
<?php $this->load->view('includes/template/scripts') ?>
</body>
</html>