<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{        
        function __construct() {
            parent::__construct();
            $this->load->model('querys');
            $this->load->library('form_validation');
        }       

        public function loadView($param = array('view' => 'main')) {
            if($this->router->fetch_method()!='editor'){
                $param['page'] = $this->querys->fillFields($param['page']);
            }
            parent::loadView($param);
        }
        
        function read($url){            
            $theme = $this->theme;
            $params = $this->uri->segments;
            //Existe?            
            if(file_exists(APPPATH.'modules/paginas/views/'.$this->theme.$url.'.php')){
                $page = $this->load->view($theme.$url,array(),TRUE);
            }else{
                $idiomas = $this->db->get('ajustes')->row()->idiomas;
                $idiomas = explode(',',$idiomas);
                foreach($idiomas as $i){
                    $i = trim($i);
                    if(file_exists(APPPATH.'modules/paginas/views/'.$i.'/'.$url.'.php')){
                        $page = $this->load->view($i.'/'.$url,array(),TRUE);
                        $_SESSION['lang'] = $i;
                        $this->theme = $i.'/';
                    }
                }
            }
            if(empty($page)){
                throw new exception('Pagina web no encontrada',404);
            }
            $this->load->model('querys');
            $this->loadView(
                array(
                    'view'=>'read',
                    'page'=>$page,
                    'link'=>$url,
                    'url'=>$url,
                    'title'=>ucfirst(str_replace('-',' ',$url))
                )
            );
        }
        
        function getFormReg($x = '2'){                    
            return $this->querys->getFormReg($x);
        }
        
        function editor($url){            
            $this->load->helper('string');
            if(!empty($_SESSION['user']) && $this->user->admin==1){                                            
                $page = $this->load->view($url,array(),TRUE);
                $this->loadView(array('view'=>'cms/edit','scripts'=>true,'name'=>$url,'edit'=>TRUE,'page'=>$page,'title'=>'Editar '.ucfirst(str_replace('-',' ',$url))));
            }else{
                redirect(base_url());
            }
        }
        
        function contacto(){             
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('nombre','Nombre','required');
            //$this->form_validation->set_rules('telefono','Telefono','required');   
            $this->form_validation->set_rules('politicas','Politicas','required');            
            if($this->form_validation->run()){
                $this->load->library('recaptcha');                      
                if(!$this->recaptcha->recaptcha_check_answer($_POST['g-recaptcha-response'])){
                    $_SESSION['msj'] = $this->error('error de validación de bot');
                }else{          
                    $notif = $_SESSION['lang']=='es'?11:6;                
                    $this->enviarcorreo((object)$_POST,$notif,'info@moldarc.com');
                    unset($_POST['politicas']);
                    unset($_POST['form-check']);
                    unset($_POST['subject']);
                    unset($_POST['fields']);
                    unset($_POST['g-recaptcha-response']);
                    $_POST['fecha'] = date("Y-m-d H:i:s");
                    $this->db->insert('contacto',$_POST);
                    $_SESSION['msj'] = 'Gracias por contactarnos, en breve nos pondremos en contacto';                
                }
            }else{                
               $_SESSION['msj'] = 'Por favor complete los datos solicitados  <script>$("#guardar").attr("disabled",false); </script>';
            }            
            echo $_SESSION['msj'];
            unset($_SESSION['msj']);
        }
        
        function subscribir(){
            $err = 'error';
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('politicas','Políticas','required');
            if($this->form_validation->run()){
                
                $emails = $this->db->get_where('subscritos',array('email'=>$_POST['email']));
                $success = $emails->num_rows()==0?TRUE:FALSE;
                if($success){
                    $this->db->insert('subscritos',array('email'=>$_POST['email']));    
                    $_SESSION['msj2'] = 'Subscripción exitosa';
                    $err = 'success';
                    $this->enviarcorreo((object)$_POST,10);
                }else{
                    $_SESSION['msj2'] = 'Ya el correo esta registrado';
                }
            }else{
                $_SESSION['msj2'] = $this->error($this->form_validation->error_string());
            }
            echo json_encode(array('result'=>$err,'msg'=>$_SESSION['msj2']));
            unset($_SESSION['msj2']);            
        }
        
        function search(){
            $domain = base_url();
            $domain = explode('/',$domain);
            $domain = str_replace('www.','',$domain[2]);
            if(!empty($_GET['q'])){
                if(empty($_SESSION[$_GET['q']])){
                    $_SESSION[$_GET['q']] = file_get_contents('http://www.google.es/search?ei=meclXLnwCNma1fAPgbCYCA&q=site%3A'.$domain.'+'.urlencode($_GET['q']).'&oq=site%3A'.$domain.'+'.urlencode($_GET['q']).'&gs_l=psy-ab.3...10613.16478..16743...0.0..0.108.1753.20j3......0....1..gws-wiz.0XRgmCZL0TA');                
                }

                $result = $_SESSION[$_GET['q']];                
                preg_match_all('@<div class=\"ZINbbc xpd O9g5cc uUPGi\">(.*)</div>@si',$result,$result);
                $resultado = $result[0][0];
                $resultado = explode('<footer>',$resultado);
                $resultado = $resultado[0];
                $resultado = str_replace('/url?q=','',$resultado);
                $resultado = explode('<div class="ZINbbc xpd O9g5cc uUPGi">',$resultado);                
                foreach($resultado as $n=>$r){
                    if(strpos($r,'/search?q=site:')){
                        unset($resultado[$n]);
                        continue;
                    }
                    $resultado[$n] = $r;                    
                    $resultado[$n] = substr($r,0,strpos($r,'&'));
                    $pos = strpos($r,'">')+2;
                    $rr = substr($r,$pos);
                    $pos = strpos($rr,'">');
                    $rr = substr($rr,$pos);                    
                    $resultado[$n].= $rr;
                    $resultado[$n] = '<div class="ZINbbc xpd O9g5cc uUPGi">'.$resultado[$n];
                    $resultado[$n] = utf8_encode($resultado[$n]);
                }
                $resultado = implode('',$resultado);
                $this->loadView(array(
                    'view'=>'read',
                    'page'=>$this->load->view($this->theme.'search',array('resultado'=>$resultado),TRUE,'paginas'),
                    'result'=>$resultado,
                    'title'=>'Resultado de busqueda'
                ));

            }
        }


        
    }
