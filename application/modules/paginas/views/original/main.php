	[menu]
	<div class="realfactory-page-wrapper" id="realfactory-page-wrapper">
		<div class="gdlr-core-page-builder-body">
			<div class="gdlr-core-pbf-wrapper " style="padding: 0px 0px 0px 0px;">
				<div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
					<div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-pbf-wrapper-full-no-space">
						<div class="gdlr-core-pbf-element">
							<div class="gdlr-core-revolution-slider-item gdlr-core-item-pdlr gdlr-core-item-pdb " style="padding-bottom: 0px;">
								<div id="rev_slider_2_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-source="gallery" style="margin:0px auto;background:#262626;padding:0px;margin-top:0px;margin-bottom:0px;">
									<div id="rev_slider_2_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.5.1">
										<ul>
											<li data-index="rs-4" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="[base_url]theme/theme/upload/slider-1-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description=""> <img src="[base_url]theme/theme/upload/slider-1.jpg" alt="" title="slider-1" width="1800" height="730" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina="">
												<div class="tp-caption   tp-resizeme" id="slide-4-layer-1" data-x="26" data-y="center" data-voffset="-108" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.8;sY:0.8;skX:0;skY:0;opacity:0;","speed":260,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' data-textalign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; white-space: nowrap; font-size: 50px;  font-weight: 400; color: rgba(248,193,44,1);font-family:Hind;">Welcome To</div>
												<div class="tp-caption   tp-resizeme" id="slide-4-layer-2" data-x="18" data-y="center" data-voffset="-3" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"from":"x:-50px;opacity:0;","speed":300,"to":"o:1;","delay":830,"ease":"Power2.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' data-textalign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; white-space: nowrap; font-size: 130px;  font-weight: 700; color: rgba(247,247,247,1);font-family:Hind;">Real</div>
												<div class="tp-caption   tp-resizeme" id="slide-4-layer-3" data-x="278" data-y="center" data-voffset="-5" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"from":"x:50px;opacity:0;","speed":300,"to":"o:1;","delay":1150,"ease":"Power2.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' data-textalign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 7; white-space: nowrap; font-size: 130px;  font-weight: 700; color: rgba(248,193,44,1);font-family:Hind;">Factory</div>
												<div class="tp-caption   tp-resizeme" id="slide-4-layer-4" data-x="27" data-y="center" data-voffset="91" data-width="['666']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":300,"to":"o:1;","delay":1460,"ease":"Power2.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' data-textalign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 8; min-width: 666px; max-width: 666px; white-space: normal; font-size: 20px; line-height: 30px; font-weight: 400; color: rgba(255,255,255,1);font-family:Hind;">Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Maecenas sed diam eget risus varius blandit sit amet non.</div>
												<div class="tp-caption rev-btn " id="slide-4-layer-6" data-x="26" data-y="center" data-voffset="177" data-width="['auto']" data-height="['auto']" data-type="button" data-responsive_offset="on" data-responsive="off" data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":300,"to":"o:1;","delay":1770,"ease":"Power2.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0,0,0,1);bg:rgba(247,204,86,1);"}]' data-textalign="['left','left','left','left']" data-paddingtop="[17,17,17,17]" data-paddingright="[34,34,34,34]" data-paddingbottom="[17,17,17,17]" data-paddingleft="[34,34,34,34]" style="z-index: 9; white-space: nowrap; font-size: 14px; line-height: 17px; font-weight: 600; color: rgba(10,10,10,1);font-family:Hind;background-color:rgba(248,193,44,1);border-radius:3px 3px 3px 3px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Learn More</div>
											</li>
											<li data-index="rs-6" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="[base_url]theme/theme/upload/slider-3-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description=""> <img src="[base_url]theme/theme/upload/slider-3.jpg" alt="" title="slider-3" width="1800" height="827" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina="">
												<div class="tp-caption   tp-resizeme" id="slide-6-layer-3" data-x="24" data-y="center" data-voffset="-12" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"from":"x:50px;opacity:0;","speed":300,"to":"o:1;","delay":340,"ease":"Power2.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' data-textalign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 10; white-space: nowrap; font-size: 70px;  font-weight: 700; color: rgba(248,193,44,1);font-family:Hind;">High Standard Labour</div>
												<div class="tp-caption   tp-resizeme" id="slide-6-layer-4" data-x="27" data-y="center" data-voffset="74" data-width="['666']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":300,"to":"o:1;","delay":640,"ease":"Power2.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' data-textalign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 11; min-width: 666px; max-width: 666px; white-space: normal; font-size: 20px; line-height: 30px; font-weight: 400; color: rgba(255,255,255,1);font-family:Hind;">Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Maecenas sed diam eget risus varius blandit sit amet non.</div>
											</li>
											<li data-index="rs-5" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="[base_url]theme/theme/upload/slider-2-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description=""> <img src="[base_url]theme/theme/upload/slider-2.jpg" alt="" title="slider-2" width="1800" height="827" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina="">
												<div class="tp-caption   tp-resizeme" id="slide-5-layer-2" data-x="24" data-y="center" data-voffset="-93" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"from":"x:-50px;opacity:0;","speed":300,"to":"o:1;","delay":310,"ease":"Power2.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' data-textalign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 12; white-space: nowrap; font-size: 54px;  font-weight: 300; color: rgba(247,247,247,1);font-family:Hind;">Empower Your</div>
												<div class="tp-caption   tp-resizeme" id="slide-5-layer-3" data-x="20" data-y="center" data-voffset="1" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"from":"x:50px;opacity:0;","speed":300,"to":"o:1;","delay":620,"ease":"Power2.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' data-textalign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 13; white-space: nowrap; font-size: 110px;  font-weight: 700; color: rgba(248,193,44,1);font-family:Hind;text-transform:uppercase;">Factory</div>
												<div class="tp-caption   tp-resizeme" id="slide-5-layer-4" data-x="27" data-y="center" data-voffset="81" data-width="['666']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":300,"to":"o:1;","delay":880,"ease":"Power2.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' data-textalign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 14; min-width: 666px; max-width: 666px; white-space: normal; font-size: 20px; line-height: 30px; font-weight: 400; color: rgba(255,255,255,1);font-family:Hind;">Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Maecenas sed diam eget risus varius blandit sit amet non.</div>
												<div class="tp-caption rev-btn " id="slide-5-layer-6" data-x="25" data-y="center" data-voffset="165" data-width="['auto']" data-height="['auto']" data-type="button" data-responsive_offset="on" data-responsive="off" data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":300,"to":"o:1;","delay":1120,"ease":"Power2.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0,0,0,1);bg:rgba(247,204,86,1);"}]' data-textalign="['left','left','left','left']" data-paddingtop="[17,17,17,17]" data-paddingright="[34,34,34,34]" data-paddingbottom="[17,17,17,17]" data-paddingleft="[34,34,34,34]" style="z-index: 15; white-space: nowrap; font-size: 14px; line-height: 17px; font-weight: 600; color: rgba(10,10,10,1);font-family:Hind;background-color:rgba(248,193,44,1);border-radius:3px 3px 3px 3px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Learn More</div>
											</li>
										</ul>
										<div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="gdlr-core-pbf-wrapper " style="padding: 50px 0px 50px 0px;" data-skin="Yellow Service">
				<div class="gdlr-core-pbf-background-wrap" style="background-color: #eab53a ;"></div>
				<div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
					<div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
						<div class="gdlr-core-pbf-element">
							<div class="gdlr-core-call-to-action-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-style-left-align-right-button" style="padding-bottom: 0px;">
								<div class="gdlr-core-call-to-action-item-inner">
									<div class="gdlr-core-call-to-action-item-content-wrap">
										<h3 class="gdlr-core-call-to-action-item-title" style="font-size: 26px;font-weight: 300;letter-spacing: 0px;">We Won Many Factory Awards & Got Many Ceritificates Since 2001-2016</h3>
									</div>
									<div class="gdlr-core-call-to-action-item-button"><a class="gdlr-core-button  gdlr-core-button-solid gdlr-core-button-no-border" href="#" style="border-radius: 3px;-moz-border-radius: 3px;-webkit-border-radius: 3px;"><span class="gdlr-core-content">Learn More</span></a></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="gdlr-core-pbf-wrapper " style="padding: 0px 0px 0px 0px;">
				<div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
					<div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-pbf-wrapper-full-no-space">
						<div class="gdlr-core-pbf-column gdlr-core-column-30 gdlr-core-column-first" data-skin="Dark">
							<div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 170px 70px 110px 100px;" data-sync-height="height-service">
								<div class="gdlr-core-pbf-background-wrap">
									<div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/service-7-1.jpg);background-size: cover;background-position: center;" data-parallax-speed="0.2"></div>
								</div>
								<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content">
									<div class="gdlr-core-pbf-element">
										<div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr">
											<div class="gdlr-core-title-item-title-wrap ">
												<h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 27px;font-weight: 300;letter-spacing: 0px;text-transform: none;color: #f8c12c;">Aero Space<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3>
											</div>
										</div>
									</div>
									<div class="gdlr-core-pbf-element">
										<div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" style="padding-bottom: 0px;">
											<div class="gdlr-core-text-box-item-content" style="font-size: 18px;">
												<p>We are a leading Aerospace steels manufacturer. Our special grade steels are being used in commercial and military aerospace projects around the world.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="gdlr-core-pbf-column gdlr-core-column-15">
							<div class="gdlr-core-pbf-column-content-margin gdlr-core-js " data-sync-height="height-service">
								<div class="gdlr-core-pbf-background-wrap">
									<div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/service-8.jpg);background-size: cover;background-position: center;" data-parallax-speed="0"></div>
								</div>
								<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content"></div>
							</div>
						</div>
						<div class="gdlr-core-pbf-column gdlr-core-column-15">
							<div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 60px 0px 60px 0px;" data-sync-height="height-service" data-sync-height-center="">
								<div class="gdlr-core-pbf-background-wrap" style="background-color: #d8a734 ;"></div>
									<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content">
										<div class="gdlr-core-pbf-element">
											<div class="gdlr-core-image-item gdlr-core-item-pdlr gdlr-core-item-pdb  gdlr-core-center-align" style="padding-bottom: 35px;">
												<div class="gdlr-core-image-item-wrap gdlr-core-media-image  gdlr-core-image-item-style-rectangle" style="border-width: 0px;"><img src="[base_url]theme/theme/upload/about-icon-11.png" alt="" width="104" height="60"></div>
											</div>
										</div>
										<div class="gdlr-core-pbf-element">
											<div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-center-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-bottom: 0px;">
												<div class="gdlr-core-title-item-title-wrap ">
													<h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 18px;font-weight: 300;letter-spacing: 0px;text-transform: none;">Aero Space Parts<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="gdlr-core-pbf-wrapper " style="padding: 0px 0px 0px 0px;">
					<div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
						<div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-pbf-wrapper-full-no-space">
							<div class="gdlr-core-pbf-column gdlr-core-column-15 gdlr-core-column-first">
								<div class="gdlr-core-pbf-column-content-margin gdlr-core-js " data-sync-height="height-service">
									<div class="gdlr-core-pbf-background-wrap">
										<div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/shutterstock_134132600.jpg);background-size: cover;background-position: center;" data-parallax-speed="0"></div>
									</div>
									<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content"></div>
								</div>
							</div>
							<div class="gdlr-core-pbf-column gdlr-core-column-15">
								<div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 60px 0px 60px 0px;" data-sync-height="height-service" data-sync-height-center="">
									<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content">
										<div class="gdlr-core-pbf-element">
											<div class="gdlr-core-image-item gdlr-core-item-pdlr gdlr-core-item-pdb  gdlr-core-center-align" style="padding-bottom: 35px;">
												<div class="gdlr-core-image-item-wrap gdlr-core-media-image  gdlr-core-image-item-style-rectangle" style="border-width: 0px;"><img src="[base_url]theme/theme/upload/about-icon-12.png" alt="" width="72" height="53"></div>
											</div>
										</div>
										<div class="gdlr-core-pbf-element">
											<div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-center-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-bottom: 0px;">
												<div class="gdlr-core-title-item-title-wrap ">
													<h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 18px;font-weight: 300;letter-spacing: 0px;text-transform: none;">Construction Products<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="gdlr-core-pbf-column gdlr-core-column-30" data-skin="Dark">
								<div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 150px 70px 90px 100px;" data-sync-height="height-service">
									<div class="gdlr-core-pbf-background-wrap">
										<div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/service-9-1.jpg);background-size: cover;background-position: center;" data-parallax-speed="0.2"></div>
									</div>
									<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content">
										<div class="gdlr-core-pbf-element">
											<div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr">
												<div class="gdlr-core-title-item-title-wrap ">
													<h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 27px;font-weight: 300;letter-spacing: 0px;text-transform: none;color: #f8c12c;">Construction & Engineering<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3>
												</div>
											</div>
										</div>
									<div class="gdlr-core-pbf-element">
										<div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" style="padding-bottom: 0px;">
											<div class="gdlr-core-text-box-item-content" style="font-size: 18px;">
												<p>We provides a comprehensive range of products and techniques to create value for the construction industry and support sustainable development.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="gdlr-core-pbf-wrapper " style="padding: 0px 0px 0px 0px;">
				<div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
					<div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-pbf-wrapper-full-no-space">
						<div class="gdlr-core-pbf-column gdlr-core-column-30 gdlr-core-column-first" data-skin="Dark">
							<div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 150px 70px 90px 100px;" data-sync-height="height-service">
								<div class="gdlr-core-pbf-background-wrap">
									<div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/service-10-1.jpg);background-size: cover;background-position: center;" data-parallax-speed="0.2"></div>
								</div>
								<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content">
									<div class="gdlr-core-pbf-element">
										<div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr">
											<div class="gdlr-core-title-item-title-wrap ">
												<h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 27px;font-weight: 300;letter-spacing: 0px;text-transform: none;color: #f8c12c;">Railway<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3>
											</div>
											</div>
										</div>
										<div class="gdlr-core-pbf-element">
											<div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" style="padding-bottom: 0px;">
												<div class="gdlr-core-text-box-item-content" style="font-size: 18px;">
													<p>We provides a wide range of high performance rail products and railway infrastructure services to the international rail industry.</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="gdlr-core-pbf-column gdlr-core-column-15" data-skin="White Text">
								<div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 60px 0px 60px 0px;" data-sync-height="height-service" data-sync-height-center="">
									<div class="gdlr-core-pbf-background-wrap" style="background-color: #000000 ;"></div>
										<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content">
											<div class="gdlr-core-pbf-element">
												<div class="gdlr-core-image-item gdlr-core-item-pdlr gdlr-core-item-pdb  gdlr-core-center-align" style="padding-bottom: 35px;">
													<div class="gdlr-core-image-item-wrap gdlr-core-media-image  gdlr-core-image-item-style-rectangle" style="border-width: 0px;"><img src="[base_url]theme/theme/upload/about-icon-13.png" alt="" width="60" height="60"></div>
												</div>
											</div>
											<div class="gdlr-core-pbf-element">
												<div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-center-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-bottom: 0px;">
													<div class="gdlr-core-title-item-title-wrap ">
														<h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 18px;font-weight: 300;letter-spacing: 0px;text-transform: none;">Aero Space Parts<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3>
													</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								<div class="gdlr-core-pbf-column gdlr-core-column-15">
									<div class="gdlr-core-pbf-column-content-margin gdlr-core-js " data-sync-height="height-service">
										<div class="gdlr-core-pbf-background-wrap">
											<div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/train-railway-s-bahn-transport.jpg);background-size: cover;background-position: center;" data-parallax-speed="0"></div>
										</div>
										<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="gdlr-core-pbf-wrapper " style="padding: 95px 0px 50px 0px;">
						<div class="gdlr-core-pbf-background-wrap">
							<div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/about-home-bg.jpg);background-size: cover;background-position: center;" data-parallax-speed="0.6"></div>
						</div>
						<div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
							<div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
								<div class="gdlr-core-pbf-column gdlr-core-column-20 gdlr-core-column-first" data-skin="Yellow Link">
									<div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 0px 10px 0px 10px;">
										<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
											<div class="gdlr-core-pbf-element">
												<div class="gdlr-core-column-service-item gdlr-core-item-pdb  gdlr-core-center-align gdlr-core-no-caption gdlr-core-item-pdlr">
													<div class="gdlr-core-column-service-media gdlr-core-media-image" style="margin-bottom: 40px;"><img src="[base_url]theme/theme/upload/about-icon-8.png" alt="" width="43" height="59"></div>
														<div class="gdlr-core-column-service-content-wrapper">
															<div class="gdlr-core-column-service-title-wrap" style="margin-bottom: 20px;">
																<h3 class="gdlr-core-column-service-title" style="font-size: 21px;font-weight: 300;letter-spacing: 0px;text-transform: none;">The Safest Factory</h3>
															</div>
															<div class="gdlr-core-column-service-content" style="font-size: 17px;">
																<p>tiam porta sem malesuada magna mollis euismod. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p> <a class="gdlr-core-column-service-read-more gdlr-core-info-font" href="#" style="font-size: 16px;">Learn More</a></div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="gdlr-core-pbf-column gdlr-core-column-20" data-skin="Yellow Link">
											<div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 0px 10px 0px 10px;">
												<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
													<div class="gdlr-core-pbf-element">
														<div class="gdlr-core-column-service-item gdlr-core-item-pdb  gdlr-core-center-align gdlr-core-no-caption gdlr-core-item-pdlr">
															<div class="gdlr-core-column-service-media gdlr-core-media-image" style="margin-bottom: 42px;"><img src="[base_url]theme/theme/upload/about-icon-9.png" alt="" width="61" height="57"></div>
																<div class="gdlr-core-column-service-content-wrapper">
																	<div class="gdlr-core-column-service-title-wrap" style="margin-bottom: 20px;">
																	<h3 class="gdlr-core-column-service-title" style="font-size: 21px;font-weight: 300;letter-spacing: 0px;text-transform: none;">Smart Technology</h3>
																</div>
																<div class="gdlr-core-column-service-content" style="font-size: 17px;">
																	<p>tiam porta sem malesuada magna mollis euismod. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p> <a class="gdlr-core-column-service-read-more gdlr-core-info-font" href="#" style="font-size: 16px;">Learn More</a></div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="gdlr-core-pbf-column gdlr-core-column-20" data-skin="Yellow Link">
												<div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 0px 10px 0px 10px;">
													<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
														<div class="gdlr-core-pbf-element">
															<div class="gdlr-core-column-service-item gdlr-core-item-pdb  gdlr-core-center-align gdlr-core-no-caption gdlr-core-item-pdlr">
																<div class="gdlr-core-column-service-media gdlr-core-media-image" style="margin-bottom: 40px;"><img src="[base_url]theme/theme/upload/about-icon-10.png" alt="" width="43" height="59"></div>
																	<div class="gdlr-core-column-service-content-wrapper">
																		<div class="gdlr-core-column-service-title-wrap" style="margin-bottom: 20px;">
																			<h3 class="gdlr-core-column-service-title" style="font-size: 21px;font-weight: 300;letter-spacing: 0px;text-transform: none;">We Use Green Energy</h3>
																		</div>
																		<div class="gdlr-core-column-service-content" style="font-size: 17px;">
																			<p>tiam porta sem malesuada magna mollis euismod. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p> <a class="gdlr-core-column-service-read-more gdlr-core-info-font" href="#" style="font-size: 16px;">Learn More</a></div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="gdlr-core-pbf-wrapper " style="padding: 75px 0px 15px 0px;">
											<div class="gdlr-core-pbf-background-wrap">
												<div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/port-bg-2-1.jpg);background-size: cover;background-position: center;" data-parallax-speed="0.4"></div>
											</div>
											<div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
												<div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
													<div class="gdlr-core-pbf-column gdlr-core-column-15 gdlr-core-column-first">
														<div class="gdlr-core-pbf-column-content-margin gdlr-core-js ">
														<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
														<div class="gdlr-core-pbf-element">
														<div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr">
														<div class="gdlr-core-title-item-title-wrap ">
														<h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 26px;font-weight: 400;letter-spacing: 0px;text-transform: none;color: #f8c12c;">Featured EPC Works<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="gdlr-core-pbf-column gdlr-core-column-30" data-skin="White Text">
									<div class="gdlr-core-pbf-column-content-margin gdlr-core-js ">
										<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
											<div class="gdlr-core-pbf-element">
												<div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align">
													<div class="gdlr-core-text-box-item-content" style="font-size: 16px;">
														<p>We don’t only sell products or part but we are also a well-known
														<br> EPC contractor in the region.</p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="gdlr-core-pbf-column gdlr-core-column-15" data-skin="White Text">
									<div class="gdlr-core-pbf-column-content-margin gdlr-core-js ">
										<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
											<div class="gdlr-core-pbf-element">
												<div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-right-align">
													<div class="gdlr-core-text-box-item-content">
														<p><a href="../portfolio-3-columns/index.htm">More Works <i class="fa fa-long-arrow-right" style="font-size: 16px;color: #fff;margin-left: 15px;"></i></a></p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="gdlr-core-pbf-wrapper " style="padding: 0px 0px 0px 0px;">
						<div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
							<div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-pbf-wrapper-full-no-space">
								<div class="gdlr-core-pbf-column gdlr-core-column-60 gdlr-core-column-first" data-skin="Portfolio Homepage">
									<div class="gdlr-core-pbf-column-content-margin gdlr-core-js " data-sync-height="height-port">
										<div class="gdlr-core-pbf-background-wrap">
											<div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/port-bg.jpg);background-size: cover;background-position: center;" data-parallax-speed="0"></div>
										</div>
										<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content">
											<div class="gdlr-core-pbf-element">
												<div class="gdlr-core-portfolio-item gdlr-core-item-pdb clearfix  gdlr-core-portfolio-item-style-grid-no-space gdlr-core-item-pdlr" style="padding-bottom: 10px;">
													<div class="gdlr-core-flexslider flexslider gdlr-core-js-2 " data-type="carousel" data-column="5" data-nav="navigation" data-nav-parent="gdlr-core-portfolio-item" data-disable-autoslide="1">
														<ul class="slides">
															<li>
																<div class="gdlr-core-portfolio-grid  gdlr-core-left-align gdlr-core-style-normal">
																	<div class="gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-title-icon">
																		<div class="gdlr-core-portfolio-thumbnail-image-wrap  gdlr-core-zoom-on-hover"><img src="[base_url]theme/theme/upload/pexels-photo-109915-500x625.jpeg" alt="" width="500" height="625"><span class="gdlr-core-image-overlay  gdlr-core-portfolio-overlay gdlr-core-image-overlay-center-icon gdlr-core-js"><span class="gdlr-core-image-overlay-content"><span class="gdlr-core-portfolio-title gdlr-core-title-font"><a href="../#">Muchen Railway Station</a></span><span class="gdlr-core-portfolio-icon-wrap"><a class="gdlr-core-ilightbox gdlr-core-js " href="[base_url]theme/theme/upload/pexels-photo-109915.jpeg" data-ilightbox-group="gdlr-core-img-group-1"><i class="gdlr-core-portfolio-icon icon_zoom-in_alt"></i></a></span></span></span></div>
																	</div>
																	<div class="gdlr-core-portfolio-content-wrap gdlr-core-skin-divider">
																		<h3 class="gdlr-core-portfolio-title gdlr-core-skin-title" style="font-size: 18px;font-weight: 600;letter-spacing: 0px;text-transform: none;"><a href="../#">Muchen Railway Station</a></h3><span class="gdlr-core-portfolio-info gdlr-core-portfolio-info-tag gdlr-core-info-font gdlr-core-skin-caption"><a href="../#" rel="tag">Muchen</a> <span class="gdlr-core-sep">/</span> <a href="../#" rel="tag">Railway</a></span>
																	</div>
																</div>
															</li>
															<li>
																<div class="gdlr-core-portfolio-grid  gdlr-core-left-align gdlr-core-style-normal">
																	<div class="gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-title-icon">
																		<div class="gdlr-core-portfolio-thumbnail-image-wrap  gdlr-core-zoom-on-hover"><img src="[base_url]theme/theme/upload/shutterstock_57862405-500x625.jpg" alt="" width="500" height="625"><span class="gdlr-core-image-overlay  gdlr-core-portfolio-overlay gdlr-core-image-overlay-center-icon gdlr-core-js"><span class="gdlr-core-image-overlay-content"><span class="gdlr-core-portfolio-title gdlr-core-title-font"><a href="../#">USA Bank Building</a></span><span class="gdlr-core-portfolio-icon-wrap"><a class="gdlr-core-ilightbox gdlr-core-js " href="[base_url]theme/theme/upload/shutterstock_57862405.jpg" data-ilightbox-group="gdlr-core-img-group-1"><i class="gdlr-core-portfolio-icon icon_zoom-in_alt"></i></a></span></span></span></div>
																	</div>
																	<div class="gdlr-core-portfolio-content-wrap gdlr-core-skin-divider">
																		<h3 class="gdlr-core-portfolio-title gdlr-core-skin-title" style="font-size: 18px;font-weight: 600;letter-spacing: 0px;text-transform: none;"><a href="../#">USA Bank Building</a></h3><span class="gdlr-core-portfolio-info gdlr-core-portfolio-info-tag gdlr-core-info-font gdlr-core-skin-caption"><a href="../#" rel="tag">Bank</a> <span class="gdlr-core-sep">/</span> <a href="../#" rel="tag">System</a></span>
																	</div>
																</div>
															</li>
															<li>
																<div class="gdlr-core-portfolio-grid  gdlr-core-left-align gdlr-core-style-normal">
																	<div class="gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-title-icon">
																		<div class="gdlr-core-portfolio-thumbnail-image-wrap  gdlr-core-zoom-on-hover"><img src="[base_url]theme/theme/upload/shutterstock_130285502-500x625.jpg" alt="" width="500" height="625"><span class="gdlr-core-image-overlay  gdlr-core-portfolio-overlay gdlr-core-image-overlay-center-icon gdlr-core-js"><span class="gdlr-core-image-overlay-content"><span class="gdlr-core-portfolio-title gdlr-core-title-font"><a href="../#">Mining Plant Set Up</a></span><span class="gdlr-core-portfolio-icon-wrap"><a class="gdlr-core-ilightbox gdlr-core-js " href="[base_url]theme/theme/upload/shutterstock_130285502.jpg" data-ilightbox-group="gdlr-core-img-group-1"><i class="gdlr-core-portfolio-icon icon_zoom-in_alt"></i></a></span></span></span></div>
																	</div>
																	<div class="gdlr-core-portfolio-content-wrap gdlr-core-skin-divider">
																		<h3 class="gdlr-core-portfolio-title gdlr-core-skin-title" style="font-size: 18px;font-weight: 600;letter-spacing: 0px;text-transform: none;"><a href="../#">Mining Plant Set Up</a></h3><span class="gdlr-core-portfolio-info gdlr-core-portfolio-info-tag gdlr-core-info-font gdlr-core-skin-caption"><a href="../#" rel="tag">Mining</a> <span class="gdlr-core-sep">/</span> <a href="../#" rel="tag">Plants</a></span>
																	</div>
																</div>
															</li>
															<li>
																<div class="gdlr-core-portfolio-grid  gdlr-core-left-align gdlr-core-style-normal">
																	<div class="gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-title-icon">
																		<div class="gdlr-core-portfolio-thumbnail-image-wrap  gdlr-core-zoom-on-hover"><img src="[base_url]theme/theme/upload/shutterstock_95662675-500x625.jpg" alt="" width="500" height="625"><span class="gdlr-core-image-overlay  gdlr-core-portfolio-overlay gdlr-core-image-overlay-center-icon gdlr-core-js"><span class="gdlr-core-image-overlay-content"><span class="gdlr-core-portfolio-title gdlr-core-title-font"><a href="../#">Apple&#8217;s Server Room</a></span><span class="gdlr-core-portfolio-icon-wrap"><a class="gdlr-core-ilightbox gdlr-core-js " href="[base_url]theme/theme/upload/shutterstock_95662675.jpg" data-ilightbox-group="gdlr-core-img-group-1"><i class="gdlr-core-portfolio-icon icon_zoom-in_alt"></i></a></span></span></span></div>
																	</div>
																	<div class="gdlr-core-portfolio-content-wrap gdlr-core-skin-divider">
																		<h3 class="gdlr-core-portfolio-title gdlr-core-skin-title" style="font-size: 18px;font-weight: 600;letter-spacing: 0px;text-transform: none;"><a href="../#">Apple&#8217;s Server Room</a></h3><span class="gdlr-core-portfolio-info gdlr-core-portfolio-info-tag gdlr-core-info-font gdlr-core-skin-caption"><a href="../#" rel="tag">Data</a> <span class="gdlr-core-sep">/</span> <a href="../#" rel="tag">Server</a></span>
																	</div>
																</div>
															</li>
															<li>
																<div class="gdlr-core-portfolio-grid  gdlr-core-left-align gdlr-core-style-normal">
																	<div class="gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-title-icon">
																		<div class="gdlr-core-portfolio-thumbnail-image-wrap  gdlr-core-zoom-on-hover"><img src="[base_url]theme/theme/upload/landmark-bridge-metal-architecture-500x625.jpg" alt="" width="500" height="625"><span class="gdlr-core-image-overlay  gdlr-core-portfolio-overlay gdlr-core-image-overlay-center-icon gdlr-core-js"><span class="gdlr-core-image-overlay-content"><span class="gdlr-core-portfolio-title gdlr-core-title-font"><a href="../#">Sanfran Cisco Bridge</a></span><span class="gdlr-core-portfolio-icon-wrap"><a class="gdlr-core-ilightbox gdlr-core-js " href="[base_url]theme/theme/upload/landmark-bridge-metal-architecture.jpg" data-ilightbox-group="gdlr-core-img-group-1"><i class="gdlr-core-portfolio-icon icon_zoom-in_alt"></i></a></span></span></span></div>
																	</div>
																	<div class="gdlr-core-portfolio-content-wrap gdlr-core-skin-divider">
																		<h3 class="gdlr-core-portfolio-title gdlr-core-skin-title" style="font-size: 18px;font-weight: 600;letter-spacing: 0px;text-transform: none;"><a href="../#">Sanfran Cisco Bridge</a></h3><span class="gdlr-core-portfolio-info gdlr-core-portfolio-info-tag gdlr-core-info-font gdlr-core-skin-caption"><a href="../#" rel="tag">Bridge</a> <span class="gdlr-core-sep">/</span> <a href="../#" rel="tag">Engineering</a></span>
																	</div>
																</div>
															</li>
															<li>
																<div class="gdlr-core-portfolio-grid  gdlr-core-left-align gdlr-core-style-normal">
																	<div class="gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-title-icon">
																		<div class="gdlr-core-portfolio-thumbnail-image-wrap  gdlr-core-zoom-on-hover"><img src="[base_url]theme/theme/upload/shutterstock_161515241-500x625.jpg" alt="" width="500" height="625"><span class="gdlr-core-image-overlay  gdlr-core-portfolio-overlay gdlr-core-image-overlay-center-icon gdlr-core-js"><span class="gdlr-core-image-overlay-content"><span class="gdlr-core-portfolio-title gdlr-core-title-font"><a href="../#">Hamburg Wind Energy Plant</a></span><span class="gdlr-core-portfolio-icon-wrap"><a class="gdlr-core-ilightbox gdlr-core-js " href="[base_url]theme/theme/upload/shutterstock_161515241.jpg" data-ilightbox-group="gdlr-core-img-group-1"><i class="gdlr-core-portfolio-icon icon_zoom-in_alt"></i></a></span></span></span></div>
																	</div>
																	<div class="gdlr-core-portfolio-content-wrap gdlr-core-skin-divider">
																		<h3 class="gdlr-core-portfolio-title gdlr-core-skin-title" style="font-size: 18px;font-weight: 600;letter-spacing: 0px;text-transform: none;"><a href="../#">Hamburg Wind Energy Plant</a></h3><span class="gdlr-core-portfolio-info gdlr-core-portfolio-info-tag gdlr-core-info-font gdlr-core-skin-caption"><a href="../#" rel="tag">Energy</a> <span class="gdlr-core-sep">/</span> <a href="../#" rel="tag">System</a></span>
																	</div>
																</div>
															</li>
															<li>
																<div class="gdlr-core-portfolio-grid  gdlr-core-left-align gdlr-core-style-normal">
																	<div class="gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-title-icon">
																		<div class="gdlr-core-portfolio-thumbnail-image-wrap  gdlr-core-zoom-on-hover"><img src="[base_url]theme/theme/upload/pexels-photo-24276-500x625.jpg" alt="" width="500" height="625"><span class="gdlr-core-image-overlay  gdlr-core-portfolio-overlay gdlr-core-image-overlay-center-icon gdlr-core-js"><span class="gdlr-core-image-overlay-content"><span class="gdlr-core-portfolio-title gdlr-core-title-font"><a href="../#">Singapore Logistic Port</a></span><span class="gdlr-core-portfolio-icon-wrap"><a class="gdlr-core-ilightbox gdlr-core-js " href="[base_url]theme/theme/upload/pexels-photo-24276.jpg" data-ilightbox-group="gdlr-core-img-group-1"><i class="gdlr-core-portfolio-icon icon_zoom-in_alt"></i></a></span></span></span></div>
																	</div>
																	<div class="gdlr-core-portfolio-content-wrap gdlr-core-skin-divider">
																		<h3 class="gdlr-core-portfolio-title gdlr-core-skin-title" style="font-size: 18px;font-weight: 600;letter-spacing: 0px;text-transform: none;"><a href="../#">Singapore Logistic Port</a></h3><span class="gdlr-core-portfolio-info gdlr-core-portfolio-info-tag gdlr-core-info-font gdlr-core-skin-caption"><a href="../#" rel="tag">Logistic</a> <span class="gdlr-core-sep">/</span> <a href="../#" rel="tag">Port</a></span>
																	</div>
																</div>
															</li>
															<li>
																<div class="gdlr-core-portfolio-grid  gdlr-core-left-align gdlr-core-style-normal">
																	<div class="gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-title-icon">
																		<div class="gdlr-core-portfolio-thumbnail-image-wrap  gdlr-core-zoom-on-hover"><img src="[base_url]theme/theme/upload/pexels-photo-3-500x625.jpg" alt="" width="500" height="625"><span class="gdlr-core-image-overlay  gdlr-core-portfolio-overlay gdlr-core-image-overlay-center-icon gdlr-core-js"><span class="gdlr-core-image-overlay-content"><span class="gdlr-core-portfolio-title gdlr-core-title-font"><a href="../#">Berlin Central Bank</a></span><span class="gdlr-core-portfolio-icon-wrap"><a class="gdlr-core-ilightbox gdlr-core-js " href="[base_url]theme/theme/upload/pexels-photo-3.jpg" data-ilightbox-group="gdlr-core-img-group-1"><i class="gdlr-core-portfolio-icon icon_zoom-in_alt"></i></a></span></span></span></div>
																	</div>
																	<div class="gdlr-core-portfolio-content-wrap gdlr-core-skin-divider">
																		<h3 class="gdlr-core-portfolio-title gdlr-core-skin-title" style="font-size: 18px;font-weight: 600;letter-spacing: 0px;text-transform: none;"><a href="../#">Berlin Central Bank</a></h3><span class="gdlr-core-portfolio-info gdlr-core-portfolio-info-tag gdlr-core-info-font gdlr-core-skin-caption"><a href="../#" rel="tag">Bank</a> <span class="gdlr-core-sep">/</span> <a href="../#" rel="tag">Constructions</a></span>
																	</div>
																</div>
															</li>
															<li>
																<div class="gdlr-core-portfolio-grid  gdlr-core-left-align gdlr-core-style-normal">
																	<div class="gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-title-icon">
																		<div class="gdlr-core-portfolio-thumbnail-image-wrap  gdlr-core-zoom-on-hover"><img src="[base_url]theme/theme/upload/aircraft-manchester-jet-fly-500x625.jpg" alt="" width="500" height="625"><span class="gdlr-core-image-overlay  gdlr-core-portfolio-overlay gdlr-core-image-overlay-center-icon gdlr-core-js"><span class="gdlr-core-image-overlay-content"><span class="gdlr-core-portfolio-title gdlr-core-title-font"><a href="../#">New York Airport System Integration</a></span><span class="gdlr-core-portfolio-icon-wrap"><a class="gdlr-core-ilightbox gdlr-core-js " href="[base_url]theme/theme/upload/aircraft-manchester-jet-fly.jpg" data-ilightbox-group="gdlr-core-img-group-1"><i class="gdlr-core-portfolio-icon icon_zoom-in_alt"></i></a></span></span></span></div>
																	</div>
																	<div class="gdlr-core-portfolio-content-wrap gdlr-core-skin-divider">
																		<h3 class="gdlr-core-portfolio-title gdlr-core-skin-title" style="font-size: 18px;font-weight: 600;letter-spacing: 0px;text-transform: none;"><a href="../#">New York Airport System Integration</a></h3><span class="gdlr-core-portfolio-info gdlr-core-portfolio-info-tag gdlr-core-info-font gdlr-core-skin-caption"><a href="../#" rel="tag">Airport</a> <span class="gdlr-core-sep">/</span> <a href="../#" rel="tag">Constructions</a></span>
																	</div>
																</div>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="gdlr-core-pbf-wrapper " style="padding: 45px 0px 15px 0px;">
						<div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
							<div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
								<div class="gdlr-core-pbf-element">
									<div class="gdlr-core-gallery-item gdlr-core-item-pdb clearfix  gdlr-core-gallery-item-style-grid gdlr-core-item-pdlr ">
										<div class="gdlr-core-flexslider flexslider gdlr-core-js-2 " data-type="carousel" data-column="5" data-nav="none" data-nav-parent="gdlr-core-blog-item">
											<ul class="slides">
												<li class="gdlr-core-item-mglr">
													<div class="gdlr-core-gallery-list gdlr-core-media-image">
														<a href="#" target="_self"><img src="[base_url]theme/theme/upload/banner-1.jpg" alt="" width="420" height="240"></a>
													</div>
												</li>
												<li class="gdlr-core-item-mglr">
													<div class="gdlr-core-gallery-list gdlr-core-media-image">
														<a href="#" target="_self"><img src="[base_url]theme/theme/upload/banner-2.jpg" alt="" width="420" height="240"></a>
													</div>
												</li>
												<li class="gdlr-core-item-mglr">
													<div class="gdlr-core-gallery-list gdlr-core-media-image">
														<a href="#" target="_self"><img src="[base_url]theme/theme/upload/banner-3.jpg" alt="" width="420" height="240"></a>
													</div>
												</li>
												<li class="gdlr-core-item-mglr">
													<div class="gdlr-core-gallery-list gdlr-core-media-image">
														<a href="#" target="_self"><img src="[base_url]theme/theme/upload/banner-4.jpg" alt="" width="420" height="240"></a>
													</div>
												</li>
												<li class="gdlr-core-item-mglr">
													<div class="gdlr-core-gallery-list gdlr-core-media-image">
														<a href="#" target="_self"><img src="[base_url]theme/theme/upload/banner-5.jpg" alt="" width="420" height="240"></a>
													</div>
												</li>
												<li class="gdlr-core-item-mglr">
													<div class="gdlr-core-gallery-list gdlr-core-media-image">
														<a href="#" target="_self"><img src="[base_url]theme/theme/upload/banner-6.jpg" alt="" width="420" height="240"></a>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="gdlr-core-pbf-wrapper " style="padding: 170px 0px 160px 0px;" data-skin="Dark">
						<div class="gdlr-core-pbf-background-wrap">
							<div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/service-11-1.jpg);background-size: cover;background-position: center;" data-parallax-speed="0.2"></div>
						</div>
						<div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
							<div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
								<div class="gdlr-core-pbf-column gdlr-core-column-30 gdlr-core-column-first">
									<div class="gdlr-core-pbf-column-content-margin gdlr-core-js ">
										<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js "></div>
									</div>
								</div>
								<div class="gdlr-core-pbf-column gdlr-core-column-30">
									<div class="gdlr-core-pbf-column-content-margin gdlr-core-js ">
										<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
											<div class="gdlr-core-pbf-element">
												<div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-bottom: 35px;">
													<div class="gdlr-core-title-item-title-wrap ">
														<h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 34px;font-weight: 300;letter-spacing: 0px;text-transform: none;color: #ffffff;">We Won Many Factory Awards & Got  Many Ceritificates Since 2001-2016<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3></div>
													</div>
												</div>
												<div class="gdlr-core-pbf-element">
													<div class="gdlr-core-button-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" style="padding-bottom: 0px;"><a class="gdlr-core-button  gdlr-core-button-solid gdlr-core-button-no-border" href="#" style="font-size: 14px;color: #141414;border-radius: 3px;-moz-border-radius: 3px;-webkit-border-radius: 3px;background: #e0b731 ;"><span class="gdlr-core-content">Learn More</span></a></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="gdlr-core-pbf-wrapper " style="padding: 0px 0px 0px 0px;">
							<div class="gdlr-core-pbf-background-wrap" style="background-color: #ececec ;"></div>
								<div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
									<div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
										<div class="gdlr-core-pbf-column gdlr-core-column-20 gdlr-core-column-first">
											<div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 85px 6px 0px 0px;" data-sync-height="height-financial">
												<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content">
													<div class="gdlr-core-pbf-element">
														<div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-bottom: 39px;">
															<div class="gdlr-core-title-item-title-wrap ">
																<h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 28px;font-weight: 500;letter-spacing: 0px;text-transform: none;color: #141414;">Financial Results<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3></div>
															</div>
														</div>
														<div class="gdlr-core-pbf-element">
															<div class="gdlr-core-icon-list-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-with-divider">
																<ul>
																	<li class=" gdlr-core-skin-divider gdlr-core-with-hover" style="border-color: #d8d8d8;margin-bottom: 3px;"><a href="#" target="_self"><span class="gdlr-core-icon-list-icon-wrap"><i class="gdlr-core-icon-list-icon-hover icon_check_alt2" style="font-size: 16px;width: 16px;"></i><i class="gdlr-core-icon-list-icon icon_check_alt2" style="font-size: 16px;width: 16px;"></i></span><span class="gdlr-core-icon-list-content" style="font-size: 15px;">Q2 2019 Press Release</span></a></li>
																	<li class=" gdlr-core-skin-divider gdlr-core-with-hover" style="border-color: #d8d8d8;margin-bottom: 3px;"><a href="#" target="_self"><span class="gdlr-core-icon-list-icon-wrap"><i class="gdlr-core-icon-list-icon-hover icon_check_alt2" style="font-size: 16px;width: 16px;"></i><i class="gdlr-core-icon-list-icon icon_check_alt2" style="font-size: 16px;width: 16px;"></i></span><span class="gdlr-core-icon-list-content" style="font-size: 15px;">Q2 2019 Presentation</span></a></li>
																	<li class=" gdlr-core-skin-divider gdlr-core-with-hover" style="border-color: #d8d8d8;margin-bottom: 3px;"><a href="#" target="_self"><span class="gdlr-core-icon-list-icon-wrap"><i class="gdlr-core-icon-list-icon-hover icon_check_alt2" style="font-size: 16px;width: 16px;"></i><i class="gdlr-core-icon-list-icon icon_check_alt2" style="font-size: 16px;width: 16px;"></i></span><span class="gdlr-core-icon-list-content" style="font-size: 15px;">H2 2019 Half Year Financial Report</span></a></li>
																</ul>
															</div>
														</div>
														<div class="gdlr-core-pbf-element">
															<div class="gdlr-core-image-item gdlr-core-item-pdlr gdlr-core-item-pdb  gdlr-core-center-align" style="padding-bottom: 25px;">
																<div class="gdlr-core-image-item-wrap gdlr-core-media-image  gdlr-core-image-item-style-rectangle" style="border-width: 0px;"><img src="[base_url]theme/theme/upload/shutterstock_207507838-800x420.jpg" alt="" width="800" height="420"></div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="gdlr-core-pbf-column gdlr-core-column-20">
												<div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 85px 30px 40px 10px;" data-sync-height="height-financial">
													<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content">
														<div class="gdlr-core-pbf-element">
															<div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-bottom: 55px;" id="gdlr-core-title-item-id-57381">
																<div class="gdlr-core-title-item-title-wrap  gdlr-core-js-2 gdlr-core-with-link-text">
																	<h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 28px;font-weight: 500;letter-spacing: 0px;text-transform: none;color: #141414;">
																		Contactenos
																		<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span>
																	</h3>
																</div>
															</div>
														</div>


														<div class="gdlr-core-pbf-element">
															<div class="gdlr-core-blog-item gdlr-core-item-pdb clearfix  gdlr-core-style-blog-list">
																<div class="gdlr-core-blog-item-holder gdlr-core-js-2 clearfix" data-layout="fitrows">
																	

																	<form action="" onsubmit="return false">
																		<div class="gdlr-core-item-list gdlr-core-blog-list gdlr-core-item-pdlr  gdlr-core-blog-list-with-frame">
																			<div class="gdlr-core-blog-list-frame gdlr-core-skin-e-background gdlr-core-input-wrap gdlr-core-large gdlr-core-full-width gdlr-core-with-column">

																				<span class="quform-element wpcf7-form-control-wrap">
																					<h3 class="gdlr-core-blog-title gdlr-core-skin-title">
																						Nombre
																					</h3>
																					<input type="text" class="wpcf7-form-control wpcf7-text wpcf7" name="nombre" placeholder="Nombre">
																				</span>
																			</div>
																		

																		
																			<div class="gdlr-core-blog-list-frame gdlr-core-skin-e-background gdlr-core-input-wrap gdlr-core-large gdlr-core-full-width gdlr-core-with-column">

																				<span class="quform-element wpcf7-form-control-wrap">
																					<h3 class="gdlr-core-blog-title gdlr-core-skin-title">
																						Email
																					</h3>
																					<input type="text" class="wpcf7-form-control wpcf7-text wpcf7" name="nombre" placeholder="Email">
																				</span>
																			</div>
																		

																		
																			<div class="gdlr-core-blog-list-frame gdlr-core-skin-e-background gdlr-core-input-wrap gdlr-core-large gdlr-core-full-width gdlr-core-with-column">

																				<span class="quform-element wpcf7-form-control-wrap">
																					<h3 class="gdlr-core-blog-title gdlr-core-skin-title">
																						Teléfono
																					</h3>
																					<input type="text" class="wpcf7-form-control wpcf7-text wpcf7" name="nombre" placeholder="Teléfono">
																				</span>
																			</div>
																		

																		
																			<div class="gdlr-core-blog-list-frame gdlr-core-skin-e-background gdlr-core-input-wrap gdlr-core-large gdlr-core-full-width gdlr-core-with-column">

																				<span class="quform-element wpcf7-form-control-wrap">
																					<h3 class="gdlr-core-blog-title gdlr-core-skin-title">
																						Mensaje
																					</h3>
																					<textarea class="wpcf7-form-control wpcf7-text wpcf7" name="nombre" placeholder="Mensaje"></textarea>
																				</span>
																			</div>
																		

																		
																			<div class="gdlr-core-blog-list-frame gdlr-core-skin-e-background gdlr-core-input-wrap gdlr-core-large gdlr-core-full-width gdlr-core-with-column">

																				<div class="quform-submit">
			                                                                        <div class="quform-submit-inner">
			                                                                            <button type="submit" class="submit-button"><span><em>Enviar</em></span></button>
			                                                                        </div>
			                                                                        <div class="quform-loading-wrap"><span class="quform-loading"></span></div>
			                                                                    </div>
																			</div>
																		</div>
																	</form>


																</div>
															</div>
														</div>


													</div>
												</div>
											</div>
												<div class="gdlr-core-pbf-column gdlr-core-column-20" data-skin="Dark">
													<div class="gdlr-core-pbf-column-content-margin gdlr-core-js  gdlr-core-column-extend-right" style="padding: 80px 0px 40px 45px;" data-sync-height="height-financial">
														<div class="gdlr-core-pbf-background-wrap">
															<div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/career-bg.jpg);background-size: cover;background-position: center;" data-parallax-speed="0.2"></div>
														</div>
														<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content">
															<div class="gdlr-core-pbf-element">
																<div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-bottom: 45px;">
																	<div class="gdlr-core-title-item-title-wrap ">
																		<h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 35px;font-weight: 300;letter-spacing: 0px;text-transform: none;color: #f8c12c;">Career & Jobs<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3>
																	</div>
																</div>
															</div>
															<div class="gdlr-core-pbf-element">
																<div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align">
																	<div class="gdlr-core-text-box-item-content" style="font-size: 18px;">
																		<p>Working at Real Factory means being able to commit resolutely to one thing and growing from it. As an individual and as a whole – in a family enterprise that creates fascinating products around the world. Once you experience it, you&#8217;ll want to stay.</p>
																	</div>
																</div>
															</div>
															<div class="gdlr-core-pbf-element">
																<div class="gdlr-core-button-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align"><a class="gdlr-core-button  gdlr-core-button-transparent gdlr-core-button-with-border" href="#" style="font-size: 15px;color: #f8c12c;padding: 15px 28px 15px 28px;border-radius: 3px;-moz-border-radius: 3px;-webkit-border-radius: 3px;border-width: 2px 2px 2px 2px;border-color: #f8c12c;"><span class="gdlr-core-content">See Opened Positions</span></a></div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							[footer]
						</div>
					</div>