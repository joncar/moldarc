[menu]
<div class="realfactory-page-wrapper" id="realfactory-page-wrapper">
	<div class="gdlr-core-page-builder-body">

		<div class="realfactory-page-title-wrap  realfactory-style-custom realfactory-left-align">
                <div class="realfactory-header-transparent-substitute"></div>
                <div class="realfactory-page-title-overlay"></div>
                <div class="realfactory-page-title-container realfactory-container">
                    <div class="realfactory-page-title-content realfactory-item-pdlr">
                        <h1 class="realfactory-page-title">Automotive Parts &#038; System</h1>
                        <div class="realfactory-page-caption">Caption aligned here</div>
                    </div>
                </div>
            </div>
            <div class="realfactory-breadcrumbs">
                <div class="realfactory-breadcrumbs-container realfactory-container">
                    <div class="realfactory-breadcrumbs-item realfactory-item-pdlr"> <span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to Real Factory." href="#" class="home"><span property="name">Home</span></a>
                        <meta property="position" content="1">
                        </span><i class="fa fa-angle-right"></i> <span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to Market Sectors." href="index.htm" class="post post-page"><span property="name">Market Sectors</span></a>
                        <meta property="position" content="2">
                        </span><i class="fa fa-angle-right"></i> <span property="itemListElement" typeof="ListItem"><span property="name">Automotive Parts &#038; System</span>
                        <meta property="position" content="3">
                        </span>
                    </div>
                </div>
            </div>
            <div class="realfactory-page-wrapper" id="realfactory-page-wrapper">
                <div class="gdlr-core-page-builder-body">
                    <div class="gdlr-core-pbf-sidebar-wrapper ">
                        <div class="gdlr-core-pbf-sidebar-container gdlr-core-line-height-0 clearfix gdlr-core-js gdlr-core-container">
                            <div class="gdlr-core-pbf-sidebar-content  gdlr-core-column-45 gdlr-core-pbf-sidebar-padding gdlr-core-line-height gdlr-core-column-extend-right" style="padding: 60px 0px 30px 30px;">
                                <div class="gdlr-core-pbf-sidebar-content-inner">
                                    <div class="gdlr-core-pbf-element">
                                        <div class="gdlr-core-center-align" style="padding-bottom: 40px;">
                                            <div class="gdlr-core-image-item-wrap " style="border-width: 0px;">
                                                <div class="twentytwenty-container">
                                                  <img src="[base_url]theme/theme/upload/shutterstock_134132600.jpg" style="width:100%;" />
                                                  <img src="http://placehold.it/1804x1200" style="width:100%;" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="gdlr-core-pbf-column gdlr-core-column-30 gdlr-core-column-first">
                                        <div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 10px 0px 0px 0px;">
                                            <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
                                                <div class="gdlr-core-pbf-element">
                                                    <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr">
                                                        <div class="gdlr-core-title-item-title-wrap ">
                                                            <h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 27px;font-weight: 700;letter-spacing: 0px;text-transform: none;color: #181818;">
                                                                Construction & Engineering
                                                                <span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider">
                                                                    
                                                                </span>
                                                            </h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="gdlr-core-pbf-column gdlr-core-column-30">
                                        <div class="gdlr-core-pbf-column-content-margin gdlr-core-js ">
                                            <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
                                                <div class="gdlr-core-pbf-element">
                                                    <div class="gdlr-core-button-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-right-align"><a class="gdlr-core-button  gdlr-core-button-transparent gdlr-core-button-with-border" href="#" id="gdlr-core-button-id-93432"><i class="gdlr-core-pos-left fa fa-file-word-o"></i><span class="gdlr-core-content">Download Doc</span></a><a class="gdlr-core-button  gdlr-core-button-transparent gdlr-core-button-with-border" href="#" id="gdlr-core-button-id-53822"><i class="gdlr-core-pos-left fa fa-file-pdf-o"></i><span class="gdlr-core-content">Download PDF</span></a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="gdlr-core-pbf-column gdlr-core-column-60 gdlr-core-column-first">
                                        <div class="gdlr-core-pbf-column-content-margin gdlr-core-js ">
                                            <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
                                                <div class="gdlr-core-pbf-element">
                                                    <div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align">
                                                        <div class="gdlr-core-text-box-item-content">
                                                            <p>Etiam porta sem malesuada magna mollis euismod. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Maecenas faucibus mollis interdum. Donec id elit non mi porta gravida at eget metus. Aenean lacinia. Donec ullamcorper nulla non metus auctor fringilla. Donec ullamcorper nulla non metus auctor fringilla. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Aenean lacinia bibendum nulla sed consectetur. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Nulla vitae elit libero, a pharetra augue. Sed posuere consectetur est at lobortis. Sed posuere consectetur est at lobortis.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="gdlr-core-pbf-column gdlr-core-column-36 gdlr-core-column-first">
                                        <div class="gdlr-core-pbf-column-content-margin gdlr-core-js ">
                                            <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
                                                <div class="gdlr-core-pbf-element">
                                                    <div class="gdlr-core-accordion-item gdlr-core-item-pdlr gdlr-core-item-pdb  gdlr-core-accordion-style-box-icon">
                                                        <div class="gdlr-core-accordion-item-tab clearfix  gdlr-core-active">
                                                            <div class="gdlr-core-accordion-item-icon gdlr-core-js gdlr-core-skin-icon  gdlr-core-skin-e-background gdlr-core-skin-border"></div>
                                                            <div class="gdlr-core-accordion-item-content-wrapper">
                                                                <h4 class="gdlr-core-accordion-item-title gdlr-core-js ">Mollis Pharetra Euismod Tellus Fermentum</h4>
                                                                <div class="gdlr-core-accordion-item-content">
                                                                    <p>Etiam porta sem malesuada magna mollis euismod. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Maecenas faucibus mollis interdum. Donec id elit non mi porta gravida at eget metus. Aenean lacinia. Donec ullamcorper nulla non metus auctor fringilla. Donec ullamcorper nulla non metus auctor fringilla. Aenean eu leo quam. Pellentesque o.rnare sem lacinia quam.</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="gdlr-core-accordion-item-tab clearfix ">
                                                            <div class="gdlr-core-accordion-item-icon gdlr-core-js gdlr-core-skin-icon  gdlr-core-skin-e-background gdlr-core-skin-border"></div>
                                                            <div class="gdlr-core-accordion-item-content-wrapper">
                                                                <h4 class="gdlr-core-accordion-item-title gdlr-core-js ">Vulputate Sem Pellentesque Adipiscing</h4>
                                                                <div class="gdlr-core-accordion-item-content">
                                                                    <p>Etiam porta sem malesuada magna mollis euismod. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Maecenas faucibus mollis interdum. Donec id elit non mi porta gravida at eget metus. Aenean lacinia. Donec ullamcorper nulla non metus auctor fringilla. Donec ullamcorper nulla non metus auctor fringilla. Aenean eu leo quam. Pellentesque o.rnare sem lacinia quam.</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="gdlr-core-accordion-item-tab clearfix ">
                                                            <div class="gdlr-core-accordion-item-icon gdlr-core-js gdlr-core-skin-icon  gdlr-core-skin-e-background gdlr-core-skin-border"></div>
                                                            <div class="gdlr-core-accordion-item-content-wrapper">
                                                                <h4 class="gdlr-core-accordion-item-title gdlr-core-js ">Cursus Sit Tortor Ligula Nullam</h4>
                                                                <div class="gdlr-core-accordion-item-content">
                                                                    <p>Etiam porta sem malesuada magna mollis euismod. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Maecenas faucibus mollis interdum. Donec id elit non mi porta gravida at eget metus. Aenean lacinia. Donec ullamcorper nulla non metus auctor fringilla. Donec ullamcorper nulla non metus auctor fringilla. Aenean eu leo quam. Pellentesque o.rnare sem lacinia quam.</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="gdlr-core-accordion-item-tab clearfix ">
                                                            <div class="gdlr-core-accordion-item-icon gdlr-core-js gdlr-core-skin-icon  gdlr-core-skin-e-background gdlr-core-skin-border"></div>
                                                            <div class="gdlr-core-accordion-item-content-wrapper">
                                                                <h4 class="gdlr-core-accordion-item-title gdlr-core-js ">Fermentum Vestibulum Purus</h4>
                                                                <div class="gdlr-core-accordion-item-content">
                                                                    <p>Etiam porta sem malesuada magna mollis euismod. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Maecenas faucibus mollis interdum. Donec id elit non mi porta gravida at eget metus. Aenean lacinia. Donec ullamcorper nulla non metus auctor fringilla. Donec ullamcorper nulla non metus auctor fringilla. Aenean eu leo quam. Pellentesque o.rnare sem lacinia quam.</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="gdlr-core-pbf-column gdlr-core-column-24">
                                        <div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 0px 0px 20px 0px;">
                                            <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
                                                <div class="gdlr-core-pbf-element">
                                                    <div class="gdlr-core-image-item gdlr-core-item-pdlr gdlr-core-item-pdb  gdlr-core-center-align">
                                                        <div class="gdlr-core-image-item-wrap gdlr-core-media-image  gdlr-core-image-item-style-rectangle" style="border-width: 0px;">
                                                            <a class="gdlr-core-ilightbox gdlr-core-js " href="[base_url]theme/theme/upload/shutterstock_57862405.jpg"><img src="[base_url]theme/theme/upload/shutterstock_57862405-920x415.jpg" alt="" width="920" height="415"><span class="gdlr-core-image-overlay "><i class="gdlr-core-image-overlay-icon fa fa-search gdlr-core-size-22"></i></span></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="gdlr-core-pbf-element">
                                                    <div class="gdlr-core-image-item gdlr-core-item-pdlr gdlr-core-item-pdb  gdlr-core-center-align">
                                                        <div class="gdlr-core-image-item-wrap gdlr-core-media-image  gdlr-core-image-item-style-rectangle" style="border-width: 0px;">
                                                            <a class="gdlr-core-ilightbox gdlr-core-js " href="[base_url]theme/theme/upload/shutterstock_130314980.jpg"><img src="[base_url]theme/theme/upload/shutterstock_130314980-920x415.jpg" alt="" width="920" height="415"><span class="gdlr-core-image-overlay "><i class="gdlr-core-image-overlay-icon fa fa-search gdlr-core-size-22"></i></span></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="gdlr-core-pbf-element">
                                                    <div class="gdlr-core-image-item gdlr-core-item-pdlr gdlr-core-item-pdb  gdlr-core-center-align">
                                                        <div class="gdlr-core-image-item-wrap gdlr-core-media-image  gdlr-core-image-item-style-rectangle" style="border-width: 0px;">
                                                            <a class="gdlr-core-ilightbox gdlr-core-js " href="[base_url]theme/theme/upload/shutterstock_150120215.jpg"><img src="[base_url]theme/theme/upload/shutterstock_150120215-920x415.jpg" alt="" width="920" height="415"><span class="gdlr-core-image-overlay "><i class="gdlr-core-image-overlay-icon fa fa-search gdlr-core-size-22"></i></span></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="gdlr-core-pbf-column gdlr-core-column-60 gdlr-core-column-first">
                                        <div class="gdlr-core-pbf-column-content-margin gdlr-core-js ">
                                            <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
                                                <div class="gdlr-core-pbf-element">
                                                    <div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" style="padding-bottom: 20px;">
                                                        <div class="gdlr-core-text-box-item-content">
                                                            <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Donec ullamcorper nulla non metus auctor fringilla. Donec ullamcorper nulla non metus auctor fringilla. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Nulla vitae elit libero, a pharetra augue. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Sed posuere consectetur est at lobortis. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="gdlr-core-pbf-element">
                                                    <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr">
                                                        <div class="gdlr-core-title-item-title-wrap ">
                                                            <h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 24px;font-weight: 700;letter-spacing: 0px;text-transform: none;">Sem Aenean Pharetra<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3></div>
                                                    </div>
                                                </div>
                                                <div class="gdlr-core-pbf-element">
                                                    <div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align">
                                                        <div class="gdlr-core-text-box-item-content">
                                                            <p>Sed posuere consectetur est at lobortis. Aenean lacinia bibendum nulla sed consectetur. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Vestibulum id ligula porta felis euismod semper. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Vestibulum id ligula porta felis euismod semper. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Maecenas faucibus mollis interdum. Donec sed odio dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="gdlr-core-pbf-element">
                                                    <div class="gdlr-core-social-share-item gdlr-core-item-pdb  gdlr-core-left-align gdlr-core-social-share-left-text gdlr-core-item-pdlr">
                                                        <span class="gdlr-core-social-share-wrap">
                                                            <a class="gdlr-core-social-share-facebook" href="#" target="_blank" style="font-size: 18px;"><i class="fa fa-facebook"></i></a>
                                                            <a class="gdlr-core-social-share-linkedin" href="#" target="_blank" style="font-size: 18px;"><i class="fa fa-linkedin"></i></a>
                                                            <a class="gdlr-core-social-share-google-plus" href="#" target="_blank" style="font-size: 18px;"><i class="fa fa-google-plus"></i></a>
                                                            <a class="gdlr-core-social-share-pinterest" href="#" target="_blank" style="font-size: 18px;"><i class="fa fa-pinterest-p"></i></a>
                                                            <a class="gdlr-core-social-share-stumbleupon" href="#" target="_blank" style="font-size: 18px;"><i class="fa fa-stumbleupon"></i></a>
                                                            <a class="gdlr-core-social-share-twitter" href="#" target="_blank" style="font-size: 18px;"><i class="fa fa-twitter"></i></a>
                                                            <a class="gdlr-core-social-share-email" href="#" style="font-size: 18px;"><i class="fa fa-envelope"></i></a>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="gdlr-core-pbf-sidebar-left gdlr-core-column-extend-left  realfactory-sidebar-area gdlr-core-column-15 gdlr-core-pbf-sidebar-padding  gdlr-core-line-height" style="padding: 60px 30px 30px 0px;">
                                <div class="gdlr-core-pbf-background-wrap" style="background-color: #f9f9f9 ;"></div>
                                <div class="gdlr-core-sidebar-item">
                                    <div id="nav_menu-2" class="widget widget_nav_menu realfactory-widget">
                                        <h3 class="realfactory-widget-title">Market Sectors</h3>
                                        <div class="menu-market-sectors-container">
                                            <ul id="menu-market-sectors" class="menu">
                                                <li class="menu-item"><a href="index.htm">Automotive Parts &#038; System</a></li>
                                                <li class="menu-item"><a href="construction-engineering.html">Construction &#038; Engineering</a></li>
                                                <li class="menu-item"><a href="power-energy.html">Power &#038; Energy</a></li>
                                                <li class="menu-item"><a href="aero-space.html">Aero Space</a></li>
                                                <li class="menu-item"><a href="ship-building-insudtry.html">Ship Building Industry</a></li>
                                                <li class="menu-item"><a href="railway.html">Railway</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div id="nav_menu-3" class="widget widget_nav_menu realfactory-widget">
                                        <h3 class="realfactory-widget-title">Useful Links</h3>
                                        <div class="menu-useful-links-container">
                                            <ul id="menu-useful-links" class="menu">
                                                <li class="menu-item"><a href="about-us.html">About Us</a></li>
                                                <li class="menu-item"><a href="blog-3-columns-with-frame.html">Recent News</a></li>
                                                <li class="menu-item"><a href="portfolio-3-columns.html">Our Works</a></li>
                                                <li class="menu-item"><a href="contact.html">Contact</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div id="text-6" class="widget widget_text realfactory-widget">
                                        <div class="textwidget"><a class="gdlr-core-button  gdlr-core-button-solid gdlr-core-button-no-border" href="contact.html" target="_parent" style="margin-right: 20px;border-radius: 3px;-moz-border-radius: 3px;-webkit-border-radius: 3px;background: #222 ;"><span class="gdlr-core-content">Get A Quote</span></a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		
		[footer]
	</div>
</div>