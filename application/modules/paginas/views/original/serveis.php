[menu]
<div class="realfactory-page-wrapper" id="realfactory-page-wrapper">
	<div class="gdlr-core-page-builder-body">

		
		<div class="realfactory-page-title-wrap  realfactory-style-medium realfactory-center-align">
                <div class="realfactory-header-transparent-substitute"></div>
                <div class="realfactory-page-title-overlay"></div>
                <div class="realfactory-page-title-container realfactory-container">
                    <div class="realfactory-page-title-content realfactory-item-pdlr">
                        <h1 class="realfactory-page-title">Portfolio Modern 4 Columns No Space</h1>
                        <div class="realfactory-page-caption">No Excerpt, No Space</div>
                    </div>
                </div>
            </div>
            
            <div class="realfactory-page-wrapper" id="realfactory-page-wrapper">
                <div class="gdlr-core-page-builder-body">
                    <div class="gdlr-core-pbf-wrapper " style="padding: 0px 0px 0px 0px;">
                        <div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
                            <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-pbf-wrapper-full-no-space">
                                <div class="gdlr-core-pbf-element">
                                    <div class="gdlr-core-portfolio-item gdlr-core-item-pdb clearfix  gdlr-core-portfolio-item-style-modern" style="padding-bottom: 0px;">
                                        <div class="gdlr-core-portfolio-item-holder gdlr-core-js-2 clearfix" data-layout="fitrows">
                                            <div class="gdlr-core-item-list  gdlr-core-item-pdlr gdlr-core-item-mgb gdlr-core-column-15 gdlr-core-column-first">
                                                <div class="gdlr-core-portfolio-modern">
                                                    <div class="gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-icon-title-tag">
                                                        <div class="gdlr-core-portfolio-thumbnail-image-wrap  gdlr-core-zoom-on-hover"><img src="[base_url]theme/theme/upload/pexels-photo-109915-800x570.jpeg" alt="" width="800" height="570"><span class="gdlr-core-image-overlay  gdlr-core-portfolio-overlay gdlr-core-image-overlay-center-icon gdlr-core-js"><span class="gdlr-core-image-overlay-content"><span class="gdlr-core-portfolio-icon-wrap"><a class="gdlr-core-ilightbox gdlr-core-js " href="[base_url]theme/theme/upload/pexels-photo-109915.jpeg" data-ilightbox-group="gdlr-core-img-group-1"><i class="gdlr-core-portfolio-icon icon_zoom-in_alt"></i></a></span><span class="gdlr-core-portfolio-title gdlr-core-title-font"><a href="[base_url]serveis-detalle.html">Muchen Railway Station</a></span><span class="gdlr-core-portfolio-info gdlr-core-portfolio-info-tag gdlr-core-info-font"><a href="[base_url]serveis-detalle.html" rel="tag">Muchen</a> <span class="gdlr-core-sep">/</span> <a href="[base_url]serveis-detalle.html" rel="tag">Railway</a></span>
                                                            </span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-item-list  gdlr-core-item-pdlr gdlr-core-item-mgb gdlr-core-column-15">
                                                <div class="gdlr-core-portfolio-modern">
                                                    <div class="gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-icon-title-tag">
                                                        <div class="gdlr-core-portfolio-thumbnail-image-wrap  gdlr-core-zoom-on-hover"><img src="[base_url]theme/theme/upload/shutterstock_57862405-800x570.jpg" alt="" width="800" height="570"><span class="gdlr-core-image-overlay  gdlr-core-portfolio-overlay gdlr-core-image-overlay-center-icon gdlr-core-js"><span class="gdlr-core-image-overlay-content"><span class="gdlr-core-portfolio-icon-wrap"><a class="gdlr-core-ilightbox gdlr-core-js " href="[base_url]theme/theme/upload/shutterstock_57862405.jpg" data-ilightbox-group="gdlr-core-img-group-1"><i class="gdlr-core-portfolio-icon icon_zoom-in_alt"></i></a></span><span class="gdlr-core-portfolio-title gdlr-core-title-font"><a href="[base_url]serveis-detalle.html">USA Bank Building</a></span><span class="gdlr-core-portfolio-info gdlr-core-portfolio-info-tag gdlr-core-info-font"><a href="[base_url]serveis-detalle.html" rel="tag">Bank</a> <span class="gdlr-core-sep">/</span> <a href="[base_url]serveis-detalle.html" rel="tag">System</a></span>
                                                            </span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-item-list  gdlr-core-item-pdlr gdlr-core-item-mgb gdlr-core-column-15">
                                                <div class="gdlr-core-portfolio-modern">
                                                    <div class="gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-icon-title-tag">
                                                        <div class="gdlr-core-portfolio-thumbnail-image-wrap  gdlr-core-zoom-on-hover"><img src="[base_url]theme/theme/upload/shutterstock_130285502-800x570.jpg" alt="" width="800" height="570"><span class="gdlr-core-image-overlay  gdlr-core-portfolio-overlay gdlr-core-image-overlay-center-icon gdlr-core-js"><span class="gdlr-core-image-overlay-content"><span class="gdlr-core-portfolio-icon-wrap"><a class="gdlr-core-ilightbox gdlr-core-js " href="[base_url]theme/theme/upload/shutterstock_130285502.jpg" data-ilightbox-group="gdlr-core-img-group-1"><i class="gdlr-core-portfolio-icon icon_zoom-in_alt"></i></a></span><span class="gdlr-core-portfolio-title gdlr-core-title-font"><a href="[base_url]serveis-detalle.html">Mining Plant Set Up</a></span><span class="gdlr-core-portfolio-info gdlr-core-portfolio-info-tag gdlr-core-info-font"><a href="[base_url]serveis-detalle.html" rel="tag">Mining</a> <span class="gdlr-core-sep">/</span> <a href="[base_url]serveis-detalle.html" rel="tag">Plants</a></span>
                                                            </span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-item-list  gdlr-core-item-pdlr gdlr-core-item-mgb gdlr-core-column-15">
                                                <div class="gdlr-core-portfolio-modern">
                                                    <div class="gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-icon-title-tag">
                                                        <div class="gdlr-core-portfolio-thumbnail-image-wrap  gdlr-core-zoom-on-hover"><img src="[base_url]theme/theme/upload/shutterstock_95662675-800x570.jpg" alt="" width="800" height="570"><span class="gdlr-core-image-overlay  gdlr-core-portfolio-overlay gdlr-core-image-overlay-center-icon gdlr-core-js"><span class="gdlr-core-image-overlay-content"><span class="gdlr-core-portfolio-icon-wrap"><a class="gdlr-core-ilightbox gdlr-core-js " href="[base_url]theme/theme/upload/shutterstock_95662675.jpg" data-ilightbox-group="gdlr-core-img-group-1"><i class="gdlr-core-portfolio-icon icon_zoom-in_alt"></i></a></span><span class="gdlr-core-portfolio-title gdlr-core-title-font"><a href="[base_url]serveis-detalle.html">Apple&#8217;s Server Room</a></span><span class="gdlr-core-portfolio-info gdlr-core-portfolio-info-tag gdlr-core-info-font"><a href="[base_url]serveis-detalle.html" rel="tag">Data</a> <span class="gdlr-core-sep">/</span> <a href="[base_url]serveis-detalle.html" rel="tag">Server</a></span>
                                                            </span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-item-list  gdlr-core-item-pdlr gdlr-core-item-mgb gdlr-core-column-15 gdlr-core-column-first">
                                                <div class="gdlr-core-portfolio-modern">
                                                    <div class="gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-icon-title-tag">
                                                        <div class="gdlr-core-portfolio-thumbnail-image-wrap  gdlr-core-zoom-on-hover"><img src="[base_url]theme/theme/upload/landmark-bridge-metal-architecture-800x570.jpg" alt="" width="800" height="570"><span class="gdlr-core-image-overlay  gdlr-core-portfolio-overlay gdlr-core-image-overlay-center-icon gdlr-core-js"><span class="gdlr-core-image-overlay-content"><span class="gdlr-core-portfolio-icon-wrap"><a class="gdlr-core-ilightbox gdlr-core-js " href="[base_url]theme/theme/upload/landmark-bridge-metal-architecture.jpg" data-ilightbox-group="gdlr-core-img-group-1"><i class="gdlr-core-portfolio-icon icon_zoom-in_alt"></i></a></span><span class="gdlr-core-portfolio-title gdlr-core-title-font"><a href="[base_url]serveis-detalle.html">Sanfran Cisco Bridge</a></span><span class="gdlr-core-portfolio-info gdlr-core-portfolio-info-tag gdlr-core-info-font"><a href="[base_url]serveis-detalle.html" rel="tag">Bridge</a> <span class="gdlr-core-sep">/</span> <a href="[base_url]serveis-detalle.html" rel="tag">Engineering</a></span>
                                                            </span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-item-list  gdlr-core-item-pdlr gdlr-core-item-mgb gdlr-core-column-15">
                                                <div class="gdlr-core-portfolio-modern">
                                                    <div class="gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-icon-title-tag">
                                                        <div class="gdlr-core-portfolio-thumbnail-image-wrap  gdlr-core-zoom-on-hover"><img src="[base_url]theme/theme/upload/shutterstock_161515241-800x570.jpg" alt="" width="800" height="570"><span class="gdlr-core-image-overlay  gdlr-core-portfolio-overlay gdlr-core-image-overlay-center-icon gdlr-core-js"><span class="gdlr-core-image-overlay-content"><span class="gdlr-core-portfolio-icon-wrap"><a class="gdlr-core-ilightbox gdlr-core-js " href="[base_url]theme/theme/upload/shutterstock_161515241.jpg" data-ilightbox-group="gdlr-core-img-group-1"><i class="gdlr-core-portfolio-icon icon_zoom-in_alt"></i></a></span><span class="gdlr-core-portfolio-title gdlr-core-title-font"><a href="[base_url]serveis-detalle.html">Hamburg Wind Energy Plant</a></span><span class="gdlr-core-portfolio-info gdlr-core-portfolio-info-tag gdlr-core-info-font"><a href="[base_url]serveis-detalle.html" rel="tag">Energy</a> <span class="gdlr-core-sep">/</span> <a href="[base_url]serveis-detalle.html" rel="tag">System</a></span>
                                                            </span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-item-list  gdlr-core-item-pdlr gdlr-core-item-mgb gdlr-core-column-15">
                                                <div class="gdlr-core-portfolio-modern">
                                                    <div class="gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-icon-title-tag">
                                                        <div class="gdlr-core-portfolio-thumbnail-image-wrap  gdlr-core-zoom-on-hover"><img src="[base_url]theme/theme/upload/pexels-photo-24276-800x570.jpg" alt="" width="800" height="570"><span class="gdlr-core-image-overlay  gdlr-core-portfolio-overlay gdlr-core-image-overlay-center-icon gdlr-core-js"><span class="gdlr-core-image-overlay-content"><span class="gdlr-core-portfolio-icon-wrap"><a class="gdlr-core-ilightbox gdlr-core-js " href="[base_url]theme/theme/upload/pexels-photo-24276.jpg" data-ilightbox-group="gdlr-core-img-group-1"><i class="gdlr-core-portfolio-icon icon_zoom-in_alt"></i></a></span><span class="gdlr-core-portfolio-title gdlr-core-title-font"><a href="[base_url]serveis-detalle.html">Singapore Logistic Port</a></span><span class="gdlr-core-portfolio-info gdlr-core-portfolio-info-tag gdlr-core-info-font"><a href="[base_url]serveis-detalle.html" rel="tag">Logistic</a> <span class="gdlr-core-sep">/</span> <a href="[base_url]serveis-detalle.html" rel="tag">Port</a></span>
                                                            </span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-item-list  gdlr-core-item-pdlr gdlr-core-item-mgb gdlr-core-column-15">
                                                <div class="gdlr-core-portfolio-modern">
                                                    <div class="gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-icon-title-tag">
                                                        <div class="gdlr-core-portfolio-thumbnail-image-wrap  gdlr-core-zoom-on-hover"><img src="[base_url]theme/theme/upload/pexels-photo-3-800x570.jpg" alt="" width="800" height="570"><span class="gdlr-core-image-overlay  gdlr-core-portfolio-overlay gdlr-core-image-overlay-center-icon gdlr-core-js"><span class="gdlr-core-image-overlay-content"><span class="gdlr-core-portfolio-icon-wrap"><a class="gdlr-core-ilightbox gdlr-core-js " href="[base_url]theme/theme/upload/pexels-photo-3.jpg" data-ilightbox-group="gdlr-core-img-group-1"><i class="gdlr-core-portfolio-icon icon_zoom-in_alt"></i></a></span><span class="gdlr-core-portfolio-title gdlr-core-title-font"><a href="[base_url]serveis-detalle.html">Berlin Central Bank</a></span><span class="gdlr-core-portfolio-info gdlr-core-portfolio-info-tag gdlr-core-info-font"><a href="[base_url]serveis-detalle.html" rel="tag">Bank</a> <span class="gdlr-core-sep">/</span> <a href="[base_url]serveis-detalle.html" rel="tag">Constructions</a></span>
                                                            </span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		[footer]
	</div>
</div>