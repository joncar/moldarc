[menu]
<div class="realfactory-page-wrapper" id="realfactory-page-wrapper">
	<div class="gdlr-core-page-builder-body">

		
		<div class="realfactory-page-title-wrap  realfactory-style-large realfactory-center-align">
                <div class="realfactory-header-transparent-substitute"></div>
                <div class="realfactory-page-title-overlay"></div>
                <div class="realfactory-page-title-container realfactory-container">
                    <div class="realfactory-page-title-content realfactory-item-pdlr">
                        <h1 class="realfactory-page-title">About Us</h1>
                        <div class="realfactory-page-caption">Justo Vulputate Vehicula</div>
                    </div>
                </div>
            </div>
            <div class="realfactory-page-wrapper" id="realfactory-page-wrapper">
                <div class="gdlr-core-page-builder-body">
                    <div class="gdlr-core-pbf-wrapper " style="padding: 0px 0px 0px 0px;">
                        <div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
                            <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
                                <div class="gdlr-core-pbf-column gdlr-core-column-30 gdlr-core-column-first">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js  gdlr-core-column-extend-left" style="padding: 100px 100px 0px 0px;" data-sync-height="height-1">
                                        <div class="gdlr-core-pbf-background-wrap" style="background-color: #f1f0f0 ;">
                                            <div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/about-bg-1.jpg);background-size: cover;background-position: center;" data-parallax-speed="0.2"></div>
                                        </div>
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content" data-gdlr-animation="fadeInLeft" data-gdlr-animation-duration="600ms" data-gdlr-animation-offset="0.8"></div>
                                    </div>
                                </div>
                                <div class="gdlr-core-pbf-column gdlr-core-column-30">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 130px 0px 110px 70px;" data-sync-height="height-1" data-sync-height-center="">
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content" data-gdlr-animation="fadeInRight" data-gdlr-animation-duration="600ms" data-gdlr-animation-offset="0.8">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-bottom: 25px;">
                                                    <div class="gdlr-core-title-item-title-wrap ">
                                                        <h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 33px;letter-spacing: 0px;text-transform: none;">Pharetra Etiam Inceptos<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3></div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" style="padding-bottom: 15px;">
                                                    <div class="gdlr-core-text-box-item-content" style="font-size: 17px;">
                                                        <p>Etiam porta sem malesuada magna mollis euismod. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Maecenas faucibus mollis interdum. Donec id elit non mi porta gravida at eget metus. Aenean lacinia.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-icon-list-item gdlr-core-item-pdlr gdlr-core-item-pdb ">
                                                    <ul>
                                                        <li class=" gdlr-core-skin-divider gdlr-core-with-hover gdlr-core-column-30 gdlr-core-column-first"><span class="gdlr-core-icon-list-icon-wrap"><i class="gdlr-core-icon-list-icon-hover fa fa-check-circle" style="font-size: 16px;width: 16px;"></i><i class="gdlr-core-icon-list-icon fa fa-check-circle" style="font-size: 16px;width: 16px;"></i></span><span class="gdlr-core-icon-list-content" style="font-size: 16px;">Vestibulum id ligula</span></li>
                                                        <li class=" gdlr-core-skin-divider gdlr-core-with-hover gdlr-core-column-30"><span class="gdlr-core-icon-list-icon-wrap"><i class="gdlr-core-icon-list-icon-hover fa fa-check-circle" style="font-size: 16px;width: 16px;"></i><i class="gdlr-core-icon-list-icon fa fa-check-circle" style="font-size: 16px;width: 16px;"></i></span><span class="gdlr-core-icon-list-content" style="font-size: 16px;">Mattis Fringilla Ultricies</span></li>
                                                        <li class=" gdlr-core-skin-divider gdlr-core-with-hover gdlr-core-column-30 gdlr-core-column-first"><span class="gdlr-core-icon-list-icon-wrap"><i class="gdlr-core-icon-list-icon-hover fa fa-check-circle" style="font-size: 16px;width: 16px;"></i><i class="gdlr-core-icon-list-icon fa fa-check-circle" style="font-size: 16px;width: 16px;"></i></span><span class="gdlr-core-icon-list-content" style="font-size: 16px;">Egestas Fermentum</span></li>
                                                        <li class=" gdlr-core-skin-divider gdlr-core-with-hover gdlr-core-column-30"><span class="gdlr-core-icon-list-icon-wrap"><i class="gdlr-core-icon-list-icon-hover fa fa-check-circle" style="font-size: 16px;width: 16px;"></i><i class="gdlr-core-icon-list-icon fa fa-check-circle" style="font-size: 16px;width: 16px;"></i></span><span class="gdlr-core-icon-list-content" style="font-size: 16px;">Adipiscing Vulputate</span></li>
                                                        <li class=" gdlr-core-skin-divider gdlr-core-with-hover gdlr-core-column-30 gdlr-core-column-first"><span class="gdlr-core-icon-list-icon-wrap"><i class="gdlr-core-icon-list-icon-hover fa fa-check-circle" style="font-size: 16px;width: 16px;"></i><i class="gdlr-core-icon-list-icon fa fa-check-circle" style="font-size: 16px;width: 16px;"></i></span><span class="gdlr-core-icon-list-content" style="font-size: 16px;">Ultricies Bibendum</span></li>
                                                        <li class=" gdlr-core-skin-divider gdlr-core-with-hover gdlr-core-column-30"><span class="gdlr-core-icon-list-icon-wrap"><i class="gdlr-core-icon-list-icon-hover fa fa-check-circle" style="font-size: 16px;width: 16px;"></i><i class="gdlr-core-icon-list-icon fa fa-check-circle" style="font-size: 16px;width: 16px;"></i></span><span class="gdlr-core-icon-list-content" style="font-size: 16px;">Fringilla Quam Elit</span></li>
                                                        <li class=" gdlr-core-skin-divider gdlr-core-with-hover gdlr-core-column-30 gdlr-core-column-first"><span class="gdlr-core-icon-list-icon-wrap"><i class="gdlr-core-icon-list-icon-hover fa fa-check-circle" style="font-size: 16px;width: 16px;"></i><i class="gdlr-core-icon-list-icon fa fa-check-circle" style="font-size: 16px;width: 16px;"></i></span><span class="gdlr-core-icon-list-content" style="font-size: 16px;">Fermentum Tellus</span></li>
                                                        <li class=" gdlr-core-skin-divider gdlr-core-with-hover gdlr-core-column-30"><span class="gdlr-core-icon-list-icon-wrap"><i class="gdlr-core-icon-list-icon-hover fa fa-check-circle" style="font-size: 16px;width: 16px;"></i><i class="gdlr-core-icon-list-icon fa fa-check-circle" style="font-size: 16px;width: 16px;"></i></span><span class="gdlr-core-icon-list-content" style="font-size: 16px;">Commodo Vestibulum</span></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                


						<div class="gdlr-core-pbf-column gdlr-core-column-30 gdlr-core-column-first">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 109px 80px 80px 0px;" data-sync-height="height-3">
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-bottom: 35px;">
                                                    <div class="gdlr-core-title-item-title-wrap ">
                                                        <h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 36px;letter-spacing: 0px;text-transform: none;">Amet Etiam Quam<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3></div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" style="padding-bottom: 0px;">
                                                    <div class="gdlr-core-text-box-item-content" style="font-size: 15px;">
                                                        <p>Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas sed diam eget risus varius blandit sit amet non magna. Maecenas sed diam eget risus varius blandit sit amet non magna. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec id elit non mi porta gravida.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="gdlr-core-pbf-column gdlr-core-column-30">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 105px 0px 30px 0px;" data-sync-height="height-3">
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content" data-gdlr-animation="fadeInRight" data-gdlr-animation-duration="600ms" data-gdlr-animation-offset="0.8">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-skill-bar-item gdlr-core-item-pdlr gdlr-core-item-pdb  gdlr-core-size-small gdlr-core-type-round">
                                                    <div class="gdlr-core-skill-bar">
                                                        <div class="gdlr-core-skill-bar-head gdlr-core-title-font"><span class="gdlr-core-skill-bar-title">Material Productions</span><span class="gdlr-core-skill-bar-right">90%</span></div>
                                                        <div class="gdlr-core-skill-bar-progress">
                                                            <div class="gdlr-core-skill-bar-filled gdlr-core-js" data-width="90"></div>
                                                        </div>
                                                    </div>
                                                    <div class="gdlr-core-skill-bar">
                                                        <div class="gdlr-core-skill-bar-head gdlr-core-title-font"><span class="gdlr-core-skill-bar-title">EPC Works</span><span class="gdlr-core-skill-bar-right">100%</span></div>
                                                        <div class="gdlr-core-skill-bar-progress">
                                                            <div class="gdlr-core-skill-bar-filled gdlr-core-js" data-width="100"></div>
                                                        </div>
                                                    </div>
                                                    <div class="gdlr-core-skill-bar">
                                                        <div class="gdlr-core-skill-bar-head gdlr-core-title-font"><span class="gdlr-core-skill-bar-title">Customer Satisfaction</span><span class="gdlr-core-skill-bar-right">85%</span></div>
                                                        <div class="gdlr-core-skill-bar-progress">
                                                            <div class="gdlr-core-skill-bar-filled gdlr-core-js" data-width="85"></div>
                                                        </div>
                                                    </div>
                                                    <div class="gdlr-core-skill-bar">
                                                        <div class="gdlr-core-skill-bar-head gdlr-core-title-font"><span class="gdlr-core-skill-bar-title">Utilization Rate</span><span class="gdlr-core-skill-bar-right">90%</span></div>
                                                        <div class="gdlr-core-skill-bar-progress">
                                                            <div class="gdlr-core-skill-bar-filled gdlr-core-js" data-width="90"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>





                    <div class="gdlr-core-pbf-wrapper " style="padding: 75px 0px 15px 0px;">
                        <div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
                            <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
                                <div class="gdlr-core-pbf-element">
                                    <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-bottom: 55px;">
                                        <div class="gdlr-core-title-item-title-wrap  gdlr-core-js-2 gdlr-core-with-divider">
                                            <h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 36px;font-weight: 300;letter-spacing: 0px;text-transform: none;margin-right: 30px;">Market Sectors<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3>
                                            <div class="gdlr-core-title-item-divider gdlr-core-right gdlr-core-skin-divider"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="gdlr-core-pbf-wrapper " style="padding: 0px 0px 0px 0px;">
                        <div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
                            <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
                                <div class="gdlr-core-pbf-column gdlr-core-column-30 gdlr-core-column-first">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js  gdlr-core-column-extend-left" data-sync-height="height-service">
                                        <div class="gdlr-core-pbf-background-wrap">
                                            <div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/shutterstock_134132600.jpg);background-size: cover;background-position: center;" data-parallax-speed="0.4"></div>
                                        </div>
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content"></div>
                                    </div>
                                </div>
                                <div class="gdlr-core-pbf-column gdlr-core-column-30" data-skin="Call to action">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js  gdlr-core-column-extend-right" style="padding: 157px 20px 60px 73px;" data-sync-height="height-service">
                                        <div class="gdlr-core-pbf-background-wrap">
                                            <div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/service-bg-1.jpg);background-size: cover;background-position: center;" data-parallax-speed="0.2"></div>
                                        </div>
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr">
                                                    <div class="gdlr-core-title-item-title-wrap ">
                                                        <h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 26px;font-weight: 300;letter-spacing: 0px;text-transform: none;">Construction & Engineering<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3></div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" style="padding-bottom: 0px;">
                                                    <div class="gdlr-core-text-box-item-content" style="font-size: 17px;">
                                                        <p>We provides a comprehensive range of products and techniques to create value for the construction industry and support sustainable development.</p>
                                                        <p><a href="#"><strong>Learn More</strong></a></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="gdlr-core-pbf-column gdlr-core-column-30 gdlr-core-column-first" data-skin="Yellow Service">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js  gdlr-core-column-extend-left" style="padding: 157px 40px 60px 0px;" data-sync-height="height-service">
                                        <div class="gdlr-core-pbf-background-wrap">
                                            <div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/service-4-1.jpg);background-size: cover;background-position: center;" data-parallax-speed="0.2"></div>
                                        </div>
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr">
                                                    <div class="gdlr-core-title-item-title-wrap ">
                                                        <h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 26px;font-weight: 300;letter-spacing: 0px;text-transform: none;">Railway<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3></div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" style="padding-bottom: 0px;">
                                                    <div class="gdlr-core-text-box-item-content" style="font-size: 17px;">
                                                        <p>We provides a wide range of high performance rail products and railway infrastructure services to the international rail industry.</p>
                                                        <p><a href="#"><strong>Learn More</strong></a></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="gdlr-core-pbf-column gdlr-core-column-30">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js  gdlr-core-column-extend-right" data-sync-height="height-service">
                                        <div class="gdlr-core-pbf-background-wrap">
                                            <div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/train-railway-s-bahn-transport.jpg);background-size: cover;background-position: center;" data-parallax-speed="0.4"></div>
                                        </div>
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content"></div>
                                    </div>
                                </div>
                                <div class="gdlr-core-pbf-column gdlr-core-column-30 gdlr-core-column-first">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js  gdlr-core-column-extend-left" data-sync-height="height-service">
                                        <div class="gdlr-core-pbf-background-wrap">
                                            <div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/shutterstock_161515241.jpg);background-size: cover;background-position: center;" data-parallax-speed="0.4"></div>
                                        </div>
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content"></div>
                                    </div>
                                </div>
                                <div class="gdlr-core-pbf-column gdlr-core-column-30" data-skin="Call to action">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js  gdlr-core-column-extend-right" style="padding: 157px 20px 60px 73px;" data-sync-height="height-service">
                                        <div class="gdlr-core-pbf-background-wrap">
                                            <div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/service-5-1.jpg);background-size: cover;background-position: center;" data-parallax-speed="0.2"></div>
                                        </div>
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr">
                                                    <div class="gdlr-core-title-item-title-wrap ">
                                                        <h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 26px;font-weight: 300;letter-spacing: 0px;text-transform: none;">Power & Energy<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3></div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" style="padding-bottom: 0px;">
                                                    <div class="gdlr-core-text-box-item-content" style="font-size: 17px;">
                                                        <p>High quality material produced by Real Factory backed by support of our expert consultants to fulfill a whole range of applications in the enegy market.</p>
                                                        <p><a href="#"><strong>Learn More</strong></a></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="gdlr-core-pbf-column gdlr-core-column-30 gdlr-core-column-first" data-skin="Yellow Service">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js  gdlr-core-column-extend-left" style="padding: 157px 40px 60px 0px;" data-sync-height="height-service">
                                        <div class="gdlr-core-pbf-background-wrap">
                                            <div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/service-6-1.jpg);background-size: cover;background-position: center;" data-parallax-speed="0.2"></div>
                                        </div>
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr">
                                                    <div class="gdlr-core-title-item-title-wrap ">
                                                        <h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 26px;font-weight: 300;letter-spacing: 0px;text-transform: none;">Aero Space<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3></div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" style="padding-bottom: 0px;">
                                                    <div class="gdlr-core-text-box-item-content" style="font-size: 17px;">
                                                        <p>We are a leading Aerospace steels manufacturer. Our special grade steels are being used in commercial and military aerospace projects around the world.</p>
                                                        <p><a href="#"><strong>Learn More</strong></a></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="gdlr-core-pbf-column gdlr-core-column-30">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js  gdlr-core-column-extend-right" data-sync-height="height-service">
                                        <div class="gdlr-core-pbf-background-wrap">
                                            <div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/shutterstock_178012379.jpg);background-size: cover;background-position: center;" data-parallax-speed="0.4"></div>
                                        </div>
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                                
                            </div>
                        </div>
                    </div>
                    <div class="gdlr-core-pbf-wrapper " style="padding: 130px 0px 100px 0px;" data-skin="Dark">
                        <div class="gdlr-core-pbf-background-wrap" style="background-color: #232323 ;">
                            <div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/about-bg.jpg);background-size: cover;background-position: center;" data-parallax-speed="0.2"></div>
                        </div>
                        <div class="gdlr-core-pbf-wrapper-content gdlr-core-js " data-gdlr-animation="fadeInUp" data-gdlr-animation-duration="600ms" data-gdlr-animation-offset="0.8">
                            <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
                                <div class="gdlr-core-pbf-column gdlr-core-column-60 gdlr-core-column-first">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 0px 0px 30px 0px;">
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-center-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-bottom: 25px;">
                                                    <div class="gdlr-core-title-item-title-wrap ">
                                                        <h4 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 54px;letter-spacing: 0px;text-transform: none;">Important <span style="color: #f8c12c;">Static</span><span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h4></div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-center-align">
                                                    <div class="gdlr-core-text-box-item-content" style="font-size: 16px;">
                                                        <p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings
                                                            <br> of spring which I enjoy with my whole heart. I am alone</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="gdlr-core-pbf-column gdlr-core-column-15 gdlr-core-column-first" data-skin="About Counter">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 10px 0px 0px 0px;">
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-counter-item gdlr-core-item-pdlr gdlr-core-item-pdb " style="padding-bottom: 10px;">
                                                    <div class="gdlr-core-counter-item-number gdlr-core-skin-title" style="font-size: 49px;"><span class="gdlr-core-counter-item-count gdlr-core-js" data-duration="4000" data-counter-start="0" data-counter-end="20000">0</span><span class="gdlr-core-counter-item-suffix">+</span></div>
                                                    <div class="gdlr-core-counter-item-bottom-text gdlr-core-skin-content" style="font-size: 18px;text-transform: none;">Customers</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="gdlr-core-pbf-column gdlr-core-column-15" data-skin="About Counter">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 10px 0px 0px 0px;">
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-counter-item gdlr-core-item-pdlr gdlr-core-item-pdb " style="padding-bottom: 10px;">
                                                    <div class="gdlr-core-counter-item-number gdlr-core-skin-title" style="font-size: 49px;"><span class="gdlr-core-counter-item-count gdlr-core-js" data-duration="4000" data-counter-start="0" data-counter-end="1400">0</span></div>
                                                    <div class="gdlr-core-counter-item-bottom-text gdlr-core-skin-content" style="font-size: 18px;text-transform: none;">Projects</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="gdlr-core-pbf-column gdlr-core-column-15" data-skin="About Counter">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 10px 0px 0px 0px;">
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-counter-item gdlr-core-item-pdlr gdlr-core-item-pdb " style="padding-bottom: 10px;">
                                                    <div class="gdlr-core-counter-item-number gdlr-core-skin-title" style="font-size: 49px;"><span class="gdlr-core-counter-item-count gdlr-core-js" data-duration="4000" data-counter-start="0" data-counter-end="16800">0</span></div>
                                                    <div class="gdlr-core-counter-item-bottom-text gdlr-core-skin-content" style="font-size: 18px;text-transform: none;">Working Hours</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="gdlr-core-pbf-column gdlr-core-column-15" data-skin="About Counter">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 10px 0px 0px 0px;">
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-counter-item gdlr-core-item-pdlr gdlr-core-item-pdb " style="padding-bottom: 10px;">
                                                    <div class="gdlr-core-counter-item-number gdlr-core-skin-title" style="font-size: 49px;"><span class="gdlr-core-counter-item-count gdlr-core-js" data-duration="4000" data-counter-start="0" data-counter-end="17000">0</span></div>
                                                    <div class="gdlr-core-counter-item-bottom-text gdlr-core-skin-content" style="font-size: 18px;text-transform: none;">SKUs</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="gdlr-core-pbf-wrapper " style="padding: 110px 0px 0px 0px;">
                        <div class="gdlr-core-pbf-background-wrap" style="background-color: #f0f0f0 ;"></div>
                        <div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
                            <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
                                <div class="gdlr-core-pbf-element">
                                    <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-center-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-bottom: 60px;">
                                        <div class="gdlr-core-title-item-title-wrap ">
                                            <h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 45px;font-weight: 700;letter-spacing: 0px;text-transform: none;">Meet The Team<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3></div>
                                    </div>
                                </div>
                                <div class="gdlr-core-pbf-element">
                                    <div class="gdlr-core-personnel-item gdlr-core-item-pdb clearfix  gdlr-core-center-align gdlr-core-personnel-item-style-grid-with-background gdlr-core-personnel-style-grid gdlr-core-with-background">
                                        <div class="gdlr-core-personnel-list-column  gdlr-core-column-20 gdlr-core-column-first gdlr-core-item-pdlr">
                                            <div class="gdlr-core-personnel-list clearfix">
                                                <div class="gdlr-core-personnel-list-image gdlr-core-media-image  gdlr-core-opacity-on-hover gdlr-core-zoom-on-hover">
                                                    <a href="../personnel/david-parker/index.htm"><img src="[base_url]theme/theme/upload/personnel-2.jpg" alt="" width="600" height="400"></a>
                                                </div>
                                                <div class="gdlr-core-personnel-list-content-wrap">
                                                    <h3 class="gdlr-core-personnel-list-title"><a href="../personnel/david-parker/index.htm">David Parker</a></h3>
                                                    <div class="gdlr-core-personnel-list-position gdlr-core-info-font gdlr-core-skin-caption">Chief Executive Officer</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="gdlr-core-personnel-list-column  gdlr-core-column-20 gdlr-core-item-pdlr">
                                            <div class="gdlr-core-personnel-list clearfix">
                                                <div class="gdlr-core-personnel-list-image gdlr-core-media-image  gdlr-core-opacity-on-hover gdlr-core-zoom-on-hover">
                                                    <a href="../personnel/alan-cooper/index.htm"><img src="[base_url]theme/theme/upload/personnel-4.jpg" alt="" width="600" height="400"></a>
                                                </div>
                                                <div class="gdlr-core-personnel-list-content-wrap">
                                                    <h3 class="gdlr-core-personnel-list-title"><a href="../personnel/alan-cooper/index.htm">Alan Cooper</a></h3>
                                                    <div class="gdlr-core-personnel-list-position gdlr-core-info-font gdlr-core-skin-caption">Vice President</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="gdlr-core-personnel-list-column  gdlr-core-column-20 gdlr-core-item-pdlr">
                                            <div class="gdlr-core-personnel-list clearfix">
                                                <div class="gdlr-core-personnel-list-image gdlr-core-media-image  gdlr-core-opacity-on-hover gdlr-core-zoom-on-hover">
                                                    <a href="../personnel/janet-cole/index.htm"><img src="[base_url]theme/theme/upload/personnel-5.jpg" alt="" width="600" height="400"></a>
                                                </div>
                                                <div class="gdlr-core-personnel-list-content-wrap">
                                                    <h3 class="gdlr-core-personnel-list-title"><a href="../personnel/janet-cole/index.htm">Janet Cole</a></h3>
                                                    <div class="gdlr-core-personnel-list-position gdlr-core-info-font gdlr-core-skin-caption">Chief Financial Officer</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="gdlr-core-personnel-list-column  gdlr-core-column-20 gdlr-core-column-first gdlr-core-item-pdlr">
                                            <div class="gdlr-core-personnel-list clearfix">
                                                <div class="gdlr-core-personnel-list-image gdlr-core-media-image  gdlr-core-opacity-on-hover gdlr-core-zoom-on-hover">
                                                    <a href="../personnel/peter-sandler/index.htm"><img src="[base_url]theme/theme/upload/personnel-3.jpg" alt="" width="600" height="400"></a>
                                                </div>
                                                <div class="gdlr-core-personnel-list-content-wrap">
                                                    <h3 class="gdlr-core-personnel-list-title"><a href="../personnel/peter-sandler/index.htm">Peter Sandler</a></h3>
                                                    <div class="gdlr-core-personnel-list-position gdlr-core-info-font gdlr-core-skin-caption">Senior Engineer</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="gdlr-core-personnel-list-column  gdlr-core-column-20 gdlr-core-item-pdlr">
                                            <div class="gdlr-core-personnel-list clearfix">
                                                <div class="gdlr-core-personnel-list-image gdlr-core-media-image  gdlr-core-opacity-on-hover gdlr-core-zoom-on-hover">
                                                    <a href="../personnel/ricardo-gomez/index.htm"><img src="[base_url]theme/theme/upload/personnel-1.jpg" alt="" width="600" height="400"></a>
                                                </div>
                                                <div class="gdlr-core-personnel-list-content-wrap">
                                                    <h3 class="gdlr-core-personnel-list-title"><a href="../personnel/ricardo-gomez/index.htm">Ricardo Gomez</a></h3>
                                                    <div class="gdlr-core-personnel-list-position gdlr-core-info-font gdlr-core-skin-caption">HR Manager</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="gdlr-core-personnel-list-column  gdlr-core-column-20 gdlr-core-item-pdlr">
                                            <div class="gdlr-core-personnel-list clearfix">
                                                <div class="gdlr-core-personnel-list-image gdlr-core-media-image  gdlr-core-opacity-on-hover gdlr-core-zoom-on-hover">
                                                    <a href="../personnel/james-smith/index.htm"><img src="[base_url]theme/theme/upload/Untitled-6.jpg" alt="" width="600" height="400"></a>
                                                </div>
                                                <div class="gdlr-core-personnel-list-content-wrap">
                                                    <h3 class="gdlr-core-personnel-list-title"><a href="../personnel/james-smith/index.htm">James Smith</a></h3>
                                                    <div class="gdlr-core-personnel-list-position gdlr-core-info-font gdlr-core-skin-caption">Chief Technology Officer</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

	[footer]
	</div>
</div>