[menu]
<div class="realfactory-page-wrapper" id="realfactory-page-wrapper">
    <div class="gdlr-core-page-builder-body">

        
        <div class="realfactory-page-title-wrap  realfactory-style-medium realfactory-center-align">
                <div class="realfactory-header-transparent-substitute"></div>
                <div class="realfactory-page-title-overlay"></div>
                <div class="realfactory-page-title-container realfactory-container">
                    <div class="realfactory-page-title-content realfactory-item-pdlr">
                        <h1 class="realfactory-page-title">Latest projects</h1>
                        <div class="realfactory-page-caption">Take a look at what we have done and what we can do</div>
                    </div>
                </div>
            </div>
            
            <div class="realfactory-page-wrapper" id="realfactory-page-wrapper">
                <div class="gdlr-core-page-builder-body">
                    <div class="gdlr-core-pbf-wrapper " style="padding: 0px 0px 0px 0px;">
                        <div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
                            <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-pbf-wrapper-full-no-space">

                                <div class="gdlr-core-pbf-column gdlr-core-column-60 gdlr-core-column-first" data-skin="Portfolio Homepage">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js " data-sync-height="height-port">
                                        <div class="gdlr-core-pbf-background-wrap">
                                            <div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/port-bg.jpg);background-size: cover;background-position: center;" data-parallax-speed="0"></div>
                                        </div>
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content" id="projectes">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-portfolio-item gdlr-core-portfolio-item-style-grid-no-space gdlr-core-item-pdb clearfix  gdlr-core-portfolio-item-style-modern" style="padding-bottom: 0px;">
                                                    <div class="gdlr-core-portfolio-item-holder gdlr-core-js-2 clearfix" data-layout="fitrows">
                                                        
                                                        <?php foreach($this->db->get_where('ultimos_trabajos',array('idioma'=>$_SESSION['lang']))->result() as $n=>$v): ?>
                                                            <div class="gdlr-core-item-list  gdlr-core-item-pdlr gdlr-core-item-mgb gdlr-core-column-15 <?= $n%4==0?'gdlr-core-column-first':'' ?>">
                                                                
                                                                <div class="gdlr-core-portfolio-grid  gdlr-core-left-align gdlr-core-style-normal">
                                                                    <div class="gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-title-icon">
                                                                        <div class="gdlr-core-portfolio-thumbnail-image-wrap  gdlr-core-zoom-on-hover">
                                                                            <img src="<?= base_url('img/servicios/'.$v->miniatura) ?>" alt="" width="500" height="625">
                                                                            <span class="gdlr-core-image-overlay  gdlr-core-portfolio-overlay gdlr-core-image-overlay-center-icon gdlr-core-js">
                                                                                <span class="gdlr-core-image-overlay-content">
                                                                                    <span class="gdlr-core-portfolio-title gdlr-core-title-font">
                                                                                        <a href="#"><?= $v->titulo ?></a>
                                                                                    </span>
                                                                                    <span class="gdlr-core-portfolio-icon-wrap">
                                                                                        <a class="gdlr-core-ilightbox gdlr-core-js " href="<?= base_url('img/servicios/'.$v->foto) ?>" data-ilightbox-group="gdlr-core-img-group-1">
                                                                                            <i class="gdlr-core-portfolio-icon icon_zoom-in_alt"></i>
                                                                                        </a>
                                                                                    </span>
                                                                                </span>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="gdlr-core-portfolio-content-wrap gdlr-core-skin-divider">
                                                                        <h3 class="gdlr-core-portfolio-title gdlr-core-skin-title" style="font-size: 18px;font-weight: 600;letter-spacing: 0px;text-transform: none;">
                                                                            <a href="#"><?= $v->titulo ?></a>
                                                                        </h3>
                                                                        <span class="gdlr-core-portfolio-info gdlr-core-portfolio-info-tag gdlr-core-info-font  gdlr-core-skin-caption">
                                                                            <?php foreach(explode(',',$v->tags) as $t): ?>
                                                                                <a href="#" rel="tag">
                                                                                    <?= $t ?>
                                                                                </a> 
                                                                                <span class="gdlr-core-sep">/</span> 
                                                                            <?php endforeach ?>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php endforeach ?>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        [footer]
    </div>
</div>