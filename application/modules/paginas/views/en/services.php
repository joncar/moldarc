[menu]
<div class="realfactory-page-wrapper" id="realfactory-page-wrapper">
    <div class="gdlr-core-page-builder-body">

        
        <div class="realfactory-page-title-wrap  realfactory-style-medium realfactory-center-align"   style="background-image:url(<?= base_url('theme/theme/images/page-title-background3.jpg') ?>);">
                <div class="realfactory-header-transparent-substitute"></div>
                <div class="realfactory-page-title-overlay"></div>
                <div class="realfactory-page-title-container realfactory-container">
                    <div class="realfactory-page-title-content realfactory-item-pdlr">
                        <h1 class="realfactory-page-title">Our services</h1>
                        <div class="realfactory-page-caption">Find all we can offer you</div>
                    </div>
                </div>
            </div>





            
            




            <div class="gdlr-core-pbf-wrapper" style="margin: 0px 0px 0px 0px;">
                <div class="gdlr-core-pbf-background-wrap">
                    <div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/button-bg.jpg);background-size: cover;background-position: center;" data-parallax-speed="0.8"></div>
                </div>
                <div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
                    <div class="gdlr-core-pbf-wrapper-container clearfix"><!--gdlr-core-container-->
                        <div class="gdlr-core-pbf-element">
                            <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-center-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-bottom: 70px;">
                                <div class="gdlr-core-title-item-title-wrap ">
                                    <h4 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 23px;color: #ffffff;">Welding types and services offered in Moldarc<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h4></div>
                            </div>
                        </div>
                        



                        <?php $this->db->order_by('orden','ASC');  foreach($this->db->get_where('servicios',array('idioma'=>$_SESSION['lang']))->result() as $n=>$v): ?>
                            <div class="gdlr-core-pbf-column gdlr-core-column-15 <?= $n%4==0?'gdlr-core-column-first':'' ?>">
                                <div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 5px 5px 5px 5px;">
                                    <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
                                        <div class="gdlr-core-pbf-element">
                                            <div class="gdlr-core-flipbox-item gdlr-core-item-pdlr gdlr-core-item-pdb ">
                                                <div class="gdlr-core-flipbox gdlr-core-js">
                                                    <div class="gdlr-core-flipbox-front gdlr-core-js  gdlr-core-center-align gdlr-core-flipbox-type-outer" style="background-color: #474747 ;padding: 86px 50px 61px 50px;border-radius: 0px;-moz-border-radius: 0px;-webkit-border-radius: 0px;border-color: #ffffff;" data-sync-height="boxes-5" data-sync-height-center="">
                                                        <div class="gdlr-core-flipbox-background" style="background-image: url(<?= base_url('img/servicios/'.$v->foto) ?>);opacity: 0.62;"></div>
                                                        <div class="gdlr-core-flipbox-content gdlr-core-sync-height-content">
                                                            <?php if(empty($v->icono)): ?>
                                                                <i class="gdlr-core-flipbox-item-icon icon_menu-circle_alt2" style="font-size: 40px;"></i>
                                                            <?php else: ?>
                                                                <img src="<?= base_url('img/servicios/'.$v->icono) ?>" alt="">
                                                            <?php endif ?>
                                                            <h3 class="gdlr-core-flipbox-item-title"><?= $v->titulo ?></h3>
                                                            <div class="gdlr-core-flipbox-item-content" style="font-size: 14px;">
                                                                <p><?= cortar_palabras(strip_tags($v->caracteristicas),15).'...' ?></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="gdlr-core-flipbox-back gdlr-core-js  gdlr-core-center-align gdlr-core-flipbox-type-outer" style="background-color: #4a75ef ;padding: 86px 50px 61px 50px;border-radius: 0px;-moz-border-radius: 0px;-webkit-border-radius: 0px;border-color: #ffffff;" data-sync-height="boxes-5" data-sync-height-center="">
                                                        <div class="gdlr-core-flipbox-background" style="background-image: url(<?= base_url('img/servicios/'.$v->foto_hover) ?>);opacity: 0.62;"></div>
                                                        <div class="gdlr-core-flipbox-content gdlr-core-sync-height-content">
                                                            <?php if(empty($v->icono)): ?>
                                                                <i class="gdlr-core-flipbox-item-icon fa fa-group" style="font-size: 40px;"></i>
                                                            <?php else: ?>
                                                                <img src="<?= base_url('img/servicios/'.$v->icono_hover) ?>" alt="">
                                                            <?php endif ?>
                                                            <h3 class="gdlr-core-flipbox-item-title"><?= $v->subtitulo ?></h3>
                                                            <div class="gdlr-core-flipbox-item-content">
                                                                <p><?= cortar_palabras(strip_tags($v->caracteristicas2),15).'...' ?></p>
                                                            </div>
                                                            <a class="gdlr-core-flipbox-link" href="<?= base_url('servei/'.toUrl($v->id.'-'.$v->titulo)) ?>" target="_self"></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach ?>




                    </div>
                </div>
            </div>






        [footer]
    </div>
</div>