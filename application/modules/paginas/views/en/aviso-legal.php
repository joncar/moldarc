[menu]
<div class="realfactory-page-wrapper" id="realfactory-page-wrapper">
	<div class="gdlr-core-page-builder-body">

		<div class="realfactory-page-wrapper" id="realfactory-page-wrapper">
                <div class="gdlr-core-page-builder-body">
                    <div class="realfactory-page-title-wrap  realfactory-style-medium realfactory-center-align"  style="background-image:url([base_url]theme/theme/upload/bg-featured-image-2.jpg); ?>);">
                <div class="realfactory-header-transparent-substitute"></div>
                <div class="realfactory-page-title-overlay"></div>
                <div class="realfactory-page-title-container realfactory-container">
                    <div class="realfactory-page-title-content realfactory-item-pdlr">
                        <h1 class="realfactory-page-title">Legal Advice</h1>
                        <div class="realfactory-page-caption">&nbsp;</div>
                    </div>
                </div>
            </div>
                    <div class="gdlr-core-pbf-wrapper ">
                        <div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
                            <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
                                
                                <h5 class="mt-5 text-theme-colored">Who is responsible for the treatment of your personal data?</h5>
              <p>MOLDARC, with registered office in Pere IV 29-35 ground floor, with N.I.F. nº B-59612846,  is responsible for the processing of the personal data that you provide us and is responsible for the personal data in accordance with the regulations applicable in the field of data protection.
</h5>
              <h5 class="mt-5 text-theme-colored">Where do we store your data?</h5>
              <p>The data we collect about you is stored within the European Economic Area ("EEA"). Any transfer of your personal data will be made in accordance with the applicable laws.
</h5>
              <h5 class="mt-5 text-theme-colored">To whom do we communicate your data?</h5>
              <p>Your data may be shared by MOLDARC. We never pass, sell or exchange your personal data with third parties. The data sent to third parties will be used solely to offer you our services. We will detail the categories of third parties that access your data for each specific treatment activity.</p>
              <h5 class="mt-5 text-theme-colored">What is the legal basis for the treatment of your personal data?</h5>
              <p>In each specific treatment of personal data collected on you, we will inform you if the communication of personal data is a legal or contractual requirement, or a necessary requirement to subscribe to a contract, and if you are obligated to provide personal information, as well as possible consequences of not providing such data.</p>
              <h5 class="mt-5 text-theme-colored">What are your rights?</h5>
              <p>Right of access: Anyone has the right to obtain confirmation as to whether MOLDARC is treating personal data that concerns them or not. You can contact MOLDARC who will send you the personal data we treat you by email.</p>
<p>Portability Law: As long as MOLDARC processes your personal data through automated means on the basis of your consent or agreement, you have the right to obtain a copy of your data in a structured format, commonly used and read mechanically transferred to your name or to a third party. In it, only the personal information that you have provided us will be included.</p>
<p>Right of rectification: You have the right to request the rectification of your personal data if they are inaccurate, including the right to complete incomplete information.</p>
<p>Right of deletion: You have the right to obtain without undue delay the abolition of any personal data treated by MOLDARC at any time, except in the following situations:

<br>* Has an outstanding debt with MOLDARC, regardless of the method of payment
<br>* It is suspected or confirmed that our services have been incorrectly used in the last four years
<br>* You have hired a service for which we will keep your personal information in relation to the transaction by accounting regulations.
</p>
<p>Opposition law to the processing of data based on legitimate interest: You have the right to oppose the processing of your personal data based on the legitimate interest of MOLDARC. In this case, MOLDARC will not continue to treat personal data unless we can prove legitimate reasons for the treatment that prevails over its interests, rights and freedoms, or for the formulation, exercise or defense of claims.</p>
<p>Opposition law to direct marketing: You have the right to oppose direct marketing, including profiling made for this direct marketing. You can disassociate yourself from direct marketing at any time in the following ways: * following the directions provided in each marketing email</p>
<p>Right to file a complaint with a control authority: If you believe that MOLDARC treats your data in an incorrect manner, you can contact us. You also have the right to file a complaint with the competent data protection authority.</p>
<p>Right to limitation in treatment: You have the right to request that MOLDARC limit the treatment of your personal data in the following circumstances:
<br>* if you oppose the treatment of your data based on the legitimate interest of MOLDARC. In this case MOLDARC will have to limit any treatment of these data pending verification of legitimate interest.
<br>* if you claim that your personal data are incorrect, MOLDARC must limit any treatment of these data until the accuracy of the data is verified.
<br>* if the treatment is illegal, you may oppose the removal of personal data and, instead, request the limitation of its use.
<br>* if MOLDARC no longer needs personal data, but you need them for the formulation, exercise or defense of claims.</p>
<p>Exercise of rights: We take very seriously the protection of data and, therefore, we have dedicated customer service personnel that deals with your requests in relation to the aforementioned rights. You can always get in touch with them at info@moldarc.com 

</p>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
		
		[footer]
	</div>
</div>