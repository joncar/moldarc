	[menu]
	<div class="realfactory-page-wrapper" id="realfactory-page-wrapper" editable="true">
		<div class="gdlr-core-page-builder-body">
			<div class="gdlr-core-pbf-wrapper " style="padding: 0px 0px 0px 0px;">
				<div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
					<div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-pbf-wrapper-full-no-space">
						<div class="gdlr-core-pbf-element">
							<div class="gdlr-core-revolution-slider-item gdlr-core-item-pdlr gdlr-core-item-pdb " style="padding-bottom: 0px;">
								<div id="rev_slider_2_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-source="gallery" style="margin:0px auto;background:#262626;padding:0px;margin-top:0px;margin-bottom:0px;">
									<div id="rev_slider_2_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.5.1">
										<ul>
											<li data-index="rs-4" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="[base_url]theme/theme/upload/slider-1-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description=""> <img src="[base_url]theme/theme/upload/slider-1.jpg" alt="" title="slider-1" width="1800" height="730" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina="">
												<div class="tp-caption   tp-resizeme" id="slide-4-layer-1" data-x="26" data-y="center" data-voffset="-108" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.8;sY:0.8;skX:0;skY:0;opacity:0;","speed":260,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' data-textalign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; white-space: nowrap; font-size: 50px;  font-weight: 400; color: rgba(248,193,44,1);font-family:Hind;">We work with the</div>
												<div class="tp-caption   tp-resizeme" id="slide-4-layer-2" data-x="18" data-y="center" data-voffset="-3" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"from":"x:-50px;opacity:0;","speed":300,"to":"o:1;","delay":830,"ease":"Power2.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' data-textalign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; white-space: nowrap; font-size: 130px;  font-weight: 700; color: rgba(247,247,247,1);font-family:Hind;">highest</div>
												<div class="tp-caption   tp-resizeme" id="slide-4-layer-3" data-x="510" data-y="center" data-voffset="-5" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"from":"x:50px;opacity:0;","speed":300,"to":"o:1;","delay":1150,"ease":"Power2.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' data-textalign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 7; white-space: nowrap; font-size: 130px;  font-weight: 700; color: rgba(248,193,44,1);font-family:Hind;">accuracy</div>
												<div class="tp-caption   tp-resizeme hidden-xs" id="slide-4-layer-4" data-x="27" data-y="center" data-voffset="91" data-width="['666']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":300,"to":"o:1;","delay":1460,"ease":"Power2.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' data-textalign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 8; min-width: 666px; max-width: 666px; white-space: normal; font-size: 20px; line-height: 30px; font-weight: 400; color: rgba(255,255,255,1);font-family:Hind;">Quality and experience at the service of soldering</div>
												<!-- 
<div class="tp-caption rev-btn " id="slide-4-layer-6" data-x="26" data-y="center" data-voffset="177" data-width="['auto']" data-height="['auto']" data-type="button" data-responsive_offset="on" data-responsive="off" data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":300,"to":"o:1;","delay":1770,"ease":"Power2.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0,0,0,1);bg:rgba(247,204,86,1);"}]' data-textalign="['left','left','left','left']" data-paddingtop="[17,17,17,17]" data-paddingright="[34,34,34,34]" data-paddingbottom="[17,17,17,17]" data-paddingleft="[34,34,34,34]" style="z-index: 9; white-space: nowrap; font-size: 14px; line-height: 17px; font-weight: 600; color: rgba(10,10,10,1);font-family:Hind;background-color:rgba(248,193,44,1);border-radius:3px 3px 3px 3px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;"></div>
											
 --></li>
											<li data-index="rs-6" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="[base_url]theme/theme/upload/slider-3-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description=""> <img src="[base_url]theme/theme/upload/slider-3.jpg" alt="" title="slider-3" width="1800" height="827" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina="">
												<div class="tp-caption   tp-resizeme" id="slide-6-layer-3" data-x="24" data-y="center" data-voffset="-12" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"from":"x:50px;opacity:0;","speed":300,"to":"o:1;","delay":340,"ease":"Power2.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' data-textalign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 10; white-space: nowrap; font-size: 70px;  font-weight: 700; color: rgba(248,193,44,1);font-family:Hind;">Pioneers in mould repairing</div>
												<div class="tp-caption   tp-resizeme hidden-xs" id="slide-6-layer-4" data-x="27" data-y="center" data-voffset="74" data-width="['666']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":300,"to":"o:1;","delay":640,"ease":"Power2.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' data-textalign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 11; min-width: 666px; max-width: 666px; white-space: normal; font-size: 20px; line-height: 30px; font-weight: 400; color: rgba(255,255,255,1);font-family:Hind;">We work with moulds and matrices, small components of different materials…</div>
											</li>
											<li data-index="rs-5" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="[base_url]theme/theme/upload/slider-2-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description=""> <img src="[base_url]theme/theme/upload/slider-2.jpg" alt="" title="slider-2" width="1800" height="827" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina="">
												<div class="tp-caption   tp-resizeme" id="slide-5-layer-2" data-x="24" data-y="center" data-voffset="-93" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"from":"x:-50px;opacity:0;","speed":300,"to":"o:1;","delay":310,"ease":"Power2.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' data-textalign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 12; white-space: nowrap; font-size: 54px;  font-weight: 300; color: rgba(247,247,247,1);font-family:Hind;">Our endorsement</div>
												<div class="tp-caption   tp-resizeme" id="slide-5-layer-3" data-x="20" data-y="center" data-voffset="1" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"from":"x:50px;opacity:0;","speed":300,"to":"o:1;","delay":620,"ease":"Power2.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' data-textalign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 13; white-space: nowrap; font-size: 110px;  font-weight: 700; color: rgba(248,193,44,1);font-family:Hind;text-transform:uppercase;">Professionalism</div>
												<div class="tp-caption   tp-resizeme hidden-xs" id="slide-5-layer-4" data-x="27" data-y="center" data-voffset="81" data-width="['666']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":300,"to":"o:1;","delay":880,"ease":"Power2.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' data-textalign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 14; min-width: 666px; max-width: 666px; white-space: normal; font-size: 20px; line-height: 30px; font-weight: 400; color: rgba(255,255,255,1);font-family:Hind;">Our experience and professionalism provide us with the utmost quality in high-end and high-precision welding</div>
												<!-- 
<div class="tp-caption rev-btn " id="slide-5-layer-6" data-x="25" data-y="center" data-voffset="165" data-width="['auto']" data-height="['auto']" data-type="button" data-responsive_offset="on" data-responsive="off" data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":300,"to":"o:1;","delay":1120,"ease":"Power2.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0,0,0,1);bg:rgba(247,204,86,1);"}]' data-textalign="['left','left','left','left']" data-paddingtop="[17,17,17,17]" data-paddingright="[34,34,34,34]" data-paddingbottom="[17,17,17,17]" data-paddingleft="[34,34,34,34]" style="z-index: 15; white-space: nowrap; font-size: 14px; line-height: 17px; font-weight: 600; color: rgba(10,10,10,1);font-family:Hind;background-color:rgba(248,193,44,1);border-radius:3px 3px 3px 3px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Treballs</div>
											
 --></li>
										</ul>
										<div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="gdlr-core-pbf-wrapper " style="padding: 50px 0px 50px 0px;" data-skin="Yellow Service">
				<div class="gdlr-core-pbf-background-wrap" style="background-color: #5795cc ;"></div>
				<div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
					<div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
						<div class="gdlr-core-pbf-element">
							<div class="gdlr-core-call-to-action-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-style-left-align-right-button" style="padding-bottom: 0px;">
								<div class="gdlr-core-call-to-action-item-inner">
									<div class="gdlr-core-call-to-action-item-content-wrap">
										<h3 class="gdlr-core-call-to-action-item-title" style="font-size: 26px;font-weight: 300;letter-spacing: 0px;">We are specialized repairing moulds and matrices</h3>
									</div>
									<div class="gdlr-core-call-to-action-item-button"><a class="gdlr-core-button  gdlr-core-button-solid gdlr-core-button-no-border" href="http://hipo.tv/moldarc/contacte.html" style="border-radius: 3px;-moz-border-radius: 3px;-webkit-border-radius: 3px;"><span class="gdlr-core-content">Contact us</span></a></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="gdlr-core-pbf-wrapper " style="padding: 0px 0px 0px 0px;">
				<div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
					<div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-pbf-wrapper-full-no-space">
						

						<a href="[base_url]servei/1-laser">

							<div class="visible-xs gdlr-core-pbf-column gdlr-core-column-15">
								<div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 60px 0px 60px 0px;" data-sync-height="height-service" data-sync-height-center="">
									<div class="gdlr-core-pbf-background-wrap" style="background-color: #64abea ;"></div>
										<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content">
											<div class="gdlr-core-pbf-element">
												<div class="gdlr-core-image-item gdlr-core-item-pdlr gdlr-core-item-pdb  gdlr-core-center-align" style="padding-bottom: 35px;">
													<div class="gdlr-core-image-item-wrap gdlr-core-media-image  gdlr-core-image-item-style-rectangle" style="border-width: 0px;"><img src="[base_url]theme/theme/upload/about-icon-11.png" alt="" width="104" height="60"></div>
												</div>
											</div>
											<div class="gdlr-core-pbf-element">
												<div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-center-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-bottom: 0px;">
													<div class="gdlr-core-title-item-title-wrap ">
														<h3 class="gdlr-core-title-item-title gdlr-core-skin-title "  style="font-size: 18px;font-weight: 300;letter-spacing: 0px;text-transform: none;">Laser<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

							<div class="gdlr-core-pbf-column gdlr-core-column-30 gdlr-core-column-first" data-skin="Dark">
								<div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 170px 70px 110px 100px;" data-sync-height="height-service">
									<div class="gdlr-core-pbf-background-wrap">
										<div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/service-7-1.jpg);background-size: cover;background-position: center;" data-parallax-speed="0.2"></div>
									</div>
									<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content">
										<div class="gdlr-core-pbf-element">
											<div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr">
												<div class="gdlr-core-title-item-title-wrap ">
													<h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 27px;font-weight: 300;letter-spacing: 0px;text-transform: none;color: #64abea;">Laser<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3>
												</div>
											</div>
										</div>
										<div class="gdlr-core-pbf-element">
											<div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" style="padding-bottom: 0px;">
												<div class="gdlr-core-text-box-item-content" style="font-size: 18px;">
													<p>Innovative technology of high-precision welding. This technique avoids potentially damages and any type of breakage (aluminium, nickel, titanium… and their alloys).</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="gdlr-core-pbf-column gdlr-core-column-15">
								<div class="gdlr-core-pbf-column-content-margin gdlr-core-js " data-sync-height="height-service">
									<div class="gdlr-core-pbf-background-wrap">
										<div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/service-8.jpg);background-size: cover;background-position: center;" data-parallax-speed="0"></div>
									</div>
									<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content"></div>
								</div>
							</div>
							<div class="hidden-xs gdlr-core-pbf-column gdlr-core-column-15">
								<div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 60px 0px 60px 0px;" data-sync-height="height-service" data-sync-height-center="">
									<div class="gdlr-core-pbf-background-wrap" style="background-color: #64abea ;"></div>
										<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content">
											<div class="gdlr-core-pbf-element">
												<div class="gdlr-core-image-item gdlr-core-item-pdlr gdlr-core-item-pdb  gdlr-core-center-align" style="padding-bottom: 35px;">
													<div class="gdlr-core-image-item-wrap gdlr-core-media-image  gdlr-core-image-item-style-rectangle" style="border-width: 0px;"><img src="[base_url]theme/theme/upload/about-icon-11.png" alt="" width="104" height="60"></div>
												</div>
											</div>
											<div class="gdlr-core-pbf-element">
												<div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-center-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-bottom: 0px;">
													<div class="gdlr-core-title-item-title-wrap ">
														<h3 class="gdlr-core-title-item-title gdlr-core-skin-title "  style="font-size: 18px;font-weight: 300;letter-spacing: 0px;text-transform: none;">Laser<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</a>

						</div>
					</div>
				</div>
				<div class="gdlr-core-pbf-wrapper " style="padding: 0px 0px 0px 0px;">
					<div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
						<div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-pbf-wrapper-full-no-space">
							

							<a href="[base_url]servei/2-microplasma">
									<div class="gdlr-core-pbf-column gdlr-core-column-15 gdlr-core-column-first">
										<div class="gdlr-core-pbf-column-content-margin gdlr-core-js " data-sync-height="height-service">
											<div class="gdlr-core-pbf-background-wrap">
												<div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/service-m.jpg);background-size: cover;background-position: center;" data-parallax-speed="0"></div>
											</div>
											<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content"></div>
										</div>
									</div>
									<div class="gdlr-core-pbf-column gdlr-core-column-15">
										<div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 60px 0px 60px 0px;" data-sync-height="height-service" data-sync-height-center="">
											<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content">
												<div class="gdlr-core-pbf-element">
													<div class="gdlr-core-image-item gdlr-core-item-pdlr gdlr-core-item-pdb  gdlr-core-center-align" style="padding-bottom: 35px;">
														<div class="gdlr-core-image-item-wrap gdlr-core-media-image  gdlr-core-image-item-style-rectangle" style="border-width: 0px;"><img src="[base_url]theme/theme/upload/about-icon-12.png" alt="" width="104" height="60"></div>
													</div>
												</div>
												<div class="gdlr-core-pbf-element">
													<div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-center-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-bottom: 0px;">
														<div class="gdlr-core-title-item-title-wrap ">
															<h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 18px;font-weight: 300;letter-spacing: 0px;text-transform: none;">Micro Plasma<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="gdlr-core-pbf-column gdlr-core-column-30" data-skin="Dark">
										<div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 150px 70px 90px 100px;" data-sync-height="height-service">
											<div class="gdlr-core-pbf-background-wrap">
												<div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/service-m-1.jpg);background-size: cover;background-position: center;" data-parallax-speed="0.2"></div>
											</div>
											<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content">
												<div class="gdlr-core-pbf-element">
													<div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr">
														<div class="gdlr-core-title-item-title-wrap ">
															<h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 27px;font-weight: 300;letter-spacing: 0px;text-transform: none;color: #64abea;">Micro Plasma Welding<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3>
														</div>
													</div>
												</div>
												<div class="gdlr-core-pbf-element">
													<div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" style="padding-bottom: 0px;">
														<div class="gdlr-core-text-box-item-content" style="font-size: 18px;">
															<p>Micro Plasma welding units of high-precision. <br>Security measure of material being processed.<br> Guarantee of a perfect component reproduction.</p>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</a>
						
					</div>
				</div>
			</div>
			<div class="gdlr-core-pbf-wrapper " style="padding: 0px 0px 0px 0px;">
				<div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
					<div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-pbf-wrapper-full-no-space">
						

						<a href="[base_url]servei/3-tig-pulsant">
							<div class="visible-xs gdlr-core-pbf-column gdlr-core-column-15" data-skin="White Text">
									<div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 60px 0px 60px 0px;" data-sync-height="height-service" data-sync-height-center="">
										<div class="gdlr-core-pbf-background-wrap" style="background-color: #000000 ;"></div>
											<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content">
												<div class="gdlr-core-pbf-element">
													<div class="gdlr-core-image-item gdlr-core-item-pdlr gdlr-core-item-pdb  gdlr-core-center-align" style="padding-bottom: 35px;">
														<div class="gdlr-core-image-item-wrap gdlr-core-media-image  gdlr-core-image-item-style-rectangle" style="border-width: 0px;"><img src="[base_url]theme/theme/upload/about-icon-13.png" alt="" width="104" height="60"></div>
													</div>
												</div>
												<div class="gdlr-core-pbf-element">
													<div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-center-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-bottom: 0px;">
														<div class="gdlr-core-title-item-title-wrap ">
															<h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 18px;font-weight: 300;letter-spacing: 0px;text-transform: none;">TIG Pulsant Welding<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3>
														</div>
													</div>
												</div>
											</div>
									</div>
								</div>
							<div class="gdlr-core-pbf-column gdlr-core-column-30 gdlr-core-column-first" data-skin="Dark">
								<div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 150px 70px 90px 100px;" data-sync-height="height-service">
									<div class="gdlr-core-pbf-background-wrap">
										<div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/tig2.jpg);background-size: cover;background-position: center;" data-parallax-speed="0.2"></div>
									</div>
									<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content">
										<div class="gdlr-core-pbf-element">
											<div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr">
												<div class="gdlr-core-title-item-title-wrap ">
													<h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 27px;font-weight: 300;letter-spacing: 0px;text-transform: none;color: #64abea;">TIG Pulsant Welding<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3>
												</div>
												</div>
											</div>
											<div class="gdlr-core-pbf-element">
												<div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" style="padding-bottom: 0px;">
													<div class="gdlr-core-text-box-item-content" style="font-size: 18px;">
														<p>This technique is intended for large welding contributions in big components.</p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="hidden-xs gdlr-core-pbf-column gdlr-core-column-15" data-skin="White Text">
									<div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 60px 0px 60px 0px;" data-sync-height="height-service" data-sync-height-center="">
										<div class="gdlr-core-pbf-background-wrap" style="background-color: #000000 ;"></div>
											<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content">
												<div class="gdlr-core-pbf-element">
													<div class="gdlr-core-image-item gdlr-core-item-pdlr gdlr-core-item-pdb  gdlr-core-center-align" style="padding-bottom: 35px;">
														<div class="gdlr-core-image-item-wrap gdlr-core-media-image  gdlr-core-image-item-style-rectangle" style="border-width: 0px;"><img src="[base_url]theme/theme/upload/about-icon-13.png" alt="" width="104" height="60"></div>
													</div>
												</div>
												<div class="gdlr-core-pbf-element">
													<div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-center-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-bottom: 0px;">
														<div class="gdlr-core-title-item-title-wrap ">
															<h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 18px;font-weight: 300;letter-spacing: 0px;text-transform: none;">TIG Pulsante<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3>
														</div>
													</div>
												</div>
											</div>
									</div>
								</div>
								<div class="gdlr-core-pbf-column gdlr-core-column-15">
									<div class="gdlr-core-pbf-column-content-margin gdlr-core-js " data-sync-height="height-service">
										<div class="gdlr-core-pbf-background-wrap">
											<div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/tig3.png);background-size: cover;background-position: center;" data-parallax-speed="0"></div>
										</div>
										<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content"></div>
									</div>
								</div>
							</a>


							</div>
						</div>
					</div>
					<div class="gdlr-core-pbf-wrapper " style="padding: 95px 0px 50px 0px;">
						<div class="gdlr-core-pbf-background-wrap">
							<div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/about-home-bg.jpg);background-size: cover;background-position: center;" data-parallax-speed="0.6"></div>
						</div>
						<div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
							<div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
								<div class="gdlr-core-pbf-column gdlr-core-column-20 gdlr-core-column-first" data-skin="Yellow Link">
									<div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 0px 10px 0px 10px;">
										<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
											<div class="gdlr-core-pbf-element">
												<div class="gdlr-core-column-service-item gdlr-core-item-pdb  gdlr-core-center-align gdlr-core-no-caption gdlr-core-item-pdlr">
													<div class="gdlr-core-column-service-media gdlr-core-media-image" style="margin-bottom: 40px;"><img src="[base_url]theme/theme/upload/about-icon-8.png" alt="" width="60" height="60"></div>
														<div class="gdlr-core-column-service-content-wrapper">
															<div class="gdlr-core-column-service-title-wrap" style="margin-bottom: 20px;">
																<h3 class="gdlr-core-column-service-title" style="font-size: 21px;font-weight: 300;letter-spacing: 0px;text-transform: none;">Personalized service</h3>
															</div>
															<div class="gdlr-core-column-service-content" style="font-size: 17px;">
																<p>We are always looking for the best solutions and advising the client what technique of welding he should use (Laser, Plasma, TIG or others). Always looking for the way to combine quality / price.</p> <a class="gdlr-core-column-service-read-more gdlr-core-info-font" href="#" style="font-size: 16px;"></a></div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="gdlr-core-pbf-column gdlr-core-column-20" data-skin="Yellow Link">
											<div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 0px 10px 0px 10px;">
												<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
													<div class="gdlr-core-pbf-element">
														<div class="gdlr-core-column-service-item gdlr-core-item-pdb  gdlr-core-center-align gdlr-core-no-caption gdlr-core-item-pdlr">
															<div class="gdlr-core-column-service-media gdlr-core-media-image" style="margin-bottom: 42px;"><img src="[base_url]theme/theme/upload/about-icon-9.png" alt="" width="60" height="60"></div>
																<div class="gdlr-core-column-service-content-wrapper">
																	<div class="gdlr-core-column-service-title-wrap" style="margin-bottom: 20px;">
																	<h3 class="gdlr-core-column-service-title" style="font-size: 21px;font-weight: 300;letter-spacing: 0px;text-transform: none;">Treatment</h3>
																</div>
																<div class="gdlr-core-column-service-content" style="font-size: 17px;">
																	<p>When we receive the material from the client we make a thorough analysis to be able to offer the client the different possibilities and solutions, always looking for the best quality / price combination.</p> <a class="gdlr-core-column-service-read-more gdlr-core-info-font" href="#" style="font-size: 16px;"></a></div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="gdlr-core-pbf-column gdlr-core-column-20" data-skin="Yellow Link">
												<div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 0px 10px 0px 10px;">
													<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
														<div class="gdlr-core-pbf-element">
															<div class="gdlr-core-column-service-item gdlr-core-item-pdb  gdlr-core-center-align gdlr-core-no-caption gdlr-core-item-pdlr">
																<div class="gdlr-core-column-service-media gdlr-core-media-image" style="margin-bottom: 40px;"><img src="[base_url]theme/theme/upload/about-icon-10.png" alt="" width="60" height="60"></div>
																	<div class="gdlr-core-column-service-content-wrapper">
																		<div class="gdlr-core-column-service-title-wrap" style="margin-bottom: 20px;">
																			<h3 class="gdlr-core-column-service-title" style="font-size: 21px;font-weight: 300;letter-spacing: 0px;text-transform: none;">Packaging</h3>
																		</div>
																		<div class="gdlr-core-column-service-content" style="font-size: 17px;">
																			<p>Once the work is finished we make stock and analysis photographs and place it and deliver it in a specific packaging to guarantee the integrity of the component.</p> <a class="gdlr-core-column-service-read-more gdlr-core-info-font" href="#" style="font-size: 16px;"></a></div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="gdlr-core-pbf-wrapper " style="padding: 75px 0px 15px 0px;">
											<div class="gdlr-core-pbf-background-wrap">
												<div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/port-bg-2-1.jpg);background-size: cover;background-position: center;" data-parallax-speed="0.4"></div>
											</div>
											<div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
												<div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
													<div class="gdlr-core-pbf-column gdlr-core-column-15 gdlr-core-column-first">
														<div class="gdlr-core-pbf-column-content-margin gdlr-core-js ">
														<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
														<div class="gdlr-core-pbf-element">
														<div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr">
														<div class="gdlr-core-title-item-title-wrap ">
														<h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 26px;font-weight: 400;letter-spacing: 0px;text-transform: none;color: #64abea;">Latest projects<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="gdlr-core-pbf-column gdlr-core-column-30" data-skin="White Text">
									<div class="gdlr-core-pbf-column-content-margin gdlr-core-js ">
										<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
											<div class="gdlr-core-pbf-element">
												<div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align">
													<div class="gdlr-core-text-box-item-content" style="font-size: 16px;">
														<p>Get to know our latest projects, <br>find out what we can do for you
														<br> </p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="gdlr-core-pbf-column gdlr-core-column-15" data-skin="White Text">
									<div class="gdlr-core-pbf-column-content-margin gdlr-core-js ">
										<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
											<div class="gdlr-core-pbf-element">
												<div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-right-align">
													<div class="gdlr-core-text-box-item-content">
														<p><a href="[base_url]ultimos-trabajos.html#projectes">More projects <i class="fa fa-long-arrow-right" style="font-size: 16px;color: #fff;margin-left: 15px;"></i></a></p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="gdlr-core-pbf-wrapper " style="padding: 0px 0px 0px 0px;">
						<div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
							<div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-pbf-wrapper-full-no-space">
								<div class="gdlr-core-pbf-column gdlr-core-column-60 gdlr-core-column-first" data-skin="Portfolio Homepage">
									<div class="gdlr-core-pbf-column-content-margin gdlr-core-js " data-sync-height="height-port">
										<div class="gdlr-core-pbf-background-wrap">
											<div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/port-bg.jpg);background-size: cover;background-position: center;" data-parallax-speed="0"></div>
										</div>
										<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content">
											<div class="gdlr-core-pbf-element">
												<div class="gdlr-core-portfolio-item gdlr-core-item-pdb clearfix  gdlr-core-portfolio-item-style-grid-no-space gdlr-core-item-pdlr" style="padding-bottom: 10px;">
													<div class="gdlr-core-flexslider flexslider gdlr-core-js-2 " data-type="carousel" data-column="5" data-nav="navigation" data-nav-parent="gdlr-core-portfolio-item" data-disable-autoslide="1">
														<ul class="slides">
															<?php foreach($this->db->get_where('ultimos_trabajos',array('idioma'=>$_SESSION['lang']))->result() as $n=>$v): ?>
																<li>
																	<div class="gdlr-core-portfolio-grid  gdlr-core-left-align gdlr-core-style-normal">
																		<div class="gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-title-icon">
																			<div class="gdlr-core-portfolio-thumbnail-image-wrap  gdlr-core-zoom-on-hover">
																				<img src="<?= base_url('img/servicios/'.$v->miniatura) ?>" alt="" width="500" height="625">
																				<span class="gdlr-core-image-overlay  gdlr-core-portfolio-overlay gdlr-core-image-overlay-center-icon gdlr-core-js">
																					<span class="gdlr-core-image-overlay-content">
																						<span class="gdlr-core-portfolio-title gdlr-core-title-font">
																							<a href="#"><?= $v->titulo ?></a>
																						</span>
																						<span class="gdlr-core-portfolio-icon-wrap">
																							<a class="gdlr-core-ilightbox gdlr-core-js " href="<?= base_url('img/servicios/'.$v->foto) ?>" data-ilightbox-group="gdlr-core-img-group-1">
																								<i class="gdlr-core-portfolio-icon icon_zoom-in_alt"></i>
																							</a>
																						</span>
																					</span>
																				</span>
																			</div>
																		</div>
																		<div class="gdlr-core-portfolio-content-wrap gdlr-core-skin-divider">
																			<h3 class="gdlr-core-portfolio-title gdlr-core-skin-title" style="font-size: 18px;font-weight: 600;letter-spacing: 0px;text-transform: none;">
																				<?= $v->titulo ?>
																			</h3>
																			<span class="gdlr-core-portfolio-info gdlr-core-portfolio-info-tag gdlr-core-info-font  gdlr-core-skin-caption">
	                                                                            <?php foreach(explode(',',$v->tags) as $t): ?>
	                                                                                
	                                                                                    <?= $t ?>
	                                                                                
	                                                                                <span class="gdlr-core-sep">/</span> 
	                                                                            <?php endforeach ?>
	                                                                        </span>
																		</div>
																	</div>
																</li>
															<?php endforeach ?>




															<!--<li>
																<div class="gdlr-core-portfolio-grid  gdlr-core-left-align gdlr-core-style-normal">
																	<div class="gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-title-icon">
																		<div class="gdlr-core-portfolio-thumbnail-image-wrap  gdlr-core-zoom-on-hover"><img src="[base_url]theme/theme/upload/pexels-photo-109915-500x625.jpg" alt="" width="500" height="625"><span class="gdlr-core-image-overlay  gdlr-core-portfolio-overlay gdlr-core-image-overlay-center-icon gdlr-core-js"><span class="gdlr-core-image-overlay-content"><span class="gdlr-core-portfolio-title gdlr-core-title-font"><a href="../#">Agulles</a></span><span class="gdlr-core-portfolio-icon-wrap"><a class="gdlr-core-ilightbox gdlr-core-js " href="[base_url]theme/theme/upload/pexels-photo-109915.jpg" data-ilightbox-group="gdlr-core-img-group-1"><i class="gdlr-core-portfolio-icon icon_zoom-in_alt"></i></a></span></span></span></div>
																	</div>
																	<div class="gdlr-core-portfolio-content-wrap gdlr-core-skin-divider">
																		<h3 class="gdlr-core-portfolio-title gdlr-core-skin-title" style="font-size: 18px;font-weight: 600;letter-spacing: 0px;text-transform: none;"><a href="../#">Agulles</a></h3><span class="gdlr-core-portfolio-info gdlr-core-portfolio-info-tag gdlr-core-info-font gdlr-core-skin-caption"><a href="../#" rel="tag">Làser</a> <span class="gdlr-core-sep">/</span> <a href="../#" rel="tag">Soldadura de precisió</a></span>
																	</div>
																</div>
															</li>
															<li>
																<div class="gdlr-core-portfolio-grid  gdlr-core-left-align gdlr-core-style-normal">
																	<div class="gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-title-icon">
																		<div class="gdlr-core-portfolio-thumbnail-image-wrap  gdlr-core-zoom-on-hover"><img src="[base_url]theme/theme/upload/pr1b.jpg" alt="" width="500" height="625"><span class="gdlr-core-image-overlay  gdlr-core-portfolio-overlay gdlr-core-image-overlay-center-icon gdlr-core-js"><span class="gdlr-core-image-overlay-content"><span class="gdlr-core-portfolio-title gdlr-core-title-font"><a href="../#">Extrusió</a></span><span class="gdlr-core-portfolio-icon-wrap"><a class="gdlr-core-ilightbox gdlr-core-js " href="[base_url]theme/theme/upload/pr1.jpg" data-ilightbox-group="gdlr-core-img-group-1"><i class="gdlr-core-portfolio-icon icon_zoom-in_alt"></i></a></span></span></span></div>
																	</div>
																	<div class="gdlr-core-portfolio-content-wrap gdlr-core-skin-divider">
																		<h3 class="gdlr-core-portfolio-title gdlr-core-skin-title" style="font-size: 18px;font-weight: 600;letter-spacing: 0px;text-transform: none;"><a href="../#">Extrusió</a></h3><span class="gdlr-core-portfolio-info gdlr-core-portfolio-info-tag gdlr-core-info-font gdlr-core-skin-caption"><a href="../#" rel="tag">Microplasma</a> <span class="gdlr-core-sep">/</span> <a href="../#" rel="tag">Soldat de relleus</a></span>
																	</div>
																</div>
															</li>
															<li>
																<div class="gdlr-core-portfolio-grid  gdlr-core-left-align gdlr-core-style-normal">
																	<div class="gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-title-icon">
																		<div class="gdlr-core-portfolio-thumbnail-image-wrap  gdlr-core-zoom-on-hover"><img src="[base_url]theme/theme/upload/pr2b.jpg" alt="" width="500" height="625"><span class="gdlr-core-image-overlay  gdlr-core-portfolio-overlay gdlr-core-image-overlay-center-icon gdlr-core-js"><span class="gdlr-core-image-overlay-content"><span class="gdlr-core-portfolio-title gdlr-core-title-font"><a href="../#">Gravat</a></span><span class="gdlr-core-portfolio-icon-wrap"><a class="gdlr-core-ilightbox gdlr-core-js " href="[base_url]theme/theme/upload/pr2.jpg" data-ilightbox-group="gdlr-core-img-group-1"><i class="gdlr-core-portfolio-icon icon_zoom-in_alt"></i></a></span></span></span></div>
																	</div>
																	<div class="gdlr-core-portfolio-content-wrap gdlr-core-skin-divider">
																		<h3 class="gdlr-core-portfolio-title gdlr-core-skin-title" style="font-size: 18px;font-weight: 600;letter-spacing: 0px;text-transform: none;"><a href="../#">Gravat</a></h3><span class="gdlr-core-portfolio-info gdlr-core-portfolio-info-tag gdlr-core-info-font gdlr-core-skin-caption"><a href="../#" rel="tag">Làser</a> <span class="gdlr-core-sep">/</span> <a href="../#" rel="tag">Tapat de gravat</a></span>
																	</div>
																</div>
															</li>
															<li>
																<div class="gdlr-core-portfolio-grid  gdlr-core-left-align gdlr-core-style-normal">
																	<div class="gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-title-icon">
																		<div class="gdlr-core-portfolio-thumbnail-image-wrap  gdlr-core-zoom-on-hover"><img src="[base_url]theme/theme/upload/pr3b.jpg" alt="" width="500" height="625"><span class="gdlr-core-image-overlay  gdlr-core-portfolio-overlay gdlr-core-image-overlay-center-icon gdlr-core-js"><span class="gdlr-core-image-overlay-content"><span class="gdlr-core-portfolio-title gdlr-core-title-font"><a href="../#">Correcció motlle</a></span><span class="gdlr-core-portfolio-icon-wrap"><a class="gdlr-core-ilightbox gdlr-core-js " href="[base_url]theme/theme/upload/pr3.jpg" data-ilightbox-group="gdlr-core-img-group-1"><i class="gdlr-core-portfolio-icon icon_zoom-in_alt"></i></a></span></span></span></div>
																	</div>
																	<div class="gdlr-core-portfolio-content-wrap gdlr-core-skin-divider">
																		<h3 class="gdlr-core-portfolio-title gdlr-core-skin-title" style="font-size: 18px;font-weight: 600;letter-spacing: 0px;text-transform: none;"><a href="../#">Làser</a></h3><span class="gdlr-core-portfolio-info gdlr-core-portfolio-info-tag gdlr-core-info-font gdlr-core-skin-caption"><a href="../#" rel="tag">Làser</a> <span class="gdlr-core-sep">/</span> <a href="../#" rel="tag">Motlles</a></span>
																	</div>
																</div>
															</li>
															<li>
																<div class="gdlr-core-portfolio-grid  gdlr-core-left-align gdlr-core-style-normal">
																	<div class="gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-title-icon">
																		<div class="gdlr-core-portfolio-thumbnail-image-wrap  gdlr-core-zoom-on-hover"><img src="[base_url]theme/theme/upload/pr4b.jpg" alt="" width="500" height="625"><span class="gdlr-core-image-overlay  gdlr-core-portfolio-overlay gdlr-core-image-overlay-center-icon gdlr-core-js"><span class="gdlr-core-image-overlay-content"><span class="gdlr-core-portfolio-title gdlr-core-title-font"><a href="../#">TIG pulsant</a></span><span class="gdlr-core-portfolio-icon-wrap"><a class="gdlr-core-ilightbox gdlr-core-js " href="[base_url]theme/theme/upload/pr4.jpg" data-ilightbox-group="gdlr-core-img-group-1"><i class="gdlr-core-portfolio-icon icon_zoom-in_alt"></i></a></span></span></span></div>
																	</div>
																	<div class="gdlr-core-portfolio-content-wrap gdlr-core-skin-divider">
																		<h3 class="gdlr-core-portfolio-title gdlr-core-skin-title" style="font-size: 18px;font-weight: 600;letter-spacing: 0px;text-transform: none;"><a href="../#">TIG pulsant</a></h3><span class="gdlr-core-portfolio-info gdlr-core-portfolio-info-tag gdlr-core-info-font gdlr-core-skin-caption"><a href="../#" rel="tag">TIG</a> <span class="gdlr-core-sep">/</span> <a href="../#" rel="tag">Soldadura correcció</a></span>
																	</div>
																</div>
															</li>
															<li>
																<div class="gdlr-core-portfolio-grid  gdlr-core-left-align gdlr-core-style-normal">
																	<div class="gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-title-icon">
																		<div class="gdlr-core-portfolio-thumbnail-image-wrap  gdlr-core-zoom-on-hover"><img src="[base_url]theme/theme/upload/shutterstock_161515241-500x625.jpg" alt="" width="500" height="625"><span class="gdlr-core-image-overlay  gdlr-core-portfolio-overlay gdlr-core-image-overlay-center-icon gdlr-core-js"><span class="gdlr-core-image-overlay-content"><span class="gdlr-core-portfolio-title gdlr-core-title-font"><a href="../#">Hamburg Wind Energy Plant</a></span><span class="gdlr-core-portfolio-icon-wrap"><a class="gdlr-core-ilightbox gdlr-core-js " href="[base_url]theme/theme/upload/shutterstock_161515241.jpg" data-ilightbox-group="gdlr-core-img-group-1"><i class="gdlr-core-portfolio-icon icon_zoom-in_alt"></i></a></span></span></span></div>
																	</div>
																	<div class="gdlr-core-portfolio-content-wrap gdlr-core-skin-divider">
																		<h3 class="gdlr-core-portfolio-title gdlr-core-skin-title" style="font-size: 18px;font-weight: 600;letter-spacing: 0px;text-transform: none;"><a href="../#">Hamburg Wind Energy Plant</a></h3><span class="gdlr-core-portfolio-info gdlr-core-portfolio-info-tag gdlr-core-info-font gdlr-core-skin-caption"><a href="../#" rel="tag">Energy</a> <span class="gdlr-core-sep">/</span> <a href="../#" rel="tag">System</a></span>
																	</div>
																</div>
															</li>
															<li>
																<div class="gdlr-core-portfolio-grid  gdlr-core-left-align gdlr-core-style-normal">
																	<div class="gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-title-icon">
																		<div class="gdlr-core-portfolio-thumbnail-image-wrap  gdlr-core-zoom-on-hover"><img src="[base_url]theme/theme/upload/pexels-photo-24276-500x625.jpg" alt="" width="500" height="625"><span class="gdlr-core-image-overlay  gdlr-core-portfolio-overlay gdlr-core-image-overlay-center-icon gdlr-core-js"><span class="gdlr-core-image-overlay-content"><span class="gdlr-core-portfolio-title gdlr-core-title-font"><a href="../#">Singapore Logistic Port</a></span><span class="gdlr-core-portfolio-icon-wrap"><a class="gdlr-core-ilightbox gdlr-core-js " href="[base_url]theme/theme/upload/pexels-photo-24276.jpg" data-ilightbox-group="gdlr-core-img-group-1"><i class="gdlr-core-portfolio-icon icon_zoom-in_alt"></i></a></span></span></span></div>
																	</div>
																	<div class="gdlr-core-portfolio-content-wrap gdlr-core-skin-divider">
																		<h3 class="gdlr-core-portfolio-title gdlr-core-skin-title" style="font-size: 18px;font-weight: 600;letter-spacing: 0px;text-transform: none;"><a href="../#">Singapore Logistic Port</a></h3><span class="gdlr-core-portfolio-info gdlr-core-portfolio-info-tag gdlr-core-info-font gdlr-core-skin-caption"><a href="../#" rel="tag">Logistic</a> <span class="gdlr-core-sep">/</span> <a href="../#" rel="tag">Port</a></span>
																	</div>
																</div>
															</li>
															<li>
																<div class="gdlr-core-portfolio-grid  gdlr-core-left-align gdlr-core-style-normal">
																	<div class="gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-title-icon">
																		<div class="gdlr-core-portfolio-thumbnail-image-wrap  gdlr-core-zoom-on-hover"><img src="[base_url]theme/theme/upload/pexels-photo-3-500x625.jpg" alt="" width="500" height="625"><span class="gdlr-core-image-overlay  gdlr-core-portfolio-overlay gdlr-core-image-overlay-center-icon gdlr-core-js"><span class="gdlr-core-image-overlay-content"><span class="gdlr-core-portfolio-title gdlr-core-title-font"><a href="../#">Berlin Central Bank</a></span><span class="gdlr-core-portfolio-icon-wrap"><a class="gdlr-core-ilightbox gdlr-core-js " href="[base_url]theme/theme/upload/pexels-photo-3.jpg" data-ilightbox-group="gdlr-core-img-group-1"><i class="gdlr-core-portfolio-icon icon_zoom-in_alt"></i></a></span></span></span></div>
																	</div>
																	<div class="gdlr-core-portfolio-content-wrap gdlr-core-skin-divider">
																		<h3 class="gdlr-core-portfolio-title gdlr-core-skin-title" style="font-size: 18px;font-weight: 600;letter-spacing: 0px;text-transform: none;"><a href="../#">Berlin Central Bank</a></h3><span class="gdlr-core-portfolio-info gdlr-core-portfolio-info-tag gdlr-core-info-font gdlr-core-skin-caption"><a href="../#" rel="tag">Bank</a> <span class="gdlr-core-sep">/</span> <a href="../#" rel="tag">Constructions</a></span>
																	</div>
																</div>
															</li>
															<li>
																<div class="gdlr-core-portfolio-grid  gdlr-core-left-align gdlr-core-style-normal">
																	<div class="gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-title-icon">
																		<div class="gdlr-core-portfolio-thumbnail-image-wrap  gdlr-core-zoom-on-hover"><img src="[base_url]theme/theme/upload/aircraft-manchester-jet-fly-500x625.jpg" alt="" width="500" height="625"><span class="gdlr-core-image-overlay  gdlr-core-portfolio-overlay gdlr-core-image-overlay-center-icon gdlr-core-js"><span class="gdlr-core-image-overlay-content"><span class="gdlr-core-portfolio-title gdlr-core-title-font"><a href="../#">New York Airport System Integration</a></span><span class="gdlr-core-portfolio-icon-wrap"><a class="gdlr-core-ilightbox gdlr-core-js " href="[base_url]theme/theme/upload/aircraft-manchester-jet-fly.jpg" data-ilightbox-group="gdlr-core-img-group-1"><i class="gdlr-core-portfolio-icon icon_zoom-in_alt"></i></a></span></span></span></div>
																	</div>
																	<div class="gdlr-core-portfolio-content-wrap gdlr-core-skin-divider">
																		<h3 class="gdlr-core-portfolio-title gdlr-core-skin-title" style="font-size: 18px;font-weight: 600;letter-spacing: 0px;text-transform: none;"><a href="../#">New York Airport System Integration</a></h3><span class="gdlr-core-portfolio-info gdlr-core-portfolio-info-tag gdlr-core-info-font gdlr-core-skin-caption"><a href="../#" rel="tag">Airport</a> <span class="gdlr-core-sep">/</span> <a href="../#" rel="tag">Constructions</a></span>
																	</div>
																</div>
															</li>-->






														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="gdlr-core-pbf-wrapper " style="padding: 45px 0px 15px 0px;">
						<div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
							<div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
								<div class="gdlr-core-pbf-element">
									<div class="gdlr-core-gallery-item gdlr-core-item-pdb clearfix  gdlr-core-gallery-item-style-grid gdlr-core-item-pdlr ">
										<div class="gdlr-core-flexslider flexslider gdlr-core-js-2 " data-type="carousel" data-column="5" data-nav="none" data-nav-parent="gdlr-core-blog-item">
											<ul class="slides">
												<li class="gdlr-core-item-mglr">
													<div class="gdlr-core-gallery-list gdlr-core-media-image">
														<a href="#" target="_self"><img src="[base_url]theme/theme/upload/banner-1.jpg" alt="" width="420" height="240"></a>
													</div>
												</li>
												<li class="gdlr-core-item-mglr">
													<div class="gdlr-core-gallery-list gdlr-core-media-image">
														<a href="#" target="_self"><img src="[base_url]theme/theme/upload/banner-2.jpg" alt="" width="420" height="240"></a>
													</div>
												</li>
												<li class="gdlr-core-item-mglr">
													<div class="gdlr-core-gallery-list gdlr-core-media-image">
														<a href="#" target="_self"><img src="[base_url]theme/theme/upload/banner-3.jpg" alt="" width="420" height="240"></a>
													</div>
												</li>
												<li class="gdlr-core-item-mglr">
													<div class="gdlr-core-gallery-list gdlr-core-media-image">
														<a href="#" target="_self"><img src="[base_url]theme/theme/upload/banner-4.jpg" alt="" width="420" height="240"></a>
													</div>
												</li>
												<li class="gdlr-core-item-mglr">
													<div class="gdlr-core-gallery-list gdlr-core-media-image">
														<a href="#" target="_self"><img src="[base_url]theme/theme/upload/banner-5.jpg" alt="" width="420" height="240"></a>
													</div>
												</li>
												<li class="gdlr-core-item-mglr">
													<div class="gdlr-core-gallery-list gdlr-core-media-image">
														<a href="#" target="_self"><img src="[base_url]theme/theme/upload/banner-6.jpg" alt="" width="420" height="240"></a>
													</div>
												</li>
												<li class="gdlr-core-item-mglr">
													<div class="gdlr-core-gallery-list gdlr-core-media-image">
														<a href="#" target="_self"><img src="[base_url]theme/theme/upload/banner-7.jpg" alt="" width="420" height="240"></a>
													</div>
												</li>
												<li class="gdlr-core-item-mglr">
													<div class="gdlr-core-gallery-list gdlr-core-media-image">
														<a href="#" target="_self"><img src="[base_url]theme/theme/upload/banner-8.jpg" alt="" width="420" height="240"></a>
													</div>
												</li>
												<li class="gdlr-core-item-mglr">
													<div class="gdlr-core-gallery-list gdlr-core-media-image">
														<a href="#" target="_self"><img src="[base_url]theme/theme/upload/banner-9.jpg" alt="" width="420" height="240"></a>
													</div>
												</li>
												<li class="gdlr-core-item-mglr">
													<div class="gdlr-core-gallery-list gdlr-core-media-image">
														<a href="#" target="_self"><img src="[base_url]theme/theme/upload/banner-10.jpg" alt="" width="420" height="240"></a>
													</div>
												</li>
												<li class="gdlr-core-item-mglr">
													<div class="gdlr-core-gallery-list gdlr-core-media-image">
														<a href="#" target="_self"><img src="[base_url]theme/theme/upload/banner-11.jpg" alt="" width="420" height="240"></a>
													</div>
												</li>
												<li class="gdlr-core-item-mglr">
													<div class="gdlr-core-gallery-list gdlr-core-media-image">
														<a href="#" target="_self"><img src="[base_url]theme/theme/upload/banner-12.jpg" alt="" width="420" height="240"></a>
													</div>
												</li>
												<li class="gdlr-core-item-mglr">
													<div class="gdlr-core-gallery-list gdlr-core-media-image">
														<a href="#" target="_self"><img src="[base_url]theme/theme/upload/banner-13.jpg" alt="" width="420" height="240"></a>
													</div>
												</li>
												<li class="gdlr-core-item-mglr">
													<div class="gdlr-core-gallery-list gdlr-core-media-image">
														<a href="#" target="_self"><img src="[base_url]theme/theme/upload/banner-14.jpg" alt="" width="420" height="240"></a>
													</div>
												</li>
												<li class="gdlr-core-item-mglr">
													<div class="gdlr-core-gallery-list gdlr-core-media-image">
														<a href="#" target="_self"><img src="[base_url]theme/theme/upload/banner-15.jpg" alt="" width="420" height="240"></a>
													</div>
												</li>
												<li class="gdlr-core-item-mglr">
													<div class="gdlr-core-gallery-list gdlr-core-media-image">
														<a href="#" target="_self"><img src="[base_url]theme/theme/upload/banner-16.jpg" alt="" width="420" height="240"></a>
													</div>
												</li>
												<li class="gdlr-core-item-mglr">
													<div class="gdlr-core-gallery-list gdlr-core-media-image">
														<a href="#" target="_self"><img src="[base_url]theme/theme/upload/banner-17.jpg" alt="" width="420" height="240"></a>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="gdlr-core-pbf-wrapper " style="padding: 170px 0px 160px 0px;" data-skin="Dark">
						<div class="gdlr-core-pbf-background-wrap">
							<div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/worker.jpg);background-size: cover;background-position: center;" data-parallax-speed="0.2"></div>
						</div>
						<div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
							<div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
								<div class="gdlr-core-pbf-column gdlr-core-column-30 gdlr-core-column-first">
									<div class="gdlr-core-pbf-column-content-margin gdlr-core-js ">
										<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js "></div>
									</div>
								</div>
								<div class="gdlr-core-pbf-column gdlr-core-column-30">
									<div class="gdlr-core-pbf-column-content-margin gdlr-core-js ">
										<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
											<div class="gdlr-core-pbf-element">
												<div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-bottom: 35px;">
													<div class="gdlr-core-title-item-title-wrap ">
														<h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 34px;font-weight: 300;letter-spacing: 0px;text-transform: none;color: #ffffff;">We are professionals with more than 30 years of experience<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3></div>
													</div>
												</div>
												<div class="gdlr-core-pbf-element">
													<div class="gdlr-core-button-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" style="padding-bottom: 0px;"><a class="gdlr-core-button  gdlr-core-button-solid gdlr-core-button-no-border" href="[base_url]lempresa.html" style="font-size: 14px;color: #141414;border-radius: 3px;-moz-border-radius: 3px;-webkit-border-radius: 3px;background: #64abea ;"><span class="gdlr-core-content">Who are we</span></a></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="gdlr-core-pbf-wrapper " style="padding: 0px 0px 0px 0px;">
							<div class="gdlr-core-pbf-background-wrap" style="background-color: #ececec ;"></div>
								<div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
									<div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
										<div class="gdlr-core-pbf-column gdlr-core-column-20 gdlr-core-column-first">
											<div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 85px 6px 0px 0px;" data-sync-height="height-financial">
												<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content">
													<div class="gdlr-core-pbf-element">
														<div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-bottom: 39px;">
															<div class="gdlr-core-title-item-title-wrap ">
																<h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 28px;font-weight: 500;letter-spacing: 0px;text-transform: none;color: #141414;">Where we are<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3></div>
															</div>
														</div>
														<div class="gdlr-core-pbf-element">
															<div class="gdlr-core-icon-list-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-with-divider">
																<ul>
																	<li class=" gdlr-core-skin-divider gdlr-core-with-hover" style="border-color: #d8d8d8;margin-bottom: 3px;"><a href="#" target="_self"><span class="gdlr-core-icon-list-icon-wrap"><i class="gdlr-core-icon-list-icon-hover icon_check_alt2" style="font-size: 16px;width: 16px;"></i><i class="gdlr-core-icon-list-icon icon_check_alt2" style="font-size: 16px;width: 16px;"></i></span><span class="gdlr-core-icon-list-content" style="font-size: 15px;">C/ Pere IV, 29-35 bajos 08018 Barcelona</span></a></li>
																	<li class=" gdlr-core-skin-divider gdlr-core-with-hover" style="border-color: #d8d8d8;margin-bottom: 3px;"><a href="tel:933001776" target="_self"><span class="gdlr-core-icon-list-icon-wrap"><i class="gdlr-core-icon-list-icon-hover icon_check_alt2" style="font-size: 16px;width: 16px;"></i><i class="gdlr-core-icon-list-icon icon_check_alt2" style="font-size: 16px;width: 16px;"></i></span><span class="gdlr-core-icon-list-content" style="font-size: 15px;">Tel. 93 300 17 76</span></a></li>
																	<li class=" gdlr-core-skin-divider gdlr-core-with-hover" style="border-color: #d8d8d8;margin-bottom: 3px;"><a href="mailto:info@moldarc.com" target="_self"><span class="gdlr-core-icon-list-icon-wrap"><i class="gdlr-core-icon-list-icon-hover icon_check_alt2" style="font-size: 16px;width: 16px;"></i><i class="gdlr-core-icon-list-icon icon_check_alt2" style="font-size: 16px;width: 16px;"></i></span><span class="gdlr-core-icon-list-content" style="font-size: 15px;">info@moldarc.com</a></li>
																</ul>
															</div>
														</div>
														<div class="gdlr-core-pbf-element">
															<div class="gdlr-core-image-item gdlr-core-item-pdlr gdlr-core-item-pdb  gdlr-core-center-align" style="padding-bottom: 25px;">
																<div class="gdlr-core-image-item-wrap gdlr-core-media-image  gdlr-core-image-item-style-rectangle" style="border-width: 0px;"><img src="[base_url]theme/theme/upload/shutterstock_207507838-800x420.jpg" alt="" width="800" height="420"></div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="gdlr-core-pbf-column gdlr-core-column-20">
												<div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 85px 30px 40px 10px;" data-sync-height="height-financial">
													<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content">
														<div class="gdlr-core-pbf-element">
															<div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-bottom: 55px;" id="gdlr-core-title-item-id-57381">
																<div class="gdlr-core-title-item-title-wrap  gdlr-core-js-2 gdlr-core-with-link-text">
																	<h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 28px;font-weight: 500;letter-spacing: 0px;text-transform: none;color: #141414;">
																		Contact form
																		<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span>
																	</h3>
																</div>
															</div>
														</div>


														<div class="gdlr-core-pbf-element">
															<div class="gdlr-core-blog-item gdlr-core-item-pdb clearfix  gdlr-core-style-blog-list">
																<div class="gdlr-core-blog-item-holder gdlr-core-js-2 clearfix" data-layout="fitrows">
																	

																	<form action="" onsubmit="return false" class="mainContact">
																		<div class="gdlr-core-item-list gdlr-core-blog-list gdlr-core-item-pdlr  gdlr-core-blog-list-with-frame">
																			<div class="gdlr-core-blog-list-frame gdlr-core-skin-e-background gdlr-core-input-wrap gdlr-core-large gdlr-core-full-width gdlr-core-with-column">

																				<span class="quform-element wpcf7-form-control-wrap">
																					<!-- 
<h3 class="gdlr-core-blog-title gdlr-core-skin-title">
																						Nom
																					</h3>
 -->
																					<input type="text" class="wpcf7-form-control wpcf7-text wpcf7" name="nombre" placeholder="Name">
																				</span>
																			
																		

																		
																			

																				<span class="quform-element wpcf7-form-control-wrap">
																					<!-- 
<h3 class="gdlr-core-blog-title gdlr-core-skin-title">
																						Email
																					</h3>
 -->
																					<input type="text" class="wpcf7-form-control wpcf7-text wpcf7" name="nombre" placeholder="Email">
																				</span>
																			
																		

																		
																			

																				<span class="quform-element wpcf7-form-control-wrap">
																					<!-- 
<h3 class="gdlr-core-blog-title gdlr-core-skin-title">
																						Telèfon
																					</h3>
 -->
																					<input type="text" class="wpcf7-form-control wpcf7-text wpcf7" name="nombre" placeholder="Phone">
																				</span>
																			
																		

																		
																			

																				<span class="quform-element wpcf7-form-control-wrap">
																					<!-- 
<h3 class="gdlr-core-blog-title gdlr-core-skin-title">
																						Missatge
																					</h3>
 -->
																					<textarea class="wpcf7-form-control wpcf7-text wpcf7" name="nombre" placeholder="Message"></textarea>
																				</span>
																			
																		
																				<span class="quform-element wpcf7-form-control-wrap">
																					<h3 class="gdlr-core-blog-title gdlr-core-skin-title">
																						<button type="submit" class="submit-button"><span><em>Send</em></span></button>
																					</h3>
																					
																				</span>
																		
																			

																				
																			</div>
																		</div>
																	</form>


																</div>
															</div>
														</div>


													</div>
												</div>
											</div>
												<div class="gdlr-core-pbf-column gdlr-core-column-20" data-skin="Dark">
													<div class="gdlr-core-pbf-column-content-margin gdlr-core-js  gdlr-core-column-extend-right" style="padding: 80px 0px 40px 45px;" data-sync-height="height-financial">
														<div class="gdlr-core-pbf-background-wrap">
															<div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/career-bg.jpg);background-size: cover;background-position: center;" data-parallax-speed="0.2"></div>
														</div>
														<div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content">
															<div class="gdlr-core-pbf-element">
																<div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-bottom: 45px;">
																	<div class="gdlr-core-title-item-title-wrap ">
																		<h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 35px;font-weight: 300;letter-spacing: 0px;text-transform: none;color: #64abea;">Moldarc<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3>
																	</div>
																</div>
															</div>
															<div class="gdlr-core-pbf-element">
																<div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align">
																	<div class="gdlr-core-text-box-item-content" style="font-size: 18px;">
																		<p>Mold-Arc is a company with more than 30 years of experience in precision welding. We are pioneers in the repair of moulds and matrices, small pieces of various steels (copper, stainless steel, aluminum, titanium, bronze and their alloys). Our professionalism and experience guarantee us, resulting in high quality and precision welding.</p>
																	</div>
																</div>
															</div>
															<div class="gdlr-core-pbf-element">
																<div class="gdlr-core-button-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align"><a class="gdlr-core-button  gdlr-core-button-transparent gdlr-core-button-with-border" href="[base_url]lempresa.html" style="font-size: 15px;color: #64abea;padding: 15px 28px 15px 28px;border-radius: 3px;-moz-border-radius: 3px;-webkit-border-radius: 3px;border-width: 2px 2px 2px 2px;border-color: #64abea;"><span class="gdlr-core-content">Company details</span></a></div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							[footer]
						</div>
					</div>