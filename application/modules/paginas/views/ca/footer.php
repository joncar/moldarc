<footer>
	<div class="realfactory-footer-wrapper">
		<div class="realfactory-footer-container realfactory-container clearfix">
			<div class="realfactory-footer-column realfactory-item-pdlr realfactory-column-15">
				<div id="text-3" class="widget widget_text realfactory-widget">
					<div class="textwidget"><span class="gdlr-core-space-shortcode" style="margin-top: -2px;"></span> <img src="[base_url]theme/theme/upload/logo-white.png" alt=""> <span class="gdlr-core-space-shortcode" style="margin-top: 7px;"></span>Mold-Arc és una empresa amb més de 30 anys d'experiència en soldadures de precisió. Som pioners en la reparació de motlles i matrius, petites peces d'acers varis (coure, acer inoxidable, alumini, titani, bronze i els seus aliatges).</div>
				</div>
			</div>
			<div class="realfactory-footer-column realfactory-item-pdlr realfactory-column-15">
				<div id="nav_menu-4" class="widget widget_nav_menu realfactory-widget">
					<h3 class="realfactory-widget-title">Serveis</h3>
					<div class="menu-market-sectors-container">
						<ul id="menu-market-sectors" class="menu">
							<?php $this->db->order_by('orden','ASC');  foreach($this->db->get_where('servicios',array('idioma'=>$_SESSION['lang']))->result() as $n=>$v): ?>
								<li class="menu-item"><a href="<?= base_url('servei/'.toUrl($v->id.'-'.$v->titulo)) ?>"><?= $v->titulo ?></a></li>
							<?php endforeach ?>
						</ul>
					</div>
				</div>
			</div>
			<div class="realfactory-footer-column realfactory-item-pdlr realfactory-column-15">
				<div id="recent-posts-3" class="widget widget_recent_entries realfactory-widget">
					<h3 class="realfactory-widget-title">Últims treballs</h3>
					<ul>
						<?php foreach($this->db->get_where('ultimos_trabajos',array('idioma'=>$_SESSION['lang']))->result() as $n=>$v): ?>
							<li><a href="<?= base_url('ultimos-trabajos') ?>.html"><?= $v->titulo ?></a></li>
						<?php endforeach ?>
					</ul>
				</div>
			</div>
			<div class="realfactory-footer-column realfactory-item-pdlr realfactory-column-15">
				<div id="text-7" class="widget widget_text realfactory-widget">
					<h3 class="realfactory-widget-title">Informació de contacte</h3>
					<div class="textwidget">
						<p><i class="fa fa-location-arrow" style="font-size: 20px;color: #64abea;margin-left: 0px;margin-right: 10px;"></i> C/ Pere IV 29-35 baixos <br>08018 Barcelona
							<br> <span class="gdlr-core-space-shortcode" style="margin-top: -6px;"></span>
							<br> <a href="tel:+34933001776"><i class="fa fa-phone" style="font-size: 20px;color: #64abea;margin-right: 10px;"></i> 93 300 17 76</a>
							<br> <span class="gdlr-core-space-shortcode" style="margin-top: -6px;"></span>
							<br> <a href="mailto:taller@moldarc.com"><i class="fa fa-envelope-o" style="font-size: 20px;color: #64abea;margin-left: 0px;margin-right: 10px;"></i> taller@moldarc.com</a>
							<br> <a href="mailto:info@moldarc.com"><i class="fa fa-envelope-o" style="font-size: 20px;color: #64abea;margin-left: 0px;margin-right: 10px; visibility: hidden;"></i> info@moldarc.com</a></p></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="realfactory-copyright-wrapper">
		<div class="realfactory-copyright-container realfactory-container">
			<div class="realfactory-copyright-text realfactory-item-pdlr">
			Copyright 2018 Moldarc, Tots els drets reservats | Per <a href="http://www.jordimagana.com">Jordi Magaña </a> | <a href="[base_url]aviso-legal.html">Avís Legal</a>
			</div>
		</div>
	</div>
</footer>
