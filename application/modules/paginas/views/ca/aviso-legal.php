[menu]
<div class="realfactory-page-wrapper" id="realfactory-page-wrapper">
	<div class="gdlr-core-page-builder-body">

		<div class="realfactory-page-wrapper" id="realfactory-page-wrapper">
                <div class="gdlr-core-page-builder-body">
                    <div class="realfactory-page-title-wrap  realfactory-style-medium realfactory-center-align"  style="background-image:url([base_url]theme/theme/upload/bg-featured-image-2.jpg); ?>);">
                <div class="realfactory-header-transparent-substitute"></div>
                <div class="realfactory-page-title-overlay"></div>
                <div class="realfactory-page-title-container realfactory-container">
                    <div class="realfactory-page-title-content realfactory-item-pdlr">
                        <h1 class="realfactory-page-title">Avís Legal</h1>
                        <div class="realfactory-page-caption">&nbsp;</div>
                    </div>
                </div>
            </div>
                    <div class="gdlr-core-pbf-wrapper ">
                        <div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
                            <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
                                
                                <h5 class="mt-5 text-theme-colored">Qui és el responsable del tractament de les seves dades personals?</h5>
              <p>MOLDARC SL, amb domicili al carrer Pere IV 29-35 baixos, amb N.I.F. nº B-59612846, és la responsable del tractament de les dades personals que vostè ens proporciona i es responsabilitza de les referides dades personals d'acord amb la normativa aplicable en matèria de protecció de dades.</p>
              <h5 class="mt-5 text-theme-colored">On emmagatzemem les seves dades?</h5>
              <p>Les dades que recopilem sobre vostè s'emmagatzemen dins de l'Espai Econòmic Europeu («EEE»). Qualsevol transferència de les seves dades personals serà realitzada de conformitat amb les lleis aplicables.</p>
              <h5 class="mt-5 text-theme-colored">A qui comuniquem les seves dades?</h5>
              <p>Les seves dades poden ser compartides per MOLDARC. Mai passem, venem ni intercanviem les seves dades personals amb terceres parts. Les dades que s'enviïn a tercers, s'utilitzaran únicament per oferir-li a vostè els nostres serveis. Li detallarem les categories de tercers que accedeixen a les seves dades per a cada activitat de tractament específica.</p>
              <h5 class="mt-5 text-theme-colored">Quina és la base legal per al tractament de les seves dades personals?</h5>
              <p>En cada tractament específic de dades personals recopilades sobre vostè, li informarem si la comunicació de dades personals és un requisit legal o contractual, o un requisit necessari per subscriure un contracte, i si està obligat a facilitar les dades personals, així com de les possibles conseqüències de no facilitar tals dades.</p>
              <h5 class="mt-5 text-theme-colored">Quins són els seus drets?</h5>
              <p>Dret d’accés: Qualsevol persona té dret a obtenir confirmació sobre si a MOLDARC estem tractant dades personals que els concerneixin, o no. Pot contactar a MOLDARC que li remetrà les dades personals que tractem sobre vostè per correu electrònic.</p>
              <p>Dret de portabilitat: Sempre que MOLDARC processi les seves dades personals a través de mitjans automatitzats sobre la base del seu consentiment o a un acord, vostè té dret a obtenir una còpia de les seves dades en un format estructurat, d'ús comú i lectura mecànica transferida al seu nom o a un tercer. En ella s'inclouran únicament les dades personals que vostè ens hagi facilitat.</p>
<p>Dret de rectificació: Vostè té dret a sol·licitar la rectificació de les seves dades personals si són inexactes, incloent el dret a completar dades que figurin incomplets.</p>
<p>Dret de supressió: •	Vostè té dret a obtenir sense dilació indeguda la supressió de qualsevol dada personal seva tractada per MOLDARC a qualsevol moment, excepte en les següents situacions:

<br>* té un deute pendent amb MOLDARC, independentment del mètode de pagament
<br>* se sospita o està confirmat que ha utilitzat incorrectament els nostres serveis en els últims quatre anys
<br>* ha contractat algun servei pel que conservarem les seves dades personals en relació amb la transacció per normativa comptable.
</p>
<p>Dret d’oposició al tractament de dades en base a l’interés legítim: Vostè té dret a oposar-se al tractament de les seves dades personals sobre la base de l'interès legítim de MOLDARC. En aquest cas, MOLDARC no seguirà tractant les dades personals tret que puguem acreditar motius legítims imperiosos per al tractament que prevalguin sobre els seus interessos, drets i llibertats, o bé per a la formulació, l'exercici o la defensa de reclamacions.</p>
<p>Dret d’oposició al marketing directe: 
Vostè té dret a oposar-se al màrqueting directe, incloent l'elaboració de perfils realitzada per a aquest màrqueting directe.
Pot desvincular-se del màrqueting directe en qualsevol moment de les següents maneres: *seguint les indicacions facilitades a cada correu de màrqueting</p>
<p>Dret a presentar una reclamació davant d’una autoritat de control: Si vostè considera que MOLDARC tracta les seves dades d'una manera incorrecta, pot contactar amb nosaltres. També té dret a presentar una queixa davant l'autoritat de protecció de dades competent.</p>
<p>Dret a limitació en el tractament: 
Vostè té dret a sol·licitar que MOLDARC limiti el tractament de les seves dades personals en les següents circumstàncies:
<br>* si vostè s'oposa al tractament de les seves dades sobre la base de l'interès legítim de MOLDARC. En aquest cas MOLDARC haurà de limitar qualsevol tractament d'aquestes dades a l'espera de la verificació de l'interès legítim.
<br>* si vostè reclama que les seves dades personals són incorrectes, MOLDARC ha de limitar qualsevol tractament d'aquestes dades fins que es verifiqui la precisió dels mateixos.
<br>* si el tractament és il·legal, vostè podrà oposar-se al fet que s'eliminin les dades personals i, en el seu lloc, sol·licitar la limitació del seu ús.
<br>* si MOLDARC ja no necessita les dades personals, però vostè els necessita per a la formulació, l'exercici o la defensa de reclamacions.</p>
<p>Exercici de drets: 
Ens prenem molt de debò la protecció de dades i, per tant, comptem amb personal de servei al client dedicat que s'ocupa de les seves sol·licituds en relació amb els drets abans esmentats. Sempre pot posar-se en contacte amb ells a info@moldarc.com

</p>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
		
		[footer]
	</div>
</div>