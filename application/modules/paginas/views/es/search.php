[menu]
<div class="realfactory-page-wrapper" id="realfactory-page-wrapper">
	<div class="gdlr-core-page-builder-body">

		
		<div class="realfactory-page-title-wrap  realfactory-style-medium realfactory-center-align"  style="background-image:url(<?= base_url('theme/theme/images/page-title-background2.jpg') ?>);">
            <div class="realfactory-header-transparent-substitute"></div>
            <div class="realfactory-page-title-overlay"></div>
            <div class="realfactory-page-title-container realfactory-container">
                <div class="realfactory-page-title-content realfactory-item-pdlr">
                    <h1 class="realfactory-page-title">Resultados de busqueda</h1>
                    <div class="realfactory-page-caption"></div>
                </div>
            </div>
        </div>
            


        <div class="gdlr-core-pbf-wrapper ">
            <div class="gdlr-core-pbf-background-wrap" style="background-color: #f0f0f0 ;"></div>
            <div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
                <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">                                
                    <div class="gdlr-core-pbf-element">
                        <?= $resultado ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

	[footer]
	</div>
</div>