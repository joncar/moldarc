<footer>
	<div class="realfactory-footer-wrapper">
		<div class="realfactory-footer-container realfactory-container clearfix">
			<div class="realfactory-footer-column realfactory-item-pdlr realfactory-column-15">
				<div id="text-3" class="widget widget_text realfactory-widget">
					<div class="textwidget"><span class="gdlr-core-space-shortcode" style="margin-top: -2px;"></span> <img src="[base_url]theme/theme/upload/logo-white.png" alt=""> <span class="gdlr-core-space-shortcode" style="margin-top: 7px;"></span>Mold-Arc es una empresa con más de 30 años de experiencia en soldaduras de precisión. Somos pioneros en la reparación de moldes y matrices, pequeñas piezas de aceros varios (cobre, acero inoxidable, aluminio, titanio, bronce y sus aleaciones).</div>
				</div>
			</div>
			<div class="realfactory-footer-column realfactory-item-pdlr realfactory-column-15">
				<div id="nav_menu-4" class="widget widget_nav_menu realfactory-widget">
					<h3 class="realfactory-widget-title">Servicios</h3>
					<div class="menu-market-sectors-container">
						<ul id="menu-market-sectors" class="menu">
							<?php $this->db->order_by('orden','ASC');  foreach($this->db->get_where('servicios',array('idioma'=>$_SESSION['lang']))->result() as $n=>$v): ?>
								<li class="menu-item"><a href="<?= base_url('servei/'.toUrl($v->id.'-'.$v->titulo)) ?>"><?= $v->titulo ?></a></li>
							<?php endforeach ?>
						</ul>
					</div>
				</div>
			</div>
			<div class="realfactory-footer-column realfactory-item-pdlr realfactory-column-15">
				<div id="recent-posts-3" class="widget widget_recent_entries realfactory-widget">
					<h3 class="realfactory-widget-title">Últimos trabajos</h3>
					<ul>
						<?php foreach($this->db->get_where('ultimos_trabajos',array('idioma'=>$_SESSION['lang']))->result() as $n=>$v): ?>
							<li><a href="<?= base_url('ultimos-trabajos') ?>.html"><?= $v->titulo ?></a></li>
						<?php endforeach ?>
					</ul>
				</div>
			</div>
			<div class="realfactory-footer-column realfactory-item-pdlr realfactory-column-15">
				<div id="text-7" class="widget widget_text realfactory-widget">
					<h3 class="realfactory-widget-title">Información de contacto</h3>
					<div class="textwidget">
						<p><i class="fa fa-location-arrow" style="font-size: 20px;color: #64abea;margin-left: 0px;margin-right: 10px;"></i> C/ Pere IV 29-35 bajos <br>08018 Barcelona
							<br> <span class="gdlr-core-space-shortcode" style="margin-top: -6px;"></span>
							<br> <a href="tel:+34933001776"><i class="fa fa-phone" style="font-size: 20px;color: #64abea;margin-right: 10px;"></i> 93 300 17 76</a>
							<br> <span class="gdlr-core-space-shortcode" style="margin-top: -6px;"></span>
							<br> <a href="mailto:taller@moldarc.com"><i class="fa fa-envelope-o" style="font-size: 20px;color: #64abea;margin-left: 0px;margin-right: 10px;"></i> taller@moldarc.com</a>
							<br> <a href="mailto:info@moldarc.com"><i class="fa fa-envelope-o" style="font-size: 20px;color: #64abea;margin-left: 0px;margin-right: 10px; visibility: hidden;"></i> info@moldarc.com</a></p></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="realfactory-copyright-wrapper">
		<div class="realfactory-copyright-container realfactory-container">
			<div class="realfactory-copyright-text realfactory-item-pdlr">
			Copyright 2018 Moldarc, Todos los derechos reservados | Por <a href="http://www.jordimagana.com">Jordi Magaña </a> | <a href="[base_url]aviso-legal.html">Aviso Legal</a>
			</div>
		</div>
	</div>
</footer>