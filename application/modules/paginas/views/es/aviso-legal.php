[menu]
<div class="realfactory-page-wrapper" id="realfactory-page-wrapper">
	<div class="gdlr-core-page-builder-body">

		<div class="realfactory-page-wrapper" id="realfactory-page-wrapper">
                <div class="gdlr-core-page-builder-body">
                    <div class="realfactory-page-title-wrap  realfactory-style-medium realfactory-center-align"  style="background-image:url([base_url]theme/theme/upload/bg-featured-image-2.jpg); ?>);">
                <div class="realfactory-header-transparent-substitute"></div>
                <div class="realfactory-page-title-overlay"></div>
                <div class="realfactory-page-title-container realfactory-container">
                    <div class="realfactory-page-title-content realfactory-item-pdlr">
                        <h1 class="realfactory-page-title">Aviso Legal</h1>
                        <div class="realfactory-page-caption">&nbsp;</div>
                    </div>
                </div>
            </div>
                    <div class="gdlr-core-pbf-wrapper ">
                        <div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
                            <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
                                
                                <h5 class="mt-5 text-theme-colored">¿Quién es el responsable del tratamiento de sus datos personales?</h5>
              <p>MOLDARC SL, con domicilio en la calle Pere IV 29-35 bajos, con N.I.F. nº B-59612846, es la responsable del tratamiento de los datos personales que usted nos proporciona y se responsabiliza de los datoss personales de acuerdo con la normativa aplicable en materia de protección de datos.</p>
              <h5 class="mt-5 text-theme-colored">¿Dónde se almacenan sus datos?</h5>
              <p>Los datos que recopilamos sobre usted se almacenan dentro del Espacio Económico Europeo («EEE»). Cualquier transferencia de sus datos personales será realizada de conformidad con las leyes aplicables.</p>
              <h5 class="mt-5 text-theme-colored">¿A quién comunicamos sus datos?</h5>
              <p>Sus datos pueden ser compartidos por MOLDARC. Nunca pasamos, vendemos ni intercambiamos sus datos personales con terceros. Los datos que se mandan a terceros, se utilizarán únicamente para ofrecerle a usted nuestros servicios. Le detallaremos las categorías de terceros que acceden a sus datos para cada actividad de tratamiento específica.</p>
              <h5 class="mt-5 text-theme-colored">¿Cual es la base legal para el tratamiento de sus datos personales?</h5>
              <p>En cada tratamiento específico de datos personales recopilados sobre usted, le informaremos si la comunicación de datos personales es un requisito legal o contractual, o un requisito necesario para subscribir un contrato, y si está obligado a facilitar los datos personales, así como de las posibles consecuencias de no facilitar tales datos.</p>
              <h5 class="mt-5 text-theme-colored">¿Cuales son sus derechos?</h5>
              <p>Derecho de acceso: Cualquier persona tiene derecho a obtener confirmación sobre sí a MOLDARC estamos tratando datos personales que los conciernan, o no. Puede contactar a MOLDARC que le remitirá los datos personales que tratamos sobre usted por correo electrónico.</p>
<p>Derecho de portabilidad: Siempre que MOLDARC procese sus datos personales a través de medios automatizados en base a su consentimiento o a un acuerdo, usted tiene derecho a obtener una copia de sus datos en un formato estructurado, de uso común y lectura mecánica transferida a su nombre o a un tercero. En ella se incluirán únicamente los datos personales que usted nos haya facilitado.</p>
<p>Derecho de rectificación: Usted tiene derecho a solicitar la rectificación de sus datos personales si son inexactos, incluyendo el derecho a completar datos que figuren incompletos.</p>
<p>Derecho de supresión: Usted tiene derecho a obtener sin más dilación indebida la supresión de cualquier dato personal suyo tratada por MOLDARC a cualquier momento, excepto en las siguientes situaciones:

<br>* tiene una deuda pendiente con MOLDARC, independientemente del método de pago
<br>* se sospecha o está confirmado que ha utilizado incorrectamente nuestros servicios en los últimos cuatro años
<br>* ha contratado algún servicio por el que conservaremos sus datos personales en relación con la transacción por normativa contable.
</p>
<p>Derecho de oposición al tratamiento de datos en base al interés legítimo: Usted tiene derecho a oponerse al tratamiento de sus datos personales en base al interés legítimo de MOLDARC. En este caso, MOLDARC no seguirá tratando los datos personales salvo que podamos acreditar motivos legítimos imperiosos para el tratamiento que prevalezcan sobre sus intereses, derechos y libertades, o bien para la formulación, el ejercicio o la defensa de reclamaciones.</p>
<p>Derecho de oposición al marketing directo: 
Usted tiene derecho a oponerse al marketing directo, incluyendo la elaboración de perfiles realizada para este marketing directo.
Puede desvincularse del marketing directo en cualquier momento de las siguientes maneras: siguiendo las indicaciones facilitadas a cada correo de marketing</p>
<p>Derecho a presentar una reclamación ante una autoridad de control: Si usted considera que MOLDARC trata sus datos de una manera incorrecta, puede contactar con nosotros. También tiene derecho a presentar una queja ante la autoridad de protección de datos competente.</p>
<p>Derecho a limitación en el tratamiento: 
Usted tiene derecho a solicitar que MOLDARC limite el tratamiento de sus datos personales en las siguientes circunstancias:
<br>* si usted se opone al tratamiento de sus datos en base al interés legítimo de MOLDARC. En este caso MOLDARC tendrá que limitar cualquier tratamiento de estos datos a la espera de la verificación del interés legítimo.
<br>* si usted reclama que sus datos personales son incorrectos, MOLDARC tiene que limitar cualquier tratamiento de estos datos hasta que se verifique la precisión de los mismos.
<br>* si el tratamiento es ilegal, usted podrá oponerse al hecho que se eliminen los datos personales y, en su lugar, solicitar la limitación de su uso.
<br>* si MOLDARC ya no necesita los datos personales, pero usted los necesita para la formulación, el ejercicio o la defensa de reclamaciones.</p>
<p>Ejercicio de derechos: 
Nos tomamos mucho debò la protección de datos y, por lo tanto, contamos con personal de servicio al cliente dedicado que se ocupa de sus solicitudes en relación con los derechos antes mencionados. Siempre puede ponerse en contacto con ellos a info@moldarc.com

</p>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
		
		[footer]
	</div>
</div>