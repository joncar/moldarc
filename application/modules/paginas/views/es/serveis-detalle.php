[menu]
<div class="realfactory-page-wrapper" id="realfactory-page-wrapper">
    <div class="gdlr-core-page-builder-body">

            <div class="realfactory-page-title-wrap  realfactory-style-custom realfactory-left-align" style="background-image:url(<?= base_url('img/servicios/'.$servicio->banner) ?>);">
                <div class="realfactory-header-transparent-substitute"></div>
                <div class="realfactory-page-title-overlay"></div>
                <div class="realfactory-page-title-container realfactory-container">
                    <div class="realfactory-page-title-content realfactory-item-pdlr">
                        <h1 class="realfactory-page-title"><?= $servicio->titulo ?></h1>
                        <div class="realfactory-page-caption"><?= $servicio->subtitulo ?></div>
                    </div>
                </div>
            </div>
            <div class="realfactory-breadcrumbs">
                <div class="realfactory-breadcrumbs-container realfactory-container">
                    <div class="realfactory-breadcrumbs-item realfactory-item-pdlr"> 
                        <span property="itemListElement" typeof="ListItem">
                            <a property="item" typeof="WebPage" title="Go to Real Factory." href="<?= base_url() ?>" class="home">
                                <span property="name">Inicio</span>
                            </a>
                            <meta property="position" content="1">
                        </span>
                        <i class="fa fa-angle-right"></i> 
                        <span property="itemListElement" typeof="ListItem">
                            <a property="item" typeof="WebPage" title="Go to Market Sectors." href="<?= base_url('serveis') ?>.html" class="post post-page">
                                <span property="name">Servicios</span>
                            </a>
                            <meta property="position" content="2">
                        </span>
                        <i class="fa fa-angle-right"></i> 
                        <span property="itemListElement" typeof="ListItem">
                            <span property="name">
                                <?= $servicio->titulo ?>                                
                            </span>
                            <meta property="position" content="3">
                        </span>
                    </div>
                </div>
            </div>
            <div class="realfactory-page-wrapper" id="realfactory-page-wrapper">
                <div class="gdlr-core-page-builder-body">
                    <div class="gdlr-core-pbf-sidebar-wrapper ">
                        <div class="gdlr-core-pbf-sidebar-container gdlr-core-line-height-0 clearfix gdlr-core-js gdlr-core-container">
                            <div class="gdlr-core-pbf-sidebar-content  gdlr-core-column-45 gdlr-core-pbf-sidebar-padding gdlr-core-line-height gdlr-core-column-extend-right" style="padding: 60px 0px 30px 30px;">
                                <div class="gdlr-core-pbf-sidebar-content-inner">
                                    <div class="gdlr-core-pbf-element">
                                        <div class="gdlr-core-center-align" style="padding-bottom: 40px;">
                                            <div class="gdlr-core-image-item-wrap " style="border-width: 0px;">
                                                <div class="twentytwenty-container">
                                                  <img src="<?= base_url('img/servicios/'.$servicio->foto_antes) ?>" style="width:100%;" />
                                                  <img src="<?= base_url('img/servicios/'.$servicio->foto_despues) ?>" style="width:100%;" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="gdlr-core-pbf-column gdlr-core-column-30 gdlr-core-column-first">
                                        <div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 10px 0px 0px 0px;">
                                            <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
                                                <div class="gdlr-core-pbf-element">
                                                    <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr">
                                                        <div class="gdlr-core-title-item-title-wrap ">
                                                            <h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 27px;font-weight: 700;letter-spacing: 0px;text-transform: none;color: #181818;">
                                                                Características
                                                                <span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider">
                                                                    
                                                                </span>
                                                            </h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--<div class="gdlr-core-pbf-column gdlr-core-column-30">
                                        <div class="gdlr-core-pbf-column-content-margin gdlr-core-js ">
                                            <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
                                                <div class="gdlr-core-pbf-element">
                                                    <div class="gdlr-core-button-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-right-align"><a class="gdlr-core-button  gdlr-core-button-transparent gdlr-core-button-with-border" href="#" id="gdlr-core-button-id-93432"><i class="gdlr-core-pos-left fa fa-file-word-o"></i><span class="gdlr-core-content">Download Doc</span></a><a class="gdlr-core-button  gdlr-core-button-transparent gdlr-core-button-with-border" href="#" id="gdlr-core-button-id-53822"><i class="gdlr-core-pos-left fa fa-file-pdf-o"></i><span class="gdlr-core-content">Download PDF</span></a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>-->
                                    <div class="gdlr-core-pbf-column gdlr-core-column-60 gdlr-core-column-first">
                                        <div class="gdlr-core-pbf-column-content-margin gdlr-core-js ">
                                            <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
                                                <div class="gdlr-core-pbf-element">
                                                    <div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align">
                                                        <div class="gdlr-core-text-box-item-content">
                                                            <p><?= $servicio->caracteristicas ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="gdlr-core-pbf-column gdlr-core-column-60">
                                        <div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 0px 0px 20px 0px;">
                                            <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
                                                
                                                <?php if(!empty($servicio->foto1)): ?>
                                                    <div class="gdlr-core-column-30">
                                                        <div class="gdlr-core-image-item gdlr-core-item-pdlr gdlr-core-item-pdb  gdlr-core-center-align">
                                                            <div class="gdlr-core-image-item-wrap gdlr-core-media-image  gdlr-core-image-item-style-rectangle" style="border-width: 0px;">
                                                                <a class="gdlr-core-ilightbox gdlr-core-js " href="<?= base_url('img/servicios/'.$servicio->foto1) ?>"><img src="<?= base_url('img/servicios/'.$servicio->foto1) ?>" alt="" width="920" height="415"><span class="gdlr-core-image-overlay "><i class="gdlr-core-image-overlay-icon fa fa-search gdlr-core-size-22"></i></span></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php endif ?>
                                                <?php if(!empty($servicio->foto2)): ?>
                                                    <div class="gdlr-core-column-30">
                                                        <div class="gdlr-core-image-item gdlr-core-item-pdlr gdlr-core-item-pdb  gdlr-core-center-align">
                                                            <div class="gdlr-core-image-item-wrap gdlr-core-media-image  gdlr-core-image-item-style-rectangle" style="border-width: 0px;">
                                                                <a class="gdlr-core-ilightbox gdlr-core-js " href="<?= base_url('img/servicios/'.$servicio->foto2) ?>"><img src="<?= base_url('img/servicios/'.$servicio->foto2) ?>" alt="" width="920" height="415"><span class="gdlr-core-image-overlay "><i class="gdlr-core-image-overlay-icon fa fa-search gdlr-core-size-22"></i></span></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php endif ?>
                                                <?php if(!empty($servicio->foto3)): ?>
                                                    <div class="gdlr-core-column-30">
                                                        <div class="gdlr-core-image-item gdlr-core-item-pdlr gdlr-core-item-pdb  gdlr-core-center-align">
                                                            <div class="gdlr-core-image-item-wrap gdlr-core-media-image  gdlr-core-image-item-style-rectangle" style="border-width: 0px;">
                                                                <a class="gdlr-core-ilightbox gdlr-core-js " href="<?= base_url('img/servicios/'.$servicio->foto3) ?>"><img src="<?= base_url('img/servicios/'.$servicio->foto3) ?>" alt="" width="920" height="415"><span class="gdlr-core-image-overlay "><i class="gdlr-core-image-overlay-icon fa fa-search gdlr-core-size-22"></i></span></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php endif ?>
                                                <?php if(!empty($servicio->video)): ?>
                                                    <div class="gdlr-core-column-30">
                                                        <div class="gdlr-core-image-item gdlr-core-item-pdlr gdlr-core-item-pdb  gdlr-core-center-align">
                                                            <div class="gdlr-core-image-item-wrap gdlr-core-media-image  gdlr-core-image-item-style-rectangle" style="border-width: 0px; border-width: 0px;background: url(https://img.youtube.com/vi/<?= $servicio->video ?>/0.jpg);background-repeat: no-repeat;height: 250px;background-position: center;background-size: cover;">
                                                                <a class="gdlr-core-ilightbox gdlr-core-js " href="https://www.youtube.com/embed/<?= $servicio->video ?>?rel=0">
                                                                    <img src="https://img.youtube.com/vi/<?= $servicio->video ?>/0.jpg" alt="" width="920" height="415" style="visibility: hidden;">
                                                                    <span class="gdlr-core-image-overlay "><i class="gdlr-core-image-overlay-icon fa fa-play gdlr-core-size-22"></i></span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php endif ?>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="gdlr-core-pbf-column gdlr-core-column-60 gdlr-core-column-first">
                                        <div class="gdlr-core-pbf-column-content-margin gdlr-core-js ">
                                            <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
                                                <div class="gdlr-core-pbf-element">
                                                    <div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" style="padding-bottom: 20px;">
                                                        <div class="gdlr-core-text-box-item-content">
                                                            <p><?= $servicio->caracteristicas2 ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- 
<div class="gdlr-core-pbf-element">
                                                    <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr">
                                                        <div class="gdlr-core-title-item-title-wrap ">
                                                            <h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 24px;font-weight: 700;letter-spacing: 0px;text-transform: none;">Sem Aenean Pharetra<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3></div>
                                                    </div>
                                                </div>
                                                <div class="gdlr-core-pbf-element">
                                                    <div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align">
                                                        <div class="gdlr-core-text-box-item-content">
                                                            <p>Sed posuere consectetur est at lobortis. Aenean lacinia bibendum nulla sed consectetur. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Vestibulum id ligula porta felis euismod semper. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Vestibulum id ligula porta felis euismod semper. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Maecenas faucibus mollis interdum. Donec sed odio dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                                                        </div>
                                                    </div>
                                                </div>
 -->
                                                <!-- 
<div class="gdlr-core-pbf-element">
                                                    <div class="gdlr-core-social-share-item gdlr-core-item-pdb  gdlr-core-left-align gdlr-core-social-share-left-text gdlr-core-item-pdlr">
                                                        <span class="gdlr-core-social-share-wrap">
                                                            <a class="gdlr-core-social-share-facebook" href="#" target="_blank" style="font-size: 18px;"><i class="fa fa-facebook"></i></a>
                                                            <a class="gdlr-core-social-share-linkedin" href="#" target="_blank" style="font-size: 18px;"><i class="fa fa-linkedin"></i></a>
                                                            <a class="gdlr-core-social-share-google-plus" href="#" target="_blank" style="font-size: 18px;"><i class="fa fa-google-plus"></i></a>
                                                            <a class="gdlr-core-social-share-pinterest" href="#" target="_blank" style="font-size: 18px;"><i class="fa fa-pinterest-p"></i></a>
                                                            <a class="gdlr-core-social-share-stumbleupon" href="#" target="_blank" style="font-size: 18px;"><i class="fa fa-stumbleupon"></i></a>
                                                            <a class="gdlr-core-social-share-twitter" href="#" target="_blank" style="font-size: 18px;"><i class="fa fa-twitter"></i></a>
                                                            <a class="gdlr-core-social-share-email" href="#" style="font-size: 18px;"><i class="fa fa-envelope"></i></a>
                                                        </span>
                                                    </div>
                                                </div>
 -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="gdlr-core-pbf-sidebar-left gdlr-core-column-extend-left  realfactory-sidebar-area gdlr-core-column-15 gdlr-core-pbf-sidebar-padding  gdlr-core-line-height" style="padding: 60px 30px 30px 0px;">
                                <div class="gdlr-core-pbf-background-wrap" style="background-color: #f9f9f9 ;"></div>
                                <div class="gdlr-core-sidebar-item">
                                    <div id="nav_menu-2" class="widget widget_nav_menu realfactory-widget">
                                        <h3 class="realfactory-widget-title">Servicios</h3>
                                        <div class="menu-market-sectors-container">
                                            <ul id="menu-market-sectors" class="menu">
                                                <?php foreach($this->db->get_where('servicios',array('idioma'=>$_SESSION['lang']))->result() as $n=>$v): ?>
                                                    <li class="menu-item"><a href="<?= base_url('servei/'.toUrl($v->id.'-'.$v->titulo)) ?>" <?= $v->id==$servicio->id?'style="font-weight:bold; color: #64abea;"':'' ?>><?= $v->titulo ?></a></li>
                                                <?php endforeach ?>                                                
                                            </ul>
                                        </div>
                                    </div>
                                    <div id="nav_menu-3" class="widget widget_nav_menu realfactory-widget">
                                        <h3 class="realfactory-widget-title">Links de interés</h3>
                                        <div class="menu-useful-links-container">
                                            <ul id="menu-useful-links" class="menu">
                                                <li class="menu-item"><a href="http://www.gratec.es/">GRATEC</a></li>
                                                <li class="menu-item"><a href="[base_url]ultimos-trabajos.html"></a></li>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                    <div id="text-6" class="widget widget_text realfactory-widget">
                                        <div class="textwidget"><a class="gdlr-core-button  gdlr-core-button-solid gdlr-core-button-no-border" href="[base_url]contacte.html" target="_parent" style="margin-right: 20px;border-radius: 3px;-moz-border-radius: 3px;-webkit-border-radius: 3px;background: #222 ;"><span class="gdlr-core-content">Contacto</span></a></div>
                                    </div>
                                    <div class="gdlr-core-accordion-item gdlr-core-item-pdlr gdlr-core-item-pdb  gdlr-core-accordion-style-box-icon">
                                        <?php foreach($servicio->detalles->result() as $n=>$s): ?>
                                            <div class="gdlr-core-accordion-item-tab clearfix  <?= $n==0?'gdlr-core-active':'' ?>">
                                                <div class="gdlr-core-accordion-item-icon gdlr-core-js gdlr-core-skin-icon  gdlr-core-skin-e-background gdlr-core-skin-border"></div>
                                                <div class="gdlr-core-accordion-item-content-wrapper">
                                                    <h4 class="gdlr-core-accordion-item-title gdlr-core-js "><?= $s->titulo ?></h4>
                                                    <div class="gdlr-core-accordion-item-content">
                                                        <p><?= $s->descripcion ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
        [footer]
    </div>
</div>