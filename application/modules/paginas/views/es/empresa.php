[menu]
<div class="realfactory-page-wrapper" id="realfactory-page-wrapper">
    <div class="gdlr-core-page-builder-body">

        
        <div class="realfactory-page-title-wrap  realfactory-style-medium realfactory-center-align"  style="background-image:url(<?= base_url('theme/theme/images/page-title-background2.jpg') ?>);">
                <div class="realfactory-header-transparent-substitute"></div>
                <div class="realfactory-page-title-overlay"></div>
                <div class="realfactory-page-title-container realfactory-container">
                    <div class="realfactory-page-title-content realfactory-item-pdlr">
                        <h1 class="realfactory-page-title">Empresa</h1>
                        <div class="realfactory-page-caption">Quién somos y qué hacemos</div>
                    </div>
                </div>
            </div>
            <div class="realfactory-page-wrapper" id="realfactory-page-wrapper">
                <div class="gdlr-core-page-builder-body">
                    <div class="gdlr-core-pbf-wrapper " style="padding: 0px 0px 0px 0px;">
                        <div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
                            <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
                                <div class="gdlr-core-pbf-column gdlr-core-column-30 gdlr-core-column-first">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js  gdlr-core-column-extend-left" style="padding: 100px 100px 0px 0px;" data-sync-height="height-1">
                                        <div class="gdlr-core-pbf-background-wrap" style="background-color: #f1f0f0 ;">
                                            <div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/about-bg-1.jpg);background-size: cover;background-position: center;" data-parallax-speed="0.2"></div>
                                        </div>
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content" data-gdlr-animation="fadeInLeft" data-gdlr-animation-duration="600ms" data-gdlr-animation-offset="0.8"></div>
                                    </div>
                                </div>
                                <div class="gdlr-core-pbf-column gdlr-core-column-30">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 130px 0px 110px 70px;" data-sync-height="height-1" data-sync-height-center="">
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content" data-gdlr-animation="fadeInRight" data-gdlr-animation-duration="600ms" data-gdlr-animation-offset="0.8">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-bottom: 25px;">
                                                    <div class="gdlr-core-title-item-title-wrap ">
                                                        <h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 33px;letter-spacing: 0px;text-transform: none;">Quién somos<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3></div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" style="padding-bottom: 15px;">
                                                    <div class="gdlr-core-text-box-item-content" style="font-size: 17px;">
                                                        <p>Somos una empresa con más de 30 años de experiencia en soldaduras de precisión. <br>Somos pioneros en la reparación de moldes y matrices, pequeñas piezas de aceros varios (cobre, acero inoxidable, aluminio, titanio, bronce y sus aliajes). <br>Nuestra profesionalidad y experiencia nos avalan teniendo como resultado soldaduras de alta calidad y precisión.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-icon-list-item gdlr-core-item-pdlr gdlr-core-item-pdb ">
                                                    <ul>
                                                        <li class=" gdlr-core-skin-divider gdlr-core-with-hover gdlr-core-column-30 gdlr-core-column-first"><span class="gdlr-core-icon-list-icon-wrap"><i class="gdlr-core-icon-list-icon-hover fa fa-check-circle" style="font-size: 16px;width: 16px;"></i><i class="gdlr-core-icon-list-icon fa fa-check-circle" style="font-size: 16px;width: 16px;"></i></span><span class="gdlr-core-icon-list-content" style="font-size: 16px;">Atención personalizada</span></li>
                                                        <li class=" gdlr-core-skin-divider gdlr-core-with-hover gdlr-core-column-30"><span class="gdlr-core-icon-list-icon-wrap"><i class="gdlr-core-icon-list-icon-hover fa fa-check-circle" style="font-size: 16px;width: 16px;"></i><i class="gdlr-core-icon-list-icon fa fa-check-circle" style="font-size: 16px;width: 16px;"></i></span><span class="gdlr-core-icon-list-content" style="font-size: 16px;">Tratamientos térmicos</span></li>
                                                        <li class=" gdlr-core-skin-divider gdlr-core-with-hover gdlr-core-column-30 gdlr-core-column-first"><span class="gdlr-core-icon-list-icon-wrap"><i class="gdlr-core-icon-list-icon-hover fa fa-check-circle" style="font-size: 16px;width: 16px;"></i><i class="gdlr-core-icon-list-icon fa fa-check-circle" style="font-size: 16px;width: 16px;"></i></span><span class="gdlr-core-icon-list-content" style="font-size: 16px;">Detección de fisuras</span></li>
                                                        <li class=" gdlr-core-skin-divider gdlr-core-with-hover gdlr-core-column-30"><span class="gdlr-core-icon-list-icon-wrap"><i class="gdlr-core-icon-list-icon-hover fa fa-check-circle" style="font-size: 16px;width: 16px;"></i><i class="gdlr-core-icon-list-icon fa fa-check-circle" style="font-size: 16px;width: 16px;"></i></span><span class="gdlr-core-icon-list-content" style="font-size: 16px;">Sistemas de embalaje</span></li>
                                                        <li class=" gdlr-core-skin-divider gdlr-core-with-hover gdlr-core-column-30 gdlr-core-column-first"><span class="gdlr-core-icon-list-icon-wrap"><i class="gdlr-core-icon-list-icon-hover fa fa-check-circle" style="font-size: 16px;width: 16px;"></i><i class="gdlr-core-icon-list-icon fa fa-check-circle" style="font-size: 16px;width: 16px;"></i></span><span class="gdlr-core-icon-list-content" style="font-size: 16px;">Reparación de herramientas</span></li>
                                                        <li class=" gdlr-core-skin-divider gdlr-core-with-hover gdlr-core-column-30"><span class="gdlr-core-icon-list-icon-wrap"><i class="gdlr-core-icon-list-icon-hover fa fa-check-circle" style="font-size: 16px;width: 16px;"></i><i class="gdlr-core-icon-list-icon fa fa-check-circle" style="font-size: 16px;width: 16px;"></i></span><span class="gdlr-core-icon-list-content" style="font-size: 16px;">Servicio a domicilio</span></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                


                        <div class="gdlr-core-pbf-column gdlr-core-column-30 gdlr-core-column-first">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 109px 80px 80px 0px;" data-sync-height="height-3">
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-bottom: 35px;">
                                                    <div class="gdlr-core-title-item-title-wrap ">
                                                        <h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 36px;letter-spacing: 0px;text-transform: none;">Los números nos avalan<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3></div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" style="padding-bottom: 0px;">
                                                    <div class="gdlr-core-text-box-item-content" style="font-size: 15px;">
                                                        <p>Nuestra larga trayectoria y experiencia nos han hecho situar como referente dentro del mundo de las soldaduras. <br>La satisfacción de nuestros clientes es primordial para Moldarc.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="gdlr-core-pbf-column gdlr-core-column-30">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 105px 0px 30px 0px;" data-sync-height="height-3">
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content" data-gdlr-animation="fadeInRight" data-gdlr-animation-duration="600ms" data-gdlr-animation-offset="0.8">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-skill-bar-item gdlr-core-item-pdlr gdlr-core-item-pdb  gdlr-core-size-small gdlr-core-type-round">
                                                    <div class="gdlr-core-skill-bar">
                                                        <div class="gdlr-core-skill-bar-head gdlr-core-title-font"><span class="gdlr-core-skill-bar-title">Soldaduras láser</span><span class="gdlr-core-skill-bar-right">70%</span></div>
                                                        <div class="gdlr-core-skill-bar-progress">
                                                            <div class="gdlr-core-skill-bar-filled gdlr-core-js" data-width="90"></div>
                                                        </div>
                                                    </div>
                                                    <div class="gdlr-core-skill-bar">
                                                        <div class="gdlr-core-skill-bar-head gdlr-core-title-font"><span class="gdlr-core-skill-bar-title">Soldaduras TIG pulsante</span><span class="gdlr-core-skill-bar-right">10%</span></div>
                                                        <div class="gdlr-core-skill-bar-progress">
                                                            <div class="gdlr-core-skill-bar-filled gdlr-core-js" data-width="100"></div>
                                                        </div>
                                                    </div>
                                                    <div class="gdlr-core-skill-bar">
                                                        <div class="gdlr-core-skill-bar-head gdlr-core-title-font"><span class="gdlr-core-skill-bar-title">Soldaduras microplasma</span><span class="gdlr-core-skill-bar-right">15%</span></div>
                                                        <div class="gdlr-core-skill-bar-progress">
                                                            <div class="gdlr-core-skill-bar-filled gdlr-core-js" data-width="85"></div>
                                                        </div>
                                                    </div>
                                                    <div class="gdlr-core-skill-bar">
                                                        <div class="gdlr-core-skill-bar-head gdlr-core-title-font"><span class="gdlr-core-skill-bar-title">A domicilio</span><span class="gdlr-core-skill-bar-right">5%</span></div>
                                                        <div class="gdlr-core-skill-bar-progress">
                                                            <div class="gdlr-core-skill-bar-filled gdlr-core-js" data-width="90"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>





                    <div class="gdlr-core-pbf-wrapper " style="padding: 75px 0px 15px 0px;">
                        <div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
                            <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
                                <div class="gdlr-core-pbf-element">
                                    <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-bottom: 55px;">
                                        <div class="gdlr-core-title-item-title-wrap  gdlr-core-js-2 gdlr-core-with-divider">
                                            <h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 36px;font-weight: 300;letter-spacing: 0px;text-transform: none;margin-right: 30px;">Sectores donde trabajamos<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3>
                                            <div class="gdlr-core-title-item-divider gdlr-core-right gdlr-core-skin-divider"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="gdlr-core-pbf-wrapper " style="padding: 0px 0px 0px 0px;">
                        <div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
                            <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
                                <div class="gdlr-core-pbf-column gdlr-core-column-30 gdlr-core-column-first">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js  gdlr-core-column-extend-left" data-sync-height="height-service">
                                        <div class="gdlr-core-pbf-background-wrap">
                                            <div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/alimentacio.jpg);background-size: cover;background-position: center;" data-parallax-speed="0.4"></div>
                                        </div>
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content"></div>
                                    </div>
                                </div>
                                <div class="gdlr-core-pbf-column gdlr-core-column-30" data-skin="Call to action">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js  gdlr-core-column-extend-right" style="padding: 157px 20px 60px 73px;" data-sync-height="height-service">
                                        <div class="gdlr-core-pbf-background-wrap">
                                            <div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/service-bg-1.jpg);background-size: cover;background-position: center;" data-parallax-speed="0.2"></div>
                                        </div>
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr">
                                                    <div class="gdlr-core-title-item-title-wrap ">
                                                        <h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 26px;font-weight: 300;letter-spacing: 0px;text-transform: none;">Alimentación<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3></div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" style="padding-bottom: 0px;">
                                                    <div class="gdlr-core-text-box-item-content" style="font-size: 17px;">
                                                        <p>Tenemos varios clientes dentro del sector de la alimentación. Corregimos posibles cambios y/o errores en los moldes y permitimos dar una nueva vida a los moldes originales. <br>Las soldaduras cumplen con <b>la normativa ASME BPE 2015 PARTE MJ.</b></p>
                                                        <!-- <p><a href="#"><strong>Llegir més</strong></a></p> -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="gdlr-core-pbf-column gdlr-core-column-30 gdlr-core-column-first"  data-skin="Call to action">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js  gdlr-core-column-extend-left" style="padding: 157px 40px 60px 0px;" data-sync-height="height-service">
                                        <div class="gdlr-core-pbf-background-wrap">
                                            <div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/auto.jpg);background-size: cover;background-position: center;" data-parallax-speed="0.2"></div>
                                        </div>
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr">
                                                    <div class="gdlr-core-title-item-title-wrap ">
                                                        <h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 26px;font-weight: 300;letter-spacing: 0px;text-transform: none;">Automoción<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3></div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" style="padding-bottom: 0px;">
                                                    <div class="gdlr-core-text-box-item-content" style="font-size: 17px;">
                                                        <p>El sector automobilístico requiere mucha precisión y resistencia en sus piezas. Cualquier error puede causar accidentes y problemáticas millonarias.</p>
                                                        <!-- <p><a href="#"><strong>Llegir més</strong></a></p> -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="gdlr-core-pbf-column gdlr-core-column-30">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js  gdlr-core-column-extend-right" data-sync-height="height-service">
                                        <div class="gdlr-core-pbf-background-wrap">
                                            <div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/service-4-1.jpg);background-size: cover;background-position: center;" data-parallax-speed="0.4"></div>
                                        </div>
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content"></div>
                                    </div>
                                </div>
                                <div class="gdlr-core-pbf-column gdlr-core-column-30 gdlr-core-column-first">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js  gdlr-core-column-extend-left" data-sync-height="height-service">
                                        <div class="gdlr-core-pbf-background-wrap">
                                            <div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/joi2.jpg);background-size: cover;background-position: center;" data-parallax-speed="0.4"></div>
                                        </div>
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content"></div>
                                    </div>
                                </div>
                                <div class="gdlr-core-pbf-column gdlr-core-column-30" data-skin="Call to action">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js  gdlr-core-column-extend-right" style="padding: 157px 20px 60px 73px;" data-sync-height="height-service">
                                        <div class="gdlr-core-pbf-background-wrap">
                                            <div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/eljo.png);background-size: cover;background-position: center;" data-parallax-speed="0.2"></div>
                                        </div>
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr">
                                                    <div class="gdlr-core-title-item-title-wrap ">
                                                        <h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 26px;font-weight: 300;letter-spacing: 0px;text-transform: none;">Joyeria<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3></div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" style="padding-bottom: 0px;">
                                                    <div class="gdlr-core-text-box-item-content" style="font-size: 17px;">
                                                        <p>La joyera es un arte y necesita un trabajo fino, muy preciso y de alta resistencia. Por eso muchos clientes apuestan por Moldarc para soldar sus creaciones.</p>
                                                        <!-- <p><a href="#"><strong>Llegir més</strong></a></p> -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="gdlr-core-pbf-column gdlr-core-column-30 gdlr-core-column-first"  data-skin="Call to action">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js  gdlr-core-column-extend-left" style="padding: 157px 40px 60px 0px;" data-sync-height="height-service">
                                        <div class="gdlr-core-pbf-background-wrap">
                                            <div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/elfar.png);background-size: cover;background-position: center;" data-parallax-speed="0.2"></div>
                                        </div>
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr">
                                                    <div class="gdlr-core-title-item-title-wrap ">
                                                        <h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 26px;font-weight: 300;letter-spacing: 0px;text-transform: none;">Química y farmacéutica<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3></div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" style="padding-bottom: 0px;">
                                                    <div class="gdlr-core-text-box-item-content" style="font-size: 17px;">
                                                        <p>Nuestra maquinaria profesional permite trabajar en proyectos de pequeño tamaño y máxima precisión. Se trabaja con soldadura orbital, o en su defecto soldadura anular bajo atmósfera de gas per evitar oxidaciones. También con soldadura penetrante y lisa con un Ra<0.8 exterior y Ra<0.6 interior.</p>
                                                        <!-- <p><a href="#"><strong>Llegir més</strong></a></p> -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="gdlr-core-pbf-column gdlr-core-column-30">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js  gdlr-core-column-extend-right" data-sync-height="height-service">
                                        <div class="gdlr-core-pbf-background-wrap">
                                            <div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/quim.png);background-size: cover;background-position: center;" data-parallax-speed="0.4"></div>
                                        </div>
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                                
                            </div>
                        </div>
                    </div>
                    <div class="gdlr-core-pbf-wrapper " style="padding: 130px 0px 100px 0px;" data-skin="Dark">
                        <div class="gdlr-core-pbf-background-wrap" style="background-color: #232323 ;">
                            <div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url([base_url]theme/theme/upload/about-bg.jpg);background-size: cover;background-position: center;" data-parallax-speed="0.2"></div>
                        </div>
                        <div class="gdlr-core-pbf-wrapper-content gdlr-core-js " data-gdlr-animation="fadeInUp" data-gdlr-animation-duration="600ms" data-gdlr-animation-offset="0.8">
                            <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
                                <div class="gdlr-core-pbf-column gdlr-core-column-60 gdlr-core-column-first">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 0px 0px 30px 0px;">
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-center-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-bottom: 25px;">
                                                    <div class="gdlr-core-title-item-title-wrap ">
                                                        <h4 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 54px;letter-spacing: 0px;text-transform: none;">Nuestras <span style="color: #64abea;">estadísticas</span><span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h4></div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-center-align">
                                                    <div class="gdlr-core-text-box-item-content" style="font-size: 16px;">
                                                        <p>Con más de 30 años de experiencia son muchas las cifras que hemos generado.
                                                            <br> Aquí te mostramos algunas...</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="gdlr-core-pbf-column gdlr-core-column-15 gdlr-core-column-first" data-skin="About Counter">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 10px 0px 0px 0px;">
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-counter-item gdlr-core-item-pdlr gdlr-core-item-pdb " style="padding-bottom: 10px;">
                                                    <div class="gdlr-core-counter-item-number gdlr-core-skin-title" style="font-size: 49px;"><span class="gdlr-core-counter-item-count gdlr-core-js" data-duration="4000" data-counter-start="0" data-counter-end="800">0</span><span class="gdlr-core-counter-item-suffix">+</span></div>
                                                    <div class="gdlr-core-counter-item-bottom-text gdlr-core-skin-content" style="font-size: 18px;text-transform: none;">Clientes</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="gdlr-core-pbf-column gdlr-core-column-15" data-skin="About Counter">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 10px 0px 0px 0px;">
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-counter-item gdlr-core-item-pdlr gdlr-core-item-pdb " style="padding-bottom: 10px;">
                                                    <div class="gdlr-core-counter-item-number gdlr-core-skin-title" style="font-size: 49px;"><span class="gdlr-core-counter-item-count gdlr-core-js" data-duration="4000" data-counter-start="0" data-counter-end="135">0</span><span class="gdlr-core-counter-item-suffix">.000</span></div>
                                                    <div class="gdlr-core-counter-item-bottom-text gdlr-core-skin-content" style="font-size: 18px;text-transform: none;">Proyectos</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="gdlr-core-pbf-column gdlr-core-column-15" data-skin="About Counter">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 10px 0px 0px 0px;">
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-counter-item gdlr-core-item-pdlr gdlr-core-item-pdb " style="padding-bottom: 10px;">
                                                    <div class="gdlr-core-counter-item-number gdlr-core-skin-title" style="font-size: 49px;"><span class="gdlr-core-counter-item-count gdlr-core-js" data-duration="4000" data-counter-start="0" data-counter-end="100">0</span><span class="gdlr-core-counter-item-suffix">.000</span></div>
                                                    <div class="gdlr-core-counter-item-bottom-text gdlr-core-skin-content" style="font-size: 18px;text-transform: none;">Horas de soldadura</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="gdlr-core-pbf-column gdlr-core-column-15" data-skin="About Counter">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 10px 0px 0px 0px;">
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-counter-item gdlr-core-item-pdlr gdlr-core-item-pdb " style="padding-bottom: 10px;">
                                                    <div class="gdlr-core-counter-item-number gdlr-core-skin-title" style="font-size: 49px;"><span class="gdlr-core-counter-item-count gdlr-core-js" data-duration="4000" data-counter-start="0" data-counter-end="1">0</span><span class="gdlr-core-counter-item-suffix">.000.000</span></div>
                                                    <div class="gdlr-core-counter-item-bottom-text gdlr-core-skin-content" style="font-size: 18px;text-transform: none;">Km recorridos</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="gdlr-core-pbf-wrapper " style="padding: 110px 0px 0px 0px;">
                        <div class="gdlr-core-pbf-background-wrap" style="background-color: #f0f0f0 ;"></div>
                        <div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
                            <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
                                <div class="gdlr-core-pbf-element">
                                    <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-center-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-bottom: 60px;">
                                        <div class="gdlr-core-title-item-title-wrap ">
                                            <h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 45px;font-weight: 700;letter-spacing: 0px;text-transform: none;">Responsables de los departamentos<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3></div>
                                    </div>
                                </div>
                                <div class="gdlr-core-pbf-element">
                                    <div class="gdlr-core-personnel-item gdlr-core-item-pdb clearfix  gdlr-core-center-align gdlr-core-personnel-item-style-grid-with-background gdlr-core-personnel-style-grid gdlr-core-with-background">
                                        <div class="gdlr-core-personnel-list-column  gdlr-core-column-30 gdlr-core-column-first gdlr-core-item-pdlr">
                                            <div class="gdlr-core-personnel-list clearfix">
                                                <div class="gdlr-core-personnel-list-image gdlr-core-media-image  gdlr-core-opacity-on-hover gdlr-core-zoom-on-hover">
                                                    <a href="../personnel/david-parker/index.htm"><img src="[base_url]theme/theme/upload/personnel-2.jpg" alt="" width="600" height="400"></a>
                                                </div>
                                                <div class="gdlr-core-personnel-list-content-wrap">
                                                    <h3 class="gdlr-core-personnel-list-title"><a href="../personnel/david-parker/index.htm">Carles Melús</a></h3>
                                                    <div class="gdlr-core-personnel-list-position gdlr-core-info-font gdlr-core-skin-caption">Jefe de taller</div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- 
<div class="gdlr-core-personnel-list-column  gdlr-core-column-20 gdlr-core-item-pdlr">
                                            <div class="gdlr-core-personnel-list clearfix">
                                                <div class="gdlr-core-personnel-list-image gdlr-core-media-image  gdlr-core-opacity-on-hover gdlr-core-zoom-on-hover">
                                                    <a href="../personnel/alan-cooper/index.htm"><img src="[base_url]theme/theme/upload/personnel-4.jpg" alt="" width="600" height="400"></a>
                                                </div>
                                                <div class="gdlr-core-personnel-list-content-wrap">
                                                    <h3 class="gdlr-core-personnel-list-title"><a href="../personnel/alan-cooper/index.htm">Joan Riba</a></h3>
                                                    <div class="gdlr-core-personnel-list-position gdlr-core-info-font gdlr-core-skin-caption">Soldador làser</div>
                                                </div>
                                            </div>
                                        </div>
 -->
                                        <div class="gdlr-core-personnel-list-column  gdlr-core-column-30 gdlr-core-item-pdlr">
                                            <div class="gdlr-core-personnel-list clearfix">
                                                <div class="gdlr-core-personnel-list-image gdlr-core-media-image  gdlr-core-opacity-on-hover gdlr-core-zoom-on-hover">
                                                    <a href="../personnel/janet-cole/index.htm"><img src="[base_url]theme/theme/upload/personnel-5.jpg" alt="" width="600" height="400"></a>
                                                </div>
                                                <div class="gdlr-core-personnel-list-content-wrap">
                                                    <h3 class="gdlr-core-personnel-list-title"><a href="../personnel/janet-cole/index.htm">Glòria Ustrell</a></h3>
                                                    <div class="gdlr-core-personnel-list-position gdlr-core-info-font gdlr-core-skin-caption">Administración</div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- 
<div class="gdlr-core-personnel-list-column  gdlr-core-column-20 gdlr-core-column-first gdlr-core-item-pdlr">
                                            <div class="gdlr-core-personnel-list clearfix">
                                                <div class="gdlr-core-personnel-list-image gdlr-core-media-image  gdlr-core-opacity-on-hover gdlr-core-zoom-on-hover">
                                                    <a href="../personnel/peter-sandler/index.htm"><img src="[base_url]theme/theme/upload/personnel-3.jpg" alt="" width="600" height="400"></a>
                                                </div>
                                                <div class="gdlr-core-personnel-list-content-wrap">
                                                    <h3 class="gdlr-core-personnel-list-title"><a href="../personnel/peter-sandler/index.htm">Pere Soler</a></h3>
                                                    <div class="gdlr-core-personnel-list-position gdlr-core-info-font gdlr-core-skin-caption">Soldador microplasma</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="gdlr-core-personnel-list-column  gdlr-core-column-20 gdlr-core-item-pdlr">
                                            <div class="gdlr-core-personnel-list clearfix">
                                                <div class="gdlr-core-personnel-list-image gdlr-core-media-image  gdlr-core-opacity-on-hover gdlr-core-zoom-on-hover">
                                                    <a href="../personnel/ricardo-gomez/index.htm"><img src="[base_url]theme/theme/upload/personnel-1.jpg" alt="" width="600" height="400"></a>
                                                </div>
                                                <div class="gdlr-core-personnel-list-content-wrap">
                                                    <h3 class="gdlr-core-personnel-list-title"><a href="../personnel/ricardo-gomez/index.htm">Ricardo Gomez</a></h3>
                                                    <div class="gdlr-core-personnel-list-position gdlr-core-info-font gdlr-core-skin-caption">Contable</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="gdlr-core-personnel-list-column  gdlr-core-column-20 gdlr-core-item-pdlr">
                                            <div class="gdlr-core-personnel-list clearfix">
                                                <div class="gdlr-core-personnel-list-image gdlr-core-media-image  gdlr-core-opacity-on-hover gdlr-core-zoom-on-hover">
                                                    <a href="../personnel/james-smith/index.htm"><img src="[base_url]theme/theme/upload/Untitled-6.jpg" alt="" width="600" height="400"></a>
                                                </div>
                                                <div class="gdlr-core-personnel-list-content-wrap">
                                                    <h3 class="gdlr-core-personnel-list-title"><a href="../personnel/james-smith/index.htm">Jaume Sans</a></h3>
                                                    <div class="gdlr-core-personnel-list-position gdlr-core-info-font gdlr-core-skin-caption">Informàtic</div>
                                                </div>
                                            </div>
                                        </div>
 -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    [footer]
    </div>
</div>