<div class="realfactory-mobile-header-wrap">
	<div class="realfactory-mobile-header realfactory-header-background realfactory-style-slide" id="realfactory-mobile-header">
		<div class="realfactory-mobile-header-container realfactory-container">
			<div class="realfactory-logo  realfactory-item-pdlr">
				<div class="realfactory-logo-inner">
					<a href="<?= base_url() ?>"><img src="[base_url]theme/theme/images/logo.png" alt=""></a>
				</div>
			</div>
			<div class="realfactory-mobile-menu-right">
				<div class="realfactory-main-menu-search" id="realfactory-mobile-top-search"><i class="fa fa-search"></i></div>
				<div class="realfactory-top-search-wrap">
					<div class="realfactory-top-search-close"></div>
					<div class="realfactory-top-search-row">
						<div class="realfactory-top-search-cell">
							<form role="search" method="get" class="search-form" action="<?= base_url('paginas/frontend/search') ?>">
								<input type="text" class="search-field realfactory-title-font" placeholder="Buscar..." value="" name="q">
								<div class="realfactory-top-search-submit"><i class="fa fa-search"></i></div>
								<input type="submit" class="search-submit" value="Buscar">
								<div class="realfactory-top-search-close"><i class="icon_close"></i></div>
							</form>
						</div>
					</div>
				</div>
				<div class="realfactory-mobile-menu">
					<a class="realfactory-mm-menu-button realfactory-mobile-menu-button realfactory-mobile-button-hamburger-with-border" href="#realfactory-mobile-menu">
						<i class="fa fa-bars"></i>
					</a>
					<div class="realfactory-mm-menu-wrap realfactory-navigation-font" id="realfactory-mobile-menu" data-slide="right">
						<ul id="menu-main-navigation" class="m-menu">
							<li class="menu-item"><a href="<?= base_url() ?>">Inicio</a></li>
							<li class="menu-item"><a href="<?= base_url() ?>empresa.html">Empresa</a></li>
							<li class="menu-item menu-item-has-children">
								<a href="<?= base_url() ?>servicios.html">Servicios</a>
								<ul class="m-menu">
									<?php foreach($this->db->get_where('servicios',array('idioma'=>$_SESSION['lang']))->result() as $n=>$v): ?>
										<li class="menu-item"><a href="<?= base_url('servicio/'.toUrl($v->id.'-'.$v->titulo)) ?>"><?= $v->titulo ?></a></li>
									<?php endforeach ?>
								</ul>
							</li>
							<li class="menu-item"><a href="<?= base_url() ?>ultimos-trabajos.html">Últimos trabajos</a></li>
							<li class="menu-item"><a href="<?= base_url() ?>contacto.html">Contacto</a></li>
							<li class="menu-item">
								<a href="<?= base_url('main/traduccion/ca') ?>" class="sf-with-ul-pre">CAT</a>
							</li>

						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<div class="realfactory-body-outer-wrapper ">
	<div class="realfactory-top-bar">
		<div class="realfactory-top-bar-background"></div>
		<div class="realfactory-top-bar-container clearfix realfactory-container ">
			<div class="realfactory-top-bar-left realfactory-item-pdlr">
				<div class="gdlr-core-dropdown-tab gdlr-core-js clearfix">
					<div class="gdlr-core-dropdown-tab-title">
						<span class="gdlr-core-head">Castellano</span>
						<div class="gdlr-core-dropdown-tab-head-wrap">
							<a href="<?= base_url('main/traduccion/ca') ?>"><div class="gdlr-core-dropdown-tab-head" data-index="0">Català</div></a>
							<a href="<?= base_url('main/traduccion/es') ?>"><div class="gdlr-core-dropdown-tab-head" data-index="1">Castellà</div></a>
							<a href="<?= base_url('main/traduccion/en') ?>"><div class="gdlr-core-dropdown-tab-head" data-index="2">Anglès</div></a>
						</div>
					</div>
					<div class="gdlr-core-dropdown-tab-content-wrap">
						<div class="gdlr-core-dropdown-tab-content gdlr-core-active" data-index="0"><i class="fa fa-phone" style="font-size: 16px;color: #64abea;margin-right: 10px;"></i> 93 300 17 76 <i class="fa fa-clock-o" style="font-size: 16px;color: #64abea;margin-left: 24px;margin-right: 10px;"></i> Lun - Vie 08:00 - 13:30h y 15:00 - 18:00h <i class="fa fa-location-arrow" style="font-size: 16px;color: #64abea;margin-left: 24px;margin-right: 10px;"></i>C/ Pere IV, 29-35 bajos Barcelona</div>
						<div class="gdlr-core-dropdown-tab-content " data-index="1"><i class="fa fa-phone" style="font-size: 16px;color: #f7c02e;margin-right: 10px;"></i> +1 2223 4567 <i class="fa fa-clock-o" style="font-size: 16px;color: #f7c02e;margin-left: 24px;margin-right: 10px;"></i> Mon - Fri 09:00 - 17:00 <i class="fa fa-location-arrow" style="font-size: 16px;color: #64abea;margin-left: 24px;margin-right: 10px;"></i>4th Avenue Kingston St. New York</div>						
					</div>
				</div>
			</div>
			<div class="realfactory-top-bar-right realfactory-item-pdlr">
				<div class="realfactory-top-bar-right-social"><a href="mailto:taller@moldarc.com" target="_blank" class="realfactory-top-bar-social-icon" title="email"><i class="fa fa-envelope"></i></a><a href="#" target="_blank" class="realfactory-top-bar-social-icon" title="facebook"><i class="fa fa-facebook"></i></a><a href="#" target="_blank" class="realfactory-top-bar-social-icon" title="google-plus"><i class="fa fa-google-plus"></i></a><a href="#" target="_blank" class="realfactory-top-bar-social-icon" title="linkedin"><i class="fa fa-linkedin"></i></a><a href="#" target="_blank" class="realfactory-top-bar-social-icon" title="twitter"><i class="fa fa-twitter"></i></a></div>
			</div>
		</div>
	</div>
	<header class="realfactory-header-wrap realfactory-header-style-bar realfactory-header-background  realfactory-style-left">
		<div class="realfactory-header-container clearfix  realfactory-container">
			<div class="realfactory-header-container-inner">
				<div class="realfactory-logo  realfactory-item-pdlr">
					<div class="realfactory-logo-inner">
						<a href="<?= base_url() ?>"><img src="[base_url]theme/theme/images/logo.png" alt=""></a>
					</div>
				</div>
				<div class="realfactory-logo-right-text realfactory-item-pdlr">
					<div class="realfactory-logo-right-block"><i class="realfactory-logo-right-block-icon icon_check_alt2"></i>
						<div class="realfactory-logo-right-block-content">
							<div class="realfactory-logo-right-block-title realfactory-title-font">Normas Standard Int.</div>
							<div class="realfactory-logo-right-block-caption realfactory-title-font">DIN, AFNOR, AISI, UNE, W-NR...</div>
						</div>
					</div>
					<div class="realfactory-logo-right-block"><i class="realfactory-logo-right-block-icon icon_check_alt2"></i>
						<div class="realfactory-logo-right-block-content">
							<div class="realfactory-logo-right-block-title realfactory-title-font">Maquinaria</div>
							<div class="realfactory-logo-right-block-caption realfactory-title-font">Rofin, Praxair, Alphalaser y Messer Griesheim</div>
						</div>
					</div>
					<!-- 
<div class="realfactory-logo-right-block"><i class="realfactory-logo-right-block-icon icon_check_alt2"></i>
						<div class="realfactory-logo-right-block-content">
							<div class="realfactory-logo-right-block-title realfactory-title-font">Número 1</div>
							<div class="realfactory-logo-right-block-caption realfactory-title-font">detecció de fisures</div>
						</div>
 -->
						<a class="realfactory-header-right-button" href="[base_url]contacte.html" target="_self">Contacto</a>
					</div>
				</div>
			</div>
	</header>

	<div class="realfactory-navigation-bar-wrap  realfactory-style-transparent realfactory-sticky-navigation realfactory-sticky-navigation-height realfactory-style-left  realfactory-style-fixed realfactory-without-placeholder">
		<div class="realfactory-navigation-background"></div>
		<div class="realfactory-navigation-container clearfix  realfactory-container">
			<div class="realfactory-navigation realfactory-item-pdlr clearfix ">
				<div class="realfactory-main-menu" id="realfactory-main-menu">
					<ul id="menu-main-navigation-1" class="sf-menu">
						<li class="menu-item <?= empty($url)?'current-menu-item':'' ?> realfactory-normal-menu"><a href="<?= base_url() ?>" class="sf-with-ul-pre">Inicio</a></li>
						<li class="menu-item <?= !empty($url) && $url=='empresa'?'current-menu-item':'' ?> realfactory-normal-menu"><a href="<?= base_url() ?>empresa.html" class="sf-with-ul-pre">Empresa</a></li>						
						<li class="menu-item <?= !empty($url) && $url=='serveis'?'current-menu-item':'' ?> menu-item-home menu-item-has-children realfactory-normal-menu">
							<a href="<?= base_url() ?>servicios.html" class="sf-with-ul-pre">
								Servicios
							</a>
							<ul class="sub-menu">
								<?php $this->db->order_by('orden','ASC');  foreach($this->db->get_where('servicios',array('idioma'=>$_SESSION['lang']))->result() as $n=>$v): ?>
									<li class="menu-item"><a href="<?= base_url('servicio/'.toUrl($v->id.'-'.$v->titulo)) ?>"><?= $v->titulo ?></a></li>
								<?php endforeach ?>
							</ul>
						</li>
						<li class="menu-item <?= !empty($url) && $url=='ultimos-trabajos'?'current-menu-item':'' ?> realfactory-normal-menu"><a href="<?= base_url() ?>ultimos-trabajos.html" class="sf-with-ul-pre">Últimos trabajos</a></li>
						<li class="menu-item <?= !empty($url) && $url=='contacte'?'current-menu-item':'' ?> realfactory-normal-menu"><a href="<?= base_url() ?>contacto.html" class="sf-with-ul-pre">Contacto</a></li>
						
					</ul>
					<div class="realfactory-navigation-slide-bar" id="realfactory-navigation-slide-bar"></div>
				</div>
				<div class="realfactory-main-menu-right-wrap clearfix ">
					<div class="realfactory-main-menu-search" id="realfactory-top-search"><i class="fa fa-search"></i></div>
					<div class="realfactory-top-search-wrap">
						<div class="realfactory-top-search-close"></div>
						<div class="realfactory-top-search-row">
							<div class="realfactory-top-search-cell">
								<form role="search" method="get" class="search-form" action="<?= base_url('paginas/frontend/search') ?>">
									<input type="text" class="search-field realfactory-title-font" placeholder="Buscar..." value="" name="q">
									<div class="realfactory-top-search-submit"><i class="fa fa-search"></i></div>
									<input type="submit" class="search-submit" value="Search">
									<div class="realfactory-top-search-close"><i class="icon_close"></i></div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
