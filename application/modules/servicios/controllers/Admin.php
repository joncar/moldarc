<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }

        function servicios(){
            
            $crud = $this->crud_function('','');  
            $crud->set_field_upload('icono','img/servicios');                    
            $crud->set_field_upload('icono_hover','img/servicios');      
            $crud->columns('titulo','subtitulo','idioma','orden');              
            $crud->field_type('foto_antes','image',array('path'=>'img/servicios','width'=>'840px','height'=>'560px'));
			$crud->field_type('foto_despues','image',array('path'=>'img/servicios','width'=>'840px','height'=>'560px'));
			$crud->field_type('foto_despues','image',array('path'=>'img/servicios','width'=>'840px','height'=>'560px'));
			$crud->field_type('foto1','image',array('path'=>'img/servicios','width'=>'1200px','height'=>'780px'));
			$crud->field_type('foto2','image',array('path'=>'img/servicios','width'=>'1200px','height'=>'780px'));
			$crud->field_type('foto3','image',array('path'=>'img/servicios','width'=>'1200px','height'=>'780px'));
			$crud->field_type('foto','image',array('path'=>'img/servicios','width'=>'366px','height'=>'408px'));
			$crud->field_type('foto_hover','image',array('path'=>'img/servicios','width'=>'366px','height'=>'408px'));
			$crud->field_type('idioma','dropdown',array('es'=>'Castellano','ca'=>'Catalán','en'=>'Inglés'));
            $crud->field_type('banner','image',array('path'=>'img/servicios','width'=>'1700px','height'=>'500px'));
			$crud->add_action('detalles','',base_url('servicios/admin/servicios_detalles').'/');
			$crud->set_lang_string('insert_success_message','Servicio almacenado con éxito <script>document.location.href="'.base_url('servicios/admin/servicios_detalles/{id}/add').'"; </script>');
            $crud->set_clone();
            $crud = $crud->render();
            $this->loadView($crud);
        }
        function ultimos_trabajos(){
            
            $crud = $this->crud_function('','');              
			$crud->field_type('foto','image',array('path'=>'img/servicios','width'=>'1920px','height'=>'1080px'));
            $crud->field_type('miniatura','image',array('path'=>'img/servicios','width'=>'500px','height'=>'625px'));
			$crud->field_type('idioma','dropdown',array('es'=>'Castellano','ca'=>'Catalán','en'=>'Inglés'));            
			$crud->field_type('tags','tags');
            $crud->set_clone();
            $crud = $crud->render();
            $this->loadView($crud);
        }
        function servicios_detalles($x = ''){
            $crud = $this->crud_function('','');   
            $crud->field_type('servicios_id','hidden',$x)
                 ->where('servicios_id',$x);           
            $crud = $crud->render();
            $this->loadView($crud);
        }
    }
?>
