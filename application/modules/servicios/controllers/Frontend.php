<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();
        }

        function ver($id){
        	$id = explode('-',$id);
            $id = $id[0];
            if(is_numeric($id)){
            	$servicio = $this->db->get_where('servicios',array('id'=>$id));
            	if($servicio->num_rows()>0){
                    if($servicio->row()->idioma!=$_SESSION['lang']){
                        $serv = $this->db->get_where('servicios',array('orden'=>$servicio->row()->orden,'idioma'=>$_SESSION['lang']));
                        if($serv->num_rows()>0){
                            $serv = $serv->row();
                            redirect('servei/'.toUrl($serv->id.'-'.$serv->titulo));
                        }
                    }
            		$servicio = $servicio->row();
            		$servicio->detalles = $this->db->get_where('servicios_detalles',array('servicios_id'=>$servicio->id));
            		$this->loadView(array('view'=>'ver','servicio'=>$servicio,'url'=>'serveis','title'=>$servicio->titulo));
            	}else{
            		redirect('main');
            	}
            }
        }
    }
?>
