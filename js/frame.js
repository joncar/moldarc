function emergente(data,xs,ys,boton,header){


	var x = (xs==undefined)?(window.innerWidth/2)-325:xs;
	var y = (ys==undefined)?(window.innerHeight/2):ys;
	var b = (boton==undefined || boton)?true:false;
	var h = (header==undefined)?'Mensaje':header;
	if(jQuery(".modal").html()==undefined){
	jQuery('body,html').animate({scrollTop: 0}, 800);
	var str = '';
            var str = '<!-- Modal -->'+
            '<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">'+
            '<div class="modal-dialog">'+
            '<div class="modal-content">'+
            '<div class="modal-body">'+
            data+
            '</div>'+
            '<div class="modal-footer">'+
            '<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>'+
            '</div>'+
            '</div><!-- /.modal-content -->'+
            '</div><!-- /.modal-dialog -->'+
            '</div><!-- /.modal -->';
            jQuery("body").append(str);
            jQuery("#myModal").modal("toggle");
	}
	else
	{
		jQuery(".modal-body").html(data);
                    if(jQuery("#myModal").css('display')=='none')
                        jQuery("#myModal").modal("toggle");
	}
}

function remoteConnection(url,data,callback){        
    return jQuery.ajax({
        url: URL+url,
        data: data,
        context: document.body,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        success:callback
    });
};

function insertar(url,datos,resultId,callback,callbackError){
    jQuery(resultId).removeClass('alert alert-danger').html('');
    jQuery("button[type=submit]").attr('disabled',true);
    var uri = url.replace('insert','insert_validation');
    uri = uri.replace('update','update_validation');
    remoteConnection(uri,datos,function(data){
        jQuery("button[type=submit]").attr('disabled',false);
        data = jQuery(data).text();
        data = JSON.parse(data);
        
        if(data.success){
          remoteConnection(url,datos,function(data){
            data = jQuery(data).text();
            data = JSON.parse(data);
            callback(data);
          });
        }else{
          jQuery(resultId).addClass('alert alert-danger').html(data.error_message);
          callbackError(data);
        }
    });
}

//jQuery(document).on('ready',function(){alert('');});
function sendData(form,divResponse){
    var url = jQuery(form).attr('action');
    var f = new FormData(form);
    remoteConnection(url,f,function(data){
        jQuery(divResponse).html(data);
    });

    return false;
}